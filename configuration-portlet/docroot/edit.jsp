<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>

<%@ page import="it.eng.model.ExtModule" %>
<%@ page import="it.eng.service.ExtModuleLocalServiceUtil" %>

<%
    ExtModule extModule = null;

    long moduleId = ParamUtil.getLong(request, "moduleId");

    if (moduleId > 0) {
    	extModule = ExtModuleLocalServiceUtil.getExtModule(moduleId);
    }

    String redirect = ParamUtil.getString(request, "redirect");
%>


<aui:model-context bean="<%= extModule %>" model="<%= ExtModule.class %>" />
<portlet:renderURL var="viewModuleURL" />
<portlet:actionURL name='<%= Validator.isNull(extModule) ? "addExtModule" : "updateExtModule" %>' 
					var="editModuleURL" 
					windowState="normal" />

<liferay-ui:header
    backURL="<%= viewModuleURL %>"
    title='<%= (extModule != null) ? extModule.getModuleName() : "new-module" %>'
/>

<aui:form action="<%= editModuleURL %>" method="POST" name="fm">
    <aui:fieldset>
    	<aui:input name="redirect" type="hidden" value="<%= redirect %>" />

        <aui:input name="moduleId" type="hidden" value='<%= Validator.isNull(extModule) ? "" : extModule.getModuleId() %>'/>

        <aui:input style="width:50%" type="text" label="module-name" 
        			name="moduleName" value='<%= Validator.isNull(extModule) ? "" : extModule.getModuleName() %>'>
        	<aui:validator name="required"/>
        </aui:input>

        <aui:input style="width:50%" type="text" label="module-url" 
        			name="moduleURL" value='<%= Validator.isNull(extModule) ? "" : extModule.getModuleURL() %>'>
        	<aui:validator name="required"/>
        </aui:input>
        

        
        <%
        boolean regPropagChecked = false;
        boolean delPropagChecked = false;
        boolean upsertOrganizationPropagChecked = false;
        boolean deleteOrganizationPropagChecked = false;
        boolean addOrganizationMembersPropagChecked = false;
        boolean removeOrganizationMembersPropagChecked = false;
        
        
        if (Validator.isNotNull(extModule)){
	        if ( extModule.getRegistrationPropagation())
	        	regPropagChecked = true;
	        
	        if ( extModule.getDeletionPropagation())
	        	delPropagChecked = true;
	        
	        if ( extModule.getUpsertOrganizationPropagation() )
	        	upsertOrganizationPropagChecked = true;
	        
	        if ( extModule.getDeleteOrganizationPropagation())
	        	deleteOrganizationPropagChecked = true;
	        
	        if ( extModule.getAddOrganizationMembersPropagation())
	        	addOrganizationMembersPropagChecked = true;
	        
	        if ( extModule.getRemoveOrganizationMembersPropagation())
	        	removeOrganizationMembersPropagChecked = true;
        }
        %>
        
        <aui:input style="width:50%" type="checkbox" label="registrationPropagation" name="registrationPropagation" value="<%=regPropagChecked%>"/>

        <aui:input style="width:50%" type="text" label="module-endpoint" 
        			name="moduleEndpoint" value='<%= Validator.isNull(extModule) ? "" : extModule.getModuleEndpoint() %>'
        			helpMessage="e.g. https://example.com/dev/api/ods" >
        </aui:input>


		<aui:input style="width:50%" type="checkbox" label="deletionPropagation" name="deletionPropagation" value="<%=delPropagChecked%>"/>
		<aui:input style="width:50%" type="text" label="user-deletion-endpoint" 
        			name="userDeletionEndpoint" value='<%= Validator.isNull(extModule)  ? "" : extModule.getUserDeletionEndpoint() %>'
        			helpMessage="e.g. https://example.com/dev/api/ods" >
        </aui:input>
        
        <aui:input style="width:50%" type="checkbox" label="UpsertOrganizationPropagation" name="upsertOrganizationPropagation" value="<%=upsertOrganizationPropagChecked%>"/>
		<aui:input style="width:50%" type="text" label="UpsertOrganization-endpoint" 
        			name="upsertOrganizationEndpoint" value='<%= Validator.isNull(extModule)  ? "" : extModule.getUpsertOrganizationEndpoint() %>'
        			helpMessage="e.g. https://example.com/dev/api/ods" >
        </aui:input>
        
                <aui:input style="width:50%" type="checkbox" label="DeleteOrganizationPropagation" name="deleteOrganizationPropagation" value="<%=deleteOrganizationPropagChecked%>"/>
		<aui:input style="width:50%" type="text" label="DeleteOrganization-endpoint" 
        			name="deleteOrganizationEndpoint" value='<%= Validator.isNull(extModule)  ? "" : extModule.getDeleteOrganizationEndpoint() %>'
        			helpMessage="e.g. https://example.com/dev/api/ods" >
        </aui:input>
        
                <aui:input style="width:50%" type="checkbox" label="addOrganizationMembersPropagation" name="addOrganizationMembersPropagation" value="<%=addOrganizationMembersPropagChecked%>"/>
		<aui:input style="width:50%" type="text" label="addOrganizationMembers-endpoint" 
        			name="addOrganizationMembersEndpoint" value='<%= Validator.isNull(extModule)  ? "" : extModule.getAddOrganizationMembersEndpoint() %>'
        			helpMessage="e.g. https://example.com/dev/api/ods" >
        </aui:input>
        
                <aui:input style="width:50%" type="checkbox" label="removeOrganizationMembersPropagation" name="removeOrganizationMembersPropagation" value="<%=removeOrganizationMembersPropagChecked%>"/>
		<aui:input style="width:50%" type="text" label="removeOrganizationMembers-endpoint" 
        			name="removeOrganizationMembersEndpoint" value='<%= Validator.isNull(extModule)  ? "" : extModule.getRemoveOrganizationMembersEndpoint() %>'
        			helpMessage="e.g. https://example.com/dev/api/ods" >
        </aui:input>


		

        <aui:input style="width:50%" type="text" label="module-alias" 
        			name="moduleAlias" value='<%= Validator.isNull(extModule) ? "" : extModule.getModuleAlias() %>'>
        	<aui:validator name="required"/>
        </aui:input>
        
        <aui:input style="width:50%" type="text" label="module-secret" 
        			name="moduleSharedSecret" value='<%= Validator.isNull(extModule) ? "" : extModule.getModuleSharedSecret() %>'>
        </aui:input>
        
        <aui:input style="width:50%" type="text" label="content-type" 
        			name="moduleContentType" value='<%= Validator.isNull(extModule) ? "" : extModule.getContentType() %>'>
        </aui:input>  
       

    </aui:fieldset>

    <aui:button-row>
        <aui:button type="submit" />

        <aui:button onClick="<%= viewModuleURL %>"  type="cancel" />
    </aui:button-row>
</aui:form>