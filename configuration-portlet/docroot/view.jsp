<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="javax.portlet.ActionRequest"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay" %>
<%@ page import="com.liferay.portal.kernel.workflow.WorkflowConstants" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>

<%@ page import="java.io.File" %>

<%@ page import="it.eng.model.ExtModule" %>
<%@ page import="it.eng.model.PortalConf" %>
<%@ page import="it.eng.service.ExtModuleLocalServiceUtil" %>
<%@ page import="it.eng.service.PortalConfLocalServiceUtil" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
    String redirect = PortalUtil.getCurrentURL(renderRequest);
	
	PortalConf portalConf = PortalConfLocalServiceUtil.getSecurityConfiguration();
	
	String defaultLocation = System.getProperty("java.home") + "/lib/security/cacerts".replace('/', File.separatorChar);
	String defaultPassword = "changeit";
%>

<liferay-ui:error key="delete_error" message="delete-error" />
<liferay-ui:error key="add_error" message="add-error" />
<liferay-ui:error key="update_error" message="update-error" />
<liferay-ui:error key="conf_error" message="conf-error" />

<liferay-ui:tabs names="modules-conf,portal-conf" refresh="<%= false %>">

<liferay-ui:section>
<h3><liferay-ui:message key="modules-list"/></h3>

<liferay-ui:search-container emptyResultsMessage="no-results">
    <liferay-ui:search-container-results
        results="<%= ExtModuleLocalServiceUtil.getExtModules(0,ExtModuleLocalServiceUtil.getExtModulesCount()) %>"
        total="<%= ExtModuleLocalServiceUtil.getExtModulesCount() %>"
    />

    <liferay-ui:search-container-row
        className="ExtModule"
        keyProperty="moduleId"
        modelVar="extModule" escapedModel="<%= true %>">    
                
        <liferay-ui:search-container-column-text
            name="module-name"
            value="<%= extModule.getModuleName()%>"/>
         
       <liferay-ui:search-container-column-text
            name="registration-propagation"
            value="<%= extModule.getRegistrationPropagation().toString() %>"/> 
            
        <liferay-ui:search-container-column-text
            name="module-endpoint"
            value="<%= extModule.getModuleEndpoint() %>"/>
            
		<liferay-ui:search-container-column-text
             name="deletion-propagation"
            value="<%= extModule.getDeletionPropagation().toString() %>"/>  
        
        <liferay-ui:search-container-column-text
            name="user-deletion-endpoint"
            value="<%= extModule.getUserDeletionEndpoint() %>"/>
            
  
  		<liferay-ui:search-container-column-text
             name="upsertOrganization-propagation"
            value="<%= extModule.getUpsertOrganizationPropagation().toString() %>"/>  
        
        <liferay-ui:search-container-column-text
            name="upsertOrganization-endpoint"
            value="<%= extModule.getUpsertOrganizationEndpoint() %>"/>
  
  
  		<liferay-ui:search-container-column-text
             name="DeleteOrganization-propagation"
            value="<%= extModule.getDeleteOrganizationPropagation().toString() %>"/>  
        
        <liferay-ui:search-container-column-text
            name="DeleteOrganization-endpoint"
            value="<%= extModule.getDeleteOrganizationEndpoint() %>"/>
  
  		<liferay-ui:search-container-column-text
             name="AddOrganizationMembers-propagation"
            value="<%= extModule.getAddOrganizationMembersPropagation().toString() %>"/>  
        
        <liferay-ui:search-container-column-text
            name="AddOrganizationMembers-endpoint"
            value="<%= extModule.getAddOrganizationMembersEndpoint() %>"/>
            
            
         		<liferay-ui:search-container-column-text
             name="RemoveOrganizationMembers-propagation"
            value="<%= extModule.getRemoveOrganizationMembersPropagation().toString() %>"/>  
        
        <liferay-ui:search-container-column-text
            name="RemoveOrganizationMembers-endpoint"
            value="<%= extModule.getRemoveOrganizationMembersEndpoint() %>"/>   
            
            
        <liferay-ui:search-container-column-text
            name="module-alias"
            value="<%= extModule.getModuleAlias() %>"/>
            
        <liferay-ui:search-container-column-text
            name="module-secret"
            value="<%= extModule.getModuleSharedSecret() %>"/>
            
        <liferay-ui:search-container-column-text
            name="content-type"
            value="<%= extModule.getContentType() %>"/>
            
        <liferay-ui:search-container-column-jsp
     		align="right"
     		path="/extmodule_actions.jsp"/>
        
    </liferay-ui:search-container-row>

    <liferay-ui:search-iterator />
</liferay-ui:search-container>
<hr>
<aui:button-row>
    <portlet:renderURL var="addModuleURL">
        <portlet:param name="mvcPath" value="/edit.jsp" />
        <portlet:param name="redirect" value="<%= redirect %>" />
    </portlet:renderURL>

    <aui:button onClick="<%= addModuleURL.toString() %>" value="add-module" />
</aui:button-row>
</liferay-ui:section>

<liferay-ui:section>
<portlet:actionURL name='<%= portalConf == null ? "addPortalConf" : "updatePortalConf" %>' 
					var="editConfURL" 
					windowState="normal" />
					
<aui:form action="<%= editConfURL %>" method="POST" name="fm">
	<aui:fieldset>
		<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
		
		<h3><liferay-ui:message key="tomcat-conf"/></h3>
		<aui:input style="width:50%" type="text" label="tomcat-location" 
        			name="tomcatKsLocation" value='<%= portalConf == null ? "" : portalConf.getTomcatKsLocation() %>'>
        	<aui:validator name="required"/> 	
        </aui:input>
        <aui:input style="width:50%" type="text" label="tomcat-password" 
        			name="tomcatKsPassword" value='<%= portalConf == null ? "" : portalConf.getTomcatKsPassword() %>'>
        	<aui:validator name="required"/>
        </aui:input>
        <aui:input style="width:50%" type="text" label="tomcat-alias" 
        			name="tomcatKsAlias" value='<%= portalConf == null ? "" : portalConf.getTomcatKsAlias() %>'>
        	<aui:validator name="required"/>
        </aui:input>
        <h3><liferay-ui:message key="jvm-conf"/></h3>
        <aui:input style="width:50%" type="text" label="jvm-location" 
        			name="jvmKsLocation" value='<%= portalConf == null ? defaultLocation : portalConf.getJvmKsLocation() %>'>
        	<aui:validator name="required"/>
        </aui:input>
        <aui:input style="width:50%" type="text" label="jvm-password" 
        			name="jvmKsPassword" value='<%= portalConf == null ? defaultPassword : portalConf.getJvmKsPassword() %>'>
        	<aui:validator name="required"/>
        </aui:input>
	</aui:fieldset>
	
	<aui:button-row>
        <aui:button type="submit" />
    </aui:button-row>
</aui:form>
</liferay-ui:section>

</liferay-ui:tabs>