/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class PortalConfSoap implements Serializable {
	public static PortalConfSoap toSoapModel(PortalConf model) {
		PortalConfSoap soapModel = new PortalConfSoap();

		soapModel.setConfName(model.getConfName());
		soapModel.setTomcatKsAlias(model.getTomcatKsAlias());
		soapModel.setTomcatKsPassword(model.getTomcatKsPassword());
		soapModel.setTomcatKsLocation(model.getTomcatKsLocation());
		soapModel.setJvmKsLocation(model.getJvmKsLocation());
		soapModel.setJvmKsPassword(model.getJvmKsPassword());

		return soapModel;
	}

	public static PortalConfSoap[] toSoapModels(PortalConf[] models) {
		PortalConfSoap[] soapModels = new PortalConfSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PortalConfSoap[][] toSoapModels(PortalConf[][] models) {
		PortalConfSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PortalConfSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PortalConfSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PortalConfSoap[] toSoapModels(List<PortalConf> models) {
		List<PortalConfSoap> soapModels = new ArrayList<PortalConfSoap>(models.size());

		for (PortalConf model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PortalConfSoap[soapModels.size()]);
	}

	public PortalConfSoap() {
	}

	public String getPrimaryKey() {
		return _confName;
	}

	public void setPrimaryKey(String pk) {
		setConfName(pk);
	}

	public String getConfName() {
		return _confName;
	}

	public void setConfName(String confName) {
		_confName = confName;
	}

	public String getTomcatKsAlias() {
		return _tomcatKsAlias;
	}

	public void setTomcatKsAlias(String tomcatKsAlias) {
		_tomcatKsAlias = tomcatKsAlias;
	}

	public String getTomcatKsPassword() {
		return _tomcatKsPassword;
	}

	public void setTomcatKsPassword(String tomcatKsPassword) {
		_tomcatKsPassword = tomcatKsPassword;
	}

	public String getTomcatKsLocation() {
		return _tomcatKsLocation;
	}

	public void setTomcatKsLocation(String tomcatKsLocation) {
		_tomcatKsLocation = tomcatKsLocation;
	}

	public String getJvmKsLocation() {
		return _jvmKsLocation;
	}

	public void setJvmKsLocation(String jvmKsLocation) {
		_jvmKsLocation = jvmKsLocation;
	}

	public String getJvmKsPassword() {
		return _jvmKsPassword;
	}

	public void setJvmKsPassword(String jvmKsPassword) {
		_jvmKsPassword = jvmKsPassword;
	}

	private String _confName;
	private String _tomcatKsAlias;
	private String _tomcatKsPassword;
	private String _tomcatKsLocation;
	private String _jvmKsLocation;
	private String _jvmKsPassword;
}