/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ExtModule}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see ExtModule
 * @generated
 */
public class ExtModuleWrapper implements ExtModule, ModelWrapper<ExtModule> {
	public ExtModuleWrapper(ExtModule extModule) {
		_extModule = extModule;
	}

	@Override
	public Class<?> getModelClass() {
		return ExtModule.class;
	}

	@Override
	public String getModelClassName() {
		return ExtModule.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("moduleId", getModuleId());
		attributes.put("moduleName", getModuleName());
		attributes.put("moduleURL", getModuleURL());
		attributes.put("registrationPropagation", getRegistrationPropagation());
		attributes.put("moduleEndpoint", getModuleEndpoint());
		attributes.put("deletionPropagation", getDeletionPropagation());
		attributes.put("userDeletionEndpoint", getUserDeletionEndpoint());
		attributes.put("upsertOrganizationPropagation",
			getUpsertOrganizationPropagation());
		attributes.put("upsertOrganizationEndpoint",
			getUpsertOrganizationEndpoint());
		attributes.put("deleteOrganizationPropagation",
			getDeleteOrganizationPropagation());
		attributes.put("deleteOrganizationEndpoint",
			getDeleteOrganizationEndpoint());
		attributes.put("addOrganizationMembersPropagation",
			getAddOrganizationMembersPropagation());
		attributes.put("addOrganizationMembersEndpoint",
			getAddOrganizationMembersEndpoint());
		attributes.put("removeOrganizationMembersPropagation",
			getRemoveOrganizationMembersPropagation());
		attributes.put("removeOrganizationMembersEndpoint",
			getRemoveOrganizationMembersEndpoint());
		attributes.put("moduleAlias", getModuleAlias());
		attributes.put("moduleSharedSecret", getModuleSharedSecret());
		attributes.put("contentType", getContentType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long moduleId = (Long)attributes.get("moduleId");

		if (moduleId != null) {
			setModuleId(moduleId);
		}

		String moduleName = (String)attributes.get("moduleName");

		if (moduleName != null) {
			setModuleName(moduleName);
		}

		String moduleURL = (String)attributes.get("moduleURL");

		if (moduleURL != null) {
			setModuleURL(moduleURL);
		}

		Boolean registrationPropagation = (Boolean)attributes.get(
				"registrationPropagation");

		if (registrationPropagation != null) {
			setRegistrationPropagation(registrationPropagation);
		}

		String moduleEndpoint = (String)attributes.get("moduleEndpoint");

		if (moduleEndpoint != null) {
			setModuleEndpoint(moduleEndpoint);
		}

		Boolean deletionPropagation = (Boolean)attributes.get(
				"deletionPropagation");

		if (deletionPropagation != null) {
			setDeletionPropagation(deletionPropagation);
		}

		String userDeletionEndpoint = (String)attributes.get(
				"userDeletionEndpoint");

		if (userDeletionEndpoint != null) {
			setUserDeletionEndpoint(userDeletionEndpoint);
		}

		Boolean upsertOrganizationPropagation = (Boolean)attributes.get(
				"upsertOrganizationPropagation");

		if (upsertOrganizationPropagation != null) {
			setUpsertOrganizationPropagation(upsertOrganizationPropagation);
		}

		String upsertOrganizationEndpoint = (String)attributes.get(
				"upsertOrganizationEndpoint");

		if (upsertOrganizationEndpoint != null) {
			setUpsertOrganizationEndpoint(upsertOrganizationEndpoint);
		}

		Boolean deleteOrganizationPropagation = (Boolean)attributes.get(
				"deleteOrganizationPropagation");

		if (deleteOrganizationPropagation != null) {
			setDeleteOrganizationPropagation(deleteOrganizationPropagation);
		}

		String deleteOrganizationEndpoint = (String)attributes.get(
				"deleteOrganizationEndpoint");

		if (deleteOrganizationEndpoint != null) {
			setDeleteOrganizationEndpoint(deleteOrganizationEndpoint);
		}

		Boolean addOrganizationMembersPropagation = (Boolean)attributes.get(
				"addOrganizationMembersPropagation");

		if (addOrganizationMembersPropagation != null) {
			setAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
		}

		String addOrganizationMembersEndpoint = (String)attributes.get(
				"addOrganizationMembersEndpoint");

		if (addOrganizationMembersEndpoint != null) {
			setAddOrganizationMembersEndpoint(addOrganizationMembersEndpoint);
		}

		Boolean removeOrganizationMembersPropagation = (Boolean)attributes.get(
				"removeOrganizationMembersPropagation");

		if (removeOrganizationMembersPropagation != null) {
			setRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
		}

		String removeOrganizationMembersEndpoint = (String)attributes.get(
				"removeOrganizationMembersEndpoint");

		if (removeOrganizationMembersEndpoint != null) {
			setRemoveOrganizationMembersEndpoint(removeOrganizationMembersEndpoint);
		}

		String moduleAlias = (String)attributes.get("moduleAlias");

		if (moduleAlias != null) {
			setModuleAlias(moduleAlias);
		}

		String moduleSharedSecret = (String)attributes.get("moduleSharedSecret");

		if (moduleSharedSecret != null) {
			setModuleSharedSecret(moduleSharedSecret);
		}

		String contentType = (String)attributes.get("contentType");

		if (contentType != null) {
			setContentType(contentType);
		}
	}

	/**
	* Returns the primary key of this ext module.
	*
	* @return the primary key of this ext module
	*/
	@Override
	public long getPrimaryKey() {
		return _extModule.getPrimaryKey();
	}

	/**
	* Sets the primary key of this ext module.
	*
	* @param primaryKey the primary key of this ext module
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_extModule.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the module ID of this ext module.
	*
	* @return the module ID of this ext module
	*/
	@Override
	public long getModuleId() {
		return _extModule.getModuleId();
	}

	/**
	* Sets the module ID of this ext module.
	*
	* @param moduleId the module ID of this ext module
	*/
	@Override
	public void setModuleId(long moduleId) {
		_extModule.setModuleId(moduleId);
	}

	/**
	* Returns the module name of this ext module.
	*
	* @return the module name of this ext module
	*/
	@Override
	public java.lang.String getModuleName() {
		return _extModule.getModuleName();
	}

	/**
	* Sets the module name of this ext module.
	*
	* @param moduleName the module name of this ext module
	*/
	@Override
	public void setModuleName(java.lang.String moduleName) {
		_extModule.setModuleName(moduleName);
	}

	/**
	* Returns the module u r l of this ext module.
	*
	* @return the module u r l of this ext module
	*/
	@Override
	public java.lang.String getModuleURL() {
		return _extModule.getModuleURL();
	}

	/**
	* Sets the module u r l of this ext module.
	*
	* @param moduleURL the module u r l of this ext module
	*/
	@Override
	public void setModuleURL(java.lang.String moduleURL) {
		_extModule.setModuleURL(moduleURL);
	}

	/**
	* Returns the registration propagation of this ext module.
	*
	* @return the registration propagation of this ext module
	*/
	@Override
	public java.lang.Boolean getRegistrationPropagation() {
		return _extModule.getRegistrationPropagation();
	}

	/**
	* Sets the registration propagation of this ext module.
	*
	* @param registrationPropagation the registration propagation of this ext module
	*/
	@Override
	public void setRegistrationPropagation(
		java.lang.Boolean registrationPropagation) {
		_extModule.setRegistrationPropagation(registrationPropagation);
	}

	/**
	* Returns the module endpoint of this ext module.
	*
	* @return the module endpoint of this ext module
	*/
	@Override
	public java.lang.String getModuleEndpoint() {
		return _extModule.getModuleEndpoint();
	}

	/**
	* Sets the module endpoint of this ext module.
	*
	* @param moduleEndpoint the module endpoint of this ext module
	*/
	@Override
	public void setModuleEndpoint(java.lang.String moduleEndpoint) {
		_extModule.setModuleEndpoint(moduleEndpoint);
	}

	/**
	* Returns the deletion propagation of this ext module.
	*
	* @return the deletion propagation of this ext module
	*/
	@Override
	public java.lang.Boolean getDeletionPropagation() {
		return _extModule.getDeletionPropagation();
	}

	/**
	* Sets the deletion propagation of this ext module.
	*
	* @param deletionPropagation the deletion propagation of this ext module
	*/
	@Override
	public void setDeletionPropagation(java.lang.Boolean deletionPropagation) {
		_extModule.setDeletionPropagation(deletionPropagation);
	}

	/**
	* Returns the user deletion endpoint of this ext module.
	*
	* @return the user deletion endpoint of this ext module
	*/
	@Override
	public java.lang.String getUserDeletionEndpoint() {
		return _extModule.getUserDeletionEndpoint();
	}

	/**
	* Sets the user deletion endpoint of this ext module.
	*
	* @param userDeletionEndpoint the user deletion endpoint of this ext module
	*/
	@Override
	public void setUserDeletionEndpoint(java.lang.String userDeletionEndpoint) {
		_extModule.setUserDeletionEndpoint(userDeletionEndpoint);
	}

	/**
	* Returns the upsert organization propagation of this ext module.
	*
	* @return the upsert organization propagation of this ext module
	*/
	@Override
	public java.lang.Boolean getUpsertOrganizationPropagation() {
		return _extModule.getUpsertOrganizationPropagation();
	}

	/**
	* Sets the upsert organization propagation of this ext module.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation of this ext module
	*/
	@Override
	public void setUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation) {
		_extModule.setUpsertOrganizationPropagation(upsertOrganizationPropagation);
	}

	/**
	* Returns the upsert organization endpoint of this ext module.
	*
	* @return the upsert organization endpoint of this ext module
	*/
	@Override
	public java.lang.String getUpsertOrganizationEndpoint() {
		return _extModule.getUpsertOrganizationEndpoint();
	}

	/**
	* Sets the upsert organization endpoint of this ext module.
	*
	* @param upsertOrganizationEndpoint the upsert organization endpoint of this ext module
	*/
	@Override
	public void setUpsertOrganizationEndpoint(
		java.lang.String upsertOrganizationEndpoint) {
		_extModule.setUpsertOrganizationEndpoint(upsertOrganizationEndpoint);
	}

	/**
	* Returns the delete organization propagation of this ext module.
	*
	* @return the delete organization propagation of this ext module
	*/
	@Override
	public java.lang.Boolean getDeleteOrganizationPropagation() {
		return _extModule.getDeleteOrganizationPropagation();
	}

	/**
	* Sets the delete organization propagation of this ext module.
	*
	* @param deleteOrganizationPropagation the delete organization propagation of this ext module
	*/
	@Override
	public void setDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation) {
		_extModule.setDeleteOrganizationPropagation(deleteOrganizationPropagation);
	}

	/**
	* Returns the delete organization endpoint of this ext module.
	*
	* @return the delete organization endpoint of this ext module
	*/
	@Override
	public java.lang.String getDeleteOrganizationEndpoint() {
		return _extModule.getDeleteOrganizationEndpoint();
	}

	/**
	* Sets the delete organization endpoint of this ext module.
	*
	* @param deleteOrganizationEndpoint the delete organization endpoint of this ext module
	*/
	@Override
	public void setDeleteOrganizationEndpoint(
		java.lang.String deleteOrganizationEndpoint) {
		_extModule.setDeleteOrganizationEndpoint(deleteOrganizationEndpoint);
	}

	/**
	* Returns the add organization members propagation of this ext module.
	*
	* @return the add organization members propagation of this ext module
	*/
	@Override
	public java.lang.Boolean getAddOrganizationMembersPropagation() {
		return _extModule.getAddOrganizationMembersPropagation();
	}

	/**
	* Sets the add organization members propagation of this ext module.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation of this ext module
	*/
	@Override
	public void setAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation) {
		_extModule.setAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
	}

	/**
	* Returns the add organization members endpoint of this ext module.
	*
	* @return the add organization members endpoint of this ext module
	*/
	@Override
	public java.lang.String getAddOrganizationMembersEndpoint() {
		return _extModule.getAddOrganizationMembersEndpoint();
	}

	/**
	* Sets the add organization members endpoint of this ext module.
	*
	* @param addOrganizationMembersEndpoint the add organization members endpoint of this ext module
	*/
	@Override
	public void setAddOrganizationMembersEndpoint(
		java.lang.String addOrganizationMembersEndpoint) {
		_extModule.setAddOrganizationMembersEndpoint(addOrganizationMembersEndpoint);
	}

	/**
	* Returns the remove organization members propagation of this ext module.
	*
	* @return the remove organization members propagation of this ext module
	*/
	@Override
	public java.lang.Boolean getRemoveOrganizationMembersPropagation() {
		return _extModule.getRemoveOrganizationMembersPropagation();
	}

	/**
	* Sets the remove organization members propagation of this ext module.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation of this ext module
	*/
	@Override
	public void setRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation) {
		_extModule.setRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
	}

	/**
	* Returns the remove organization members endpoint of this ext module.
	*
	* @return the remove organization members endpoint of this ext module
	*/
	@Override
	public java.lang.String getRemoveOrganizationMembersEndpoint() {
		return _extModule.getRemoveOrganizationMembersEndpoint();
	}

	/**
	* Sets the remove organization members endpoint of this ext module.
	*
	* @param removeOrganizationMembersEndpoint the remove organization members endpoint of this ext module
	*/
	@Override
	public void setRemoveOrganizationMembersEndpoint(
		java.lang.String removeOrganizationMembersEndpoint) {
		_extModule.setRemoveOrganizationMembersEndpoint(removeOrganizationMembersEndpoint);
	}

	/**
	* Returns the module alias of this ext module.
	*
	* @return the module alias of this ext module
	*/
	@Override
	public java.lang.String getModuleAlias() {
		return _extModule.getModuleAlias();
	}

	/**
	* Sets the module alias of this ext module.
	*
	* @param moduleAlias the module alias of this ext module
	*/
	@Override
	public void setModuleAlias(java.lang.String moduleAlias) {
		_extModule.setModuleAlias(moduleAlias);
	}

	/**
	* Returns the module shared secret of this ext module.
	*
	* @return the module shared secret of this ext module
	*/
	@Override
	public java.lang.String getModuleSharedSecret() {
		return _extModule.getModuleSharedSecret();
	}

	/**
	* Sets the module shared secret of this ext module.
	*
	* @param moduleSharedSecret the module shared secret of this ext module
	*/
	@Override
	public void setModuleSharedSecret(java.lang.String moduleSharedSecret) {
		_extModule.setModuleSharedSecret(moduleSharedSecret);
	}

	/**
	* Returns the content type of this ext module.
	*
	* @return the content type of this ext module
	*/
	@Override
	public java.lang.String getContentType() {
		return _extModule.getContentType();
	}

	/**
	* Sets the content type of this ext module.
	*
	* @param contentType the content type of this ext module
	*/
	@Override
	public void setContentType(java.lang.String contentType) {
		_extModule.setContentType(contentType);
	}

	@Override
	public boolean isNew() {
		return _extModule.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_extModule.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _extModule.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_extModule.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _extModule.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _extModule.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_extModule.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _extModule.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_extModule.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_extModule.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_extModule.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ExtModuleWrapper((ExtModule)_extModule.clone());
	}

	@Override
	public int compareTo(it.eng.model.ExtModule extModule) {
		return _extModule.compareTo(extModule);
	}

	@Override
	public int hashCode() {
		return _extModule.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.model.ExtModule> toCacheModel() {
		return _extModule.toCacheModel();
	}

	@Override
	public it.eng.model.ExtModule toEscapedModel() {
		return new ExtModuleWrapper(_extModule.toEscapedModel());
	}

	@Override
	public it.eng.model.ExtModule toUnescapedModel() {
		return new ExtModuleWrapper(_extModule.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _extModule.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _extModule.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_extModule.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ExtModuleWrapper)) {
			return false;
		}

		ExtModuleWrapper extModuleWrapper = (ExtModuleWrapper)obj;

		if (Validator.equals(_extModule, extModuleWrapper._extModule)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ExtModule getWrappedExtModule() {
		return _extModule;
	}

	@Override
	public ExtModule getWrappedModel() {
		return _extModule;
	}

	@Override
	public void resetOriginalValues() {
		_extModule.resetOriginalValues();
	}

	private ExtModule _extModule;
}