/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class ExtModuleSoap implements Serializable {
	public static ExtModuleSoap toSoapModel(ExtModule model) {
		ExtModuleSoap soapModel = new ExtModuleSoap();

		soapModel.setModuleId(model.getModuleId());
		soapModel.setModuleName(model.getModuleName());
		soapModel.setModuleURL(model.getModuleURL());
		soapModel.setRegistrationPropagation(model.getRegistrationPropagation());
		soapModel.setModuleEndpoint(model.getModuleEndpoint());
		soapModel.setDeletionPropagation(model.getDeletionPropagation());
		soapModel.setUserDeletionEndpoint(model.getUserDeletionEndpoint());
		soapModel.setUpsertOrganizationPropagation(model.getUpsertOrganizationPropagation());
		soapModel.setUpsertOrganizationEndpoint(model.getUpsertOrganizationEndpoint());
		soapModel.setDeleteOrganizationPropagation(model.getDeleteOrganizationPropagation());
		soapModel.setDeleteOrganizationEndpoint(model.getDeleteOrganizationEndpoint());
		soapModel.setAddOrganizationMembersPropagation(model.getAddOrganizationMembersPropagation());
		soapModel.setAddOrganizationMembersEndpoint(model.getAddOrganizationMembersEndpoint());
		soapModel.setRemoveOrganizationMembersPropagation(model.getRemoveOrganizationMembersPropagation());
		soapModel.setRemoveOrganizationMembersEndpoint(model.getRemoveOrganizationMembersEndpoint());
		soapModel.setModuleAlias(model.getModuleAlias());
		soapModel.setModuleSharedSecret(model.getModuleSharedSecret());
		soapModel.setContentType(model.getContentType());

		return soapModel;
	}

	public static ExtModuleSoap[] toSoapModels(ExtModule[] models) {
		ExtModuleSoap[] soapModels = new ExtModuleSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ExtModuleSoap[][] toSoapModels(ExtModule[][] models) {
		ExtModuleSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ExtModuleSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ExtModuleSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ExtModuleSoap[] toSoapModels(List<ExtModule> models) {
		List<ExtModuleSoap> soapModels = new ArrayList<ExtModuleSoap>(models.size());

		for (ExtModule model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ExtModuleSoap[soapModels.size()]);
	}

	public ExtModuleSoap() {
	}

	public long getPrimaryKey() {
		return _moduleId;
	}

	public void setPrimaryKey(long pk) {
		setModuleId(pk);
	}

	public long getModuleId() {
		return _moduleId;
	}

	public void setModuleId(long moduleId) {
		_moduleId = moduleId;
	}

	public String getModuleName() {
		return _moduleName;
	}

	public void setModuleName(String moduleName) {
		_moduleName = moduleName;
	}

	public String getModuleURL() {
		return _moduleURL;
	}

	public void setModuleURL(String moduleURL) {
		_moduleURL = moduleURL;
	}

	public Boolean getRegistrationPropagation() {
		return _registrationPropagation;
	}

	public void setRegistrationPropagation(Boolean registrationPropagation) {
		_registrationPropagation = registrationPropagation;
	}

	public String getModuleEndpoint() {
		return _moduleEndpoint;
	}

	public void setModuleEndpoint(String moduleEndpoint) {
		_moduleEndpoint = moduleEndpoint;
	}

	public Boolean getDeletionPropagation() {
		return _deletionPropagation;
	}

	public void setDeletionPropagation(Boolean deletionPropagation) {
		_deletionPropagation = deletionPropagation;
	}

	public String getUserDeletionEndpoint() {
		return _userDeletionEndpoint;
	}

	public void setUserDeletionEndpoint(String userDeletionEndpoint) {
		_userDeletionEndpoint = userDeletionEndpoint;
	}

	public Boolean getUpsertOrganizationPropagation() {
		return _upsertOrganizationPropagation;
	}

	public void setUpsertOrganizationPropagation(
		Boolean upsertOrganizationPropagation) {
		_upsertOrganizationPropagation = upsertOrganizationPropagation;
	}

	public String getUpsertOrganizationEndpoint() {
		return _upsertOrganizationEndpoint;
	}

	public void setUpsertOrganizationEndpoint(String upsertOrganizationEndpoint) {
		_upsertOrganizationEndpoint = upsertOrganizationEndpoint;
	}

	public Boolean getDeleteOrganizationPropagation() {
		return _deleteOrganizationPropagation;
	}

	public void setDeleteOrganizationPropagation(
		Boolean deleteOrganizationPropagation) {
		_deleteOrganizationPropagation = deleteOrganizationPropagation;
	}

	public String getDeleteOrganizationEndpoint() {
		return _deleteOrganizationEndpoint;
	}

	public void setDeleteOrganizationEndpoint(String deleteOrganizationEndpoint) {
		_deleteOrganizationEndpoint = deleteOrganizationEndpoint;
	}

	public Boolean getAddOrganizationMembersPropagation() {
		return _addOrganizationMembersPropagation;
	}

	public void setAddOrganizationMembersPropagation(
		Boolean addOrganizationMembersPropagation) {
		_addOrganizationMembersPropagation = addOrganizationMembersPropagation;
	}

	public String getAddOrganizationMembersEndpoint() {
		return _addOrganizationMembersEndpoint;
	}

	public void setAddOrganizationMembersEndpoint(
		String addOrganizationMembersEndpoint) {
		_addOrganizationMembersEndpoint = addOrganizationMembersEndpoint;
	}

	public Boolean getRemoveOrganizationMembersPropagation() {
		return _removeOrganizationMembersPropagation;
	}

	public void setRemoveOrganizationMembersPropagation(
		Boolean removeOrganizationMembersPropagation) {
		_removeOrganizationMembersPropagation = removeOrganizationMembersPropagation;
	}

	public String getRemoveOrganizationMembersEndpoint() {
		return _removeOrganizationMembersEndpoint;
	}

	public void setRemoveOrganizationMembersEndpoint(
		String removeOrganizationMembersEndpoint) {
		_removeOrganizationMembersEndpoint = removeOrganizationMembersEndpoint;
	}

	public String getModuleAlias() {
		return _moduleAlias;
	}

	public void setModuleAlias(String moduleAlias) {
		_moduleAlias = moduleAlias;
	}

	public String getModuleSharedSecret() {
		return _moduleSharedSecret;
	}

	public void setModuleSharedSecret(String moduleSharedSecret) {
		_moduleSharedSecret = moduleSharedSecret;
	}

	public String getContentType() {
		return _contentType;
	}

	public void setContentType(String contentType) {
		_contentType = contentType;
	}

	private long _moduleId;
	private String _moduleName;
	private String _moduleURL;
	private Boolean _registrationPropagation;
	private String _moduleEndpoint;
	private Boolean _deletionPropagation;
	private String _userDeletionEndpoint;
	private Boolean _upsertOrganizationPropagation;
	private String _upsertOrganizationEndpoint;
	private Boolean _deleteOrganizationPropagation;
	private String _deleteOrganizationEndpoint;
	private Boolean _addOrganizationMembersPropagation;
	private String _addOrganizationMembersEndpoint;
	private Boolean _removeOrganizationMembersPropagation;
	private String _removeOrganizationMembersEndpoint;
	private String _moduleAlias;
	private String _moduleSharedSecret;
	private String _contentType;
}