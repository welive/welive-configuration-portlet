/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PortalConf}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see PortalConf
 * @generated
 */
public class PortalConfWrapper implements PortalConf, ModelWrapper<PortalConf> {
	public PortalConfWrapper(PortalConf portalConf) {
		_portalConf = portalConf;
	}

	@Override
	public Class<?> getModelClass() {
		return PortalConf.class;
	}

	@Override
	public String getModelClassName() {
		return PortalConf.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("confName", getConfName());
		attributes.put("tomcatKsAlias", getTomcatKsAlias());
		attributes.put("tomcatKsPassword", getTomcatKsPassword());
		attributes.put("tomcatKsLocation", getTomcatKsLocation());
		attributes.put("jvmKsLocation", getJvmKsLocation());
		attributes.put("jvmKsPassword", getJvmKsPassword());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String confName = (String)attributes.get("confName");

		if (confName != null) {
			setConfName(confName);
		}

		String tomcatKsAlias = (String)attributes.get("tomcatKsAlias");

		if (tomcatKsAlias != null) {
			setTomcatKsAlias(tomcatKsAlias);
		}

		String tomcatKsPassword = (String)attributes.get("tomcatKsPassword");

		if (tomcatKsPassword != null) {
			setTomcatKsPassword(tomcatKsPassword);
		}

		String tomcatKsLocation = (String)attributes.get("tomcatKsLocation");

		if (tomcatKsLocation != null) {
			setTomcatKsLocation(tomcatKsLocation);
		}

		String jvmKsLocation = (String)attributes.get("jvmKsLocation");

		if (jvmKsLocation != null) {
			setJvmKsLocation(jvmKsLocation);
		}

		String jvmKsPassword = (String)attributes.get("jvmKsPassword");

		if (jvmKsPassword != null) {
			setJvmKsPassword(jvmKsPassword);
		}
	}

	/**
	* Returns the primary key of this portal conf.
	*
	* @return the primary key of this portal conf
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _portalConf.getPrimaryKey();
	}

	/**
	* Sets the primary key of this portal conf.
	*
	* @param primaryKey the primary key of this portal conf
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_portalConf.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the conf name of this portal conf.
	*
	* @return the conf name of this portal conf
	*/
	@Override
	public java.lang.String getConfName() {
		return _portalConf.getConfName();
	}

	/**
	* Sets the conf name of this portal conf.
	*
	* @param confName the conf name of this portal conf
	*/
	@Override
	public void setConfName(java.lang.String confName) {
		_portalConf.setConfName(confName);
	}

	/**
	* Returns the tomcat ks alias of this portal conf.
	*
	* @return the tomcat ks alias of this portal conf
	*/
	@Override
	public java.lang.String getTomcatKsAlias() {
		return _portalConf.getTomcatKsAlias();
	}

	/**
	* Sets the tomcat ks alias of this portal conf.
	*
	* @param tomcatKsAlias the tomcat ks alias of this portal conf
	*/
	@Override
	public void setTomcatKsAlias(java.lang.String tomcatKsAlias) {
		_portalConf.setTomcatKsAlias(tomcatKsAlias);
	}

	/**
	* Returns the tomcat ks password of this portal conf.
	*
	* @return the tomcat ks password of this portal conf
	*/
	@Override
	public java.lang.String getTomcatKsPassword() {
		return _portalConf.getTomcatKsPassword();
	}

	/**
	* Sets the tomcat ks password of this portal conf.
	*
	* @param tomcatKsPassword the tomcat ks password of this portal conf
	*/
	@Override
	public void setTomcatKsPassword(java.lang.String tomcatKsPassword) {
		_portalConf.setTomcatKsPassword(tomcatKsPassword);
	}

	/**
	* Returns the tomcat ks location of this portal conf.
	*
	* @return the tomcat ks location of this portal conf
	*/
	@Override
	public java.lang.String getTomcatKsLocation() {
		return _portalConf.getTomcatKsLocation();
	}

	/**
	* Sets the tomcat ks location of this portal conf.
	*
	* @param tomcatKsLocation the tomcat ks location of this portal conf
	*/
	@Override
	public void setTomcatKsLocation(java.lang.String tomcatKsLocation) {
		_portalConf.setTomcatKsLocation(tomcatKsLocation);
	}

	/**
	* Returns the jvm ks location of this portal conf.
	*
	* @return the jvm ks location of this portal conf
	*/
	@Override
	public java.lang.String getJvmKsLocation() {
		return _portalConf.getJvmKsLocation();
	}

	/**
	* Sets the jvm ks location of this portal conf.
	*
	* @param jvmKsLocation the jvm ks location of this portal conf
	*/
	@Override
	public void setJvmKsLocation(java.lang.String jvmKsLocation) {
		_portalConf.setJvmKsLocation(jvmKsLocation);
	}

	/**
	* Returns the jvm ks password of this portal conf.
	*
	* @return the jvm ks password of this portal conf
	*/
	@Override
	public java.lang.String getJvmKsPassword() {
		return _portalConf.getJvmKsPassword();
	}

	/**
	* Sets the jvm ks password of this portal conf.
	*
	* @param jvmKsPassword the jvm ks password of this portal conf
	*/
	@Override
	public void setJvmKsPassword(java.lang.String jvmKsPassword) {
		_portalConf.setJvmKsPassword(jvmKsPassword);
	}

	@Override
	public boolean isNew() {
		return _portalConf.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_portalConf.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _portalConf.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_portalConf.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _portalConf.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _portalConf.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_portalConf.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _portalConf.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_portalConf.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_portalConf.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_portalConf.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PortalConfWrapper((PortalConf)_portalConf.clone());
	}

	@Override
	public int compareTo(it.eng.model.PortalConf portalConf) {
		return _portalConf.compareTo(portalConf);
	}

	@Override
	public int hashCode() {
		return _portalConf.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.model.PortalConf> toCacheModel() {
		return _portalConf.toCacheModel();
	}

	@Override
	public it.eng.model.PortalConf toEscapedModel() {
		return new PortalConfWrapper(_portalConf.toEscapedModel());
	}

	@Override
	public it.eng.model.PortalConf toUnescapedModel() {
		return new PortalConfWrapper(_portalConf.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _portalConf.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _portalConf.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_portalConf.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PortalConfWrapper)) {
			return false;
		}

		PortalConfWrapper portalConfWrapper = (PortalConfWrapper)obj;

		if (Validator.equals(_portalConf, portalConfWrapper._portalConf)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public PortalConf getWrappedPortalConf() {
		return _portalConf;
	}

	@Override
	public PortalConf getWrappedModel() {
		return _portalConf;
	}

	@Override
	public void resetOriginalValues() {
		_portalConf.resetOriginalValues();
	}

	private PortalConf _portalConf;
}