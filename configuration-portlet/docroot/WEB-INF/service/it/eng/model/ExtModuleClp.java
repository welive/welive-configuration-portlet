/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.service.ClpSerializer;
import it.eng.service.ExtModuleLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class ExtModuleClp extends BaseModelImpl<ExtModule> implements ExtModule {
	public ExtModuleClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ExtModule.class;
	}

	@Override
	public String getModelClassName() {
		return ExtModule.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _moduleId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setModuleId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _moduleId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("moduleId", getModuleId());
		attributes.put("moduleName", getModuleName());
		attributes.put("moduleURL", getModuleURL());
		attributes.put("registrationPropagation", getRegistrationPropagation());
		attributes.put("moduleEndpoint", getModuleEndpoint());
		attributes.put("deletionPropagation", getDeletionPropagation());
		attributes.put("userDeletionEndpoint", getUserDeletionEndpoint());
		attributes.put("upsertOrganizationPropagation",
			getUpsertOrganizationPropagation());
		attributes.put("upsertOrganizationEndpoint",
			getUpsertOrganizationEndpoint());
		attributes.put("deleteOrganizationPropagation",
			getDeleteOrganizationPropagation());
		attributes.put("deleteOrganizationEndpoint",
			getDeleteOrganizationEndpoint());
		attributes.put("addOrganizationMembersPropagation",
			getAddOrganizationMembersPropagation());
		attributes.put("addOrganizationMembersEndpoint",
			getAddOrganizationMembersEndpoint());
		attributes.put("removeOrganizationMembersPropagation",
			getRemoveOrganizationMembersPropagation());
		attributes.put("removeOrganizationMembersEndpoint",
			getRemoveOrganizationMembersEndpoint());
		attributes.put("moduleAlias", getModuleAlias());
		attributes.put("moduleSharedSecret", getModuleSharedSecret());
		attributes.put("contentType", getContentType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long moduleId = (Long)attributes.get("moduleId");

		if (moduleId != null) {
			setModuleId(moduleId);
		}

		String moduleName = (String)attributes.get("moduleName");

		if (moduleName != null) {
			setModuleName(moduleName);
		}

		String moduleURL = (String)attributes.get("moduleURL");

		if (moduleURL != null) {
			setModuleURL(moduleURL);
		}

		Boolean registrationPropagation = (Boolean)attributes.get(
				"registrationPropagation");

		if (registrationPropagation != null) {
			setRegistrationPropagation(registrationPropagation);
		}

		String moduleEndpoint = (String)attributes.get("moduleEndpoint");

		if (moduleEndpoint != null) {
			setModuleEndpoint(moduleEndpoint);
		}

		Boolean deletionPropagation = (Boolean)attributes.get(
				"deletionPropagation");

		if (deletionPropagation != null) {
			setDeletionPropagation(deletionPropagation);
		}

		String userDeletionEndpoint = (String)attributes.get(
				"userDeletionEndpoint");

		if (userDeletionEndpoint != null) {
			setUserDeletionEndpoint(userDeletionEndpoint);
		}

		Boolean upsertOrganizationPropagation = (Boolean)attributes.get(
				"upsertOrganizationPropagation");

		if (upsertOrganizationPropagation != null) {
			setUpsertOrganizationPropagation(upsertOrganizationPropagation);
		}

		String upsertOrganizationEndpoint = (String)attributes.get(
				"upsertOrganizationEndpoint");

		if (upsertOrganizationEndpoint != null) {
			setUpsertOrganizationEndpoint(upsertOrganizationEndpoint);
		}

		Boolean deleteOrganizationPropagation = (Boolean)attributes.get(
				"deleteOrganizationPropagation");

		if (deleteOrganizationPropagation != null) {
			setDeleteOrganizationPropagation(deleteOrganizationPropagation);
		}

		String deleteOrganizationEndpoint = (String)attributes.get(
				"deleteOrganizationEndpoint");

		if (deleteOrganizationEndpoint != null) {
			setDeleteOrganizationEndpoint(deleteOrganizationEndpoint);
		}

		Boolean addOrganizationMembersPropagation = (Boolean)attributes.get(
				"addOrganizationMembersPropagation");

		if (addOrganizationMembersPropagation != null) {
			setAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
		}

		String addOrganizationMembersEndpoint = (String)attributes.get(
				"addOrganizationMembersEndpoint");

		if (addOrganizationMembersEndpoint != null) {
			setAddOrganizationMembersEndpoint(addOrganizationMembersEndpoint);
		}

		Boolean removeOrganizationMembersPropagation = (Boolean)attributes.get(
				"removeOrganizationMembersPropagation");

		if (removeOrganizationMembersPropagation != null) {
			setRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
		}

		String removeOrganizationMembersEndpoint = (String)attributes.get(
				"removeOrganizationMembersEndpoint");

		if (removeOrganizationMembersEndpoint != null) {
			setRemoveOrganizationMembersEndpoint(removeOrganizationMembersEndpoint);
		}

		String moduleAlias = (String)attributes.get("moduleAlias");

		if (moduleAlias != null) {
			setModuleAlias(moduleAlias);
		}

		String moduleSharedSecret = (String)attributes.get("moduleSharedSecret");

		if (moduleSharedSecret != null) {
			setModuleSharedSecret(moduleSharedSecret);
		}

		String contentType = (String)attributes.get("contentType");

		if (contentType != null) {
			setContentType(contentType);
		}
	}

	@Override
	public long getModuleId() {
		return _moduleId;
	}

	@Override
	public void setModuleId(long moduleId) {
		_moduleId = moduleId;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setModuleId", long.class);

				method.invoke(_extModuleRemoteModel, moduleId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getModuleName() {
		return _moduleName;
	}

	@Override
	public void setModuleName(String moduleName) {
		_moduleName = moduleName;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setModuleName", String.class);

				method.invoke(_extModuleRemoteModel, moduleName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getModuleURL() {
		return _moduleURL;
	}

	@Override
	public void setModuleURL(String moduleURL) {
		_moduleURL = moduleURL;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setModuleURL", String.class);

				method.invoke(_extModuleRemoteModel, moduleURL);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Boolean getRegistrationPropagation() {
		return _registrationPropagation;
	}

	@Override
	public void setRegistrationPropagation(Boolean registrationPropagation) {
		_registrationPropagation = registrationPropagation;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setRegistrationPropagation",
						Boolean.class);

				method.invoke(_extModuleRemoteModel, registrationPropagation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getModuleEndpoint() {
		return _moduleEndpoint;
	}

	@Override
	public void setModuleEndpoint(String moduleEndpoint) {
		_moduleEndpoint = moduleEndpoint;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setModuleEndpoint",
						String.class);

				method.invoke(_extModuleRemoteModel, moduleEndpoint);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Boolean getDeletionPropagation() {
		return _deletionPropagation;
	}

	@Override
	public void setDeletionPropagation(Boolean deletionPropagation) {
		_deletionPropagation = deletionPropagation;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setDeletionPropagation",
						Boolean.class);

				method.invoke(_extModuleRemoteModel, deletionPropagation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserDeletionEndpoint() {
		return _userDeletionEndpoint;
	}

	@Override
	public void setUserDeletionEndpoint(String userDeletionEndpoint) {
		_userDeletionEndpoint = userDeletionEndpoint;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setUserDeletionEndpoint",
						String.class);

				method.invoke(_extModuleRemoteModel, userDeletionEndpoint);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Boolean getUpsertOrganizationPropagation() {
		return _upsertOrganizationPropagation;
	}

	@Override
	public void setUpsertOrganizationPropagation(
		Boolean upsertOrganizationPropagation) {
		_upsertOrganizationPropagation = upsertOrganizationPropagation;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setUpsertOrganizationPropagation",
						Boolean.class);

				method.invoke(_extModuleRemoteModel,
					upsertOrganizationPropagation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUpsertOrganizationEndpoint() {
		return _upsertOrganizationEndpoint;
	}

	@Override
	public void setUpsertOrganizationEndpoint(String upsertOrganizationEndpoint) {
		_upsertOrganizationEndpoint = upsertOrganizationEndpoint;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setUpsertOrganizationEndpoint",
						String.class);

				method.invoke(_extModuleRemoteModel, upsertOrganizationEndpoint);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Boolean getDeleteOrganizationPropagation() {
		return _deleteOrganizationPropagation;
	}

	@Override
	public void setDeleteOrganizationPropagation(
		Boolean deleteOrganizationPropagation) {
		_deleteOrganizationPropagation = deleteOrganizationPropagation;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setDeleteOrganizationPropagation",
						Boolean.class);

				method.invoke(_extModuleRemoteModel,
					deleteOrganizationPropagation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDeleteOrganizationEndpoint() {
		return _deleteOrganizationEndpoint;
	}

	@Override
	public void setDeleteOrganizationEndpoint(String deleteOrganizationEndpoint) {
		_deleteOrganizationEndpoint = deleteOrganizationEndpoint;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setDeleteOrganizationEndpoint",
						String.class);

				method.invoke(_extModuleRemoteModel, deleteOrganizationEndpoint);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Boolean getAddOrganizationMembersPropagation() {
		return _addOrganizationMembersPropagation;
	}

	@Override
	public void setAddOrganizationMembersPropagation(
		Boolean addOrganizationMembersPropagation) {
		_addOrganizationMembersPropagation = addOrganizationMembersPropagation;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setAddOrganizationMembersPropagation",
						Boolean.class);

				method.invoke(_extModuleRemoteModel,
					addOrganizationMembersPropagation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAddOrganizationMembersEndpoint() {
		return _addOrganizationMembersEndpoint;
	}

	@Override
	public void setAddOrganizationMembersEndpoint(
		String addOrganizationMembersEndpoint) {
		_addOrganizationMembersEndpoint = addOrganizationMembersEndpoint;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setAddOrganizationMembersEndpoint",
						String.class);

				method.invoke(_extModuleRemoteModel,
					addOrganizationMembersEndpoint);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Boolean getRemoveOrganizationMembersPropagation() {
		return _removeOrganizationMembersPropagation;
	}

	@Override
	public void setRemoveOrganizationMembersPropagation(
		Boolean removeOrganizationMembersPropagation) {
		_removeOrganizationMembersPropagation = removeOrganizationMembersPropagation;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setRemoveOrganizationMembersPropagation",
						Boolean.class);

				method.invoke(_extModuleRemoteModel,
					removeOrganizationMembersPropagation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRemoveOrganizationMembersEndpoint() {
		return _removeOrganizationMembersEndpoint;
	}

	@Override
	public void setRemoveOrganizationMembersEndpoint(
		String removeOrganizationMembersEndpoint) {
		_removeOrganizationMembersEndpoint = removeOrganizationMembersEndpoint;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setRemoveOrganizationMembersEndpoint",
						String.class);

				method.invoke(_extModuleRemoteModel,
					removeOrganizationMembersEndpoint);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getModuleAlias() {
		return _moduleAlias;
	}

	@Override
	public void setModuleAlias(String moduleAlias) {
		_moduleAlias = moduleAlias;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setModuleAlias", String.class);

				method.invoke(_extModuleRemoteModel, moduleAlias);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getModuleSharedSecret() {
		return _moduleSharedSecret;
	}

	@Override
	public void setModuleSharedSecret(String moduleSharedSecret) {
		_moduleSharedSecret = moduleSharedSecret;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setModuleSharedSecret",
						String.class);

				method.invoke(_extModuleRemoteModel, moduleSharedSecret);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getContentType() {
		return _contentType;
	}

	@Override
	public void setContentType(String contentType) {
		_contentType = contentType;

		if (_extModuleRemoteModel != null) {
			try {
				Class<?> clazz = _extModuleRemoteModel.getClass();

				Method method = clazz.getMethod("setContentType", String.class);

				method.invoke(_extModuleRemoteModel, contentType);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getExtModuleRemoteModel() {
		return _extModuleRemoteModel;
	}

	public void setExtModuleRemoteModel(BaseModel<?> extModuleRemoteModel) {
		_extModuleRemoteModel = extModuleRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _extModuleRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_extModuleRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ExtModuleLocalServiceUtil.addExtModule(this);
		}
		else {
			ExtModuleLocalServiceUtil.updateExtModule(this);
		}
	}

	@Override
	public ExtModule toEscapedModel() {
		return (ExtModule)ProxyUtil.newProxyInstance(ExtModule.class.getClassLoader(),
			new Class[] { ExtModule.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ExtModuleClp clone = new ExtModuleClp();

		clone.setModuleId(getModuleId());
		clone.setModuleName(getModuleName());
		clone.setModuleURL(getModuleURL());
		clone.setRegistrationPropagation(getRegistrationPropagation());
		clone.setModuleEndpoint(getModuleEndpoint());
		clone.setDeletionPropagation(getDeletionPropagation());
		clone.setUserDeletionEndpoint(getUserDeletionEndpoint());
		clone.setUpsertOrganizationPropagation(getUpsertOrganizationPropagation());
		clone.setUpsertOrganizationEndpoint(getUpsertOrganizationEndpoint());
		clone.setDeleteOrganizationPropagation(getDeleteOrganizationPropagation());
		clone.setDeleteOrganizationEndpoint(getDeleteOrganizationEndpoint());
		clone.setAddOrganizationMembersPropagation(getAddOrganizationMembersPropagation());
		clone.setAddOrganizationMembersEndpoint(getAddOrganizationMembersEndpoint());
		clone.setRemoveOrganizationMembersPropagation(getRemoveOrganizationMembersPropagation());
		clone.setRemoveOrganizationMembersEndpoint(getRemoveOrganizationMembersEndpoint());
		clone.setModuleAlias(getModuleAlias());
		clone.setModuleSharedSecret(getModuleSharedSecret());
		clone.setContentType(getContentType());

		return clone;
	}

	@Override
	public int compareTo(ExtModule extModule) {
		int value = 0;

		value = getModuleName().compareTo(extModule.getModuleName());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ExtModuleClp)) {
			return false;
		}

		ExtModuleClp extModule = (ExtModuleClp)obj;

		long primaryKey = extModule.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(37);

		sb.append("{moduleId=");
		sb.append(getModuleId());
		sb.append(", moduleName=");
		sb.append(getModuleName());
		sb.append(", moduleURL=");
		sb.append(getModuleURL());
		sb.append(", registrationPropagation=");
		sb.append(getRegistrationPropagation());
		sb.append(", moduleEndpoint=");
		sb.append(getModuleEndpoint());
		sb.append(", deletionPropagation=");
		sb.append(getDeletionPropagation());
		sb.append(", userDeletionEndpoint=");
		sb.append(getUserDeletionEndpoint());
		sb.append(", upsertOrganizationPropagation=");
		sb.append(getUpsertOrganizationPropagation());
		sb.append(", upsertOrganizationEndpoint=");
		sb.append(getUpsertOrganizationEndpoint());
		sb.append(", deleteOrganizationPropagation=");
		sb.append(getDeleteOrganizationPropagation());
		sb.append(", deleteOrganizationEndpoint=");
		sb.append(getDeleteOrganizationEndpoint());
		sb.append(", addOrganizationMembersPropagation=");
		sb.append(getAddOrganizationMembersPropagation());
		sb.append(", addOrganizationMembersEndpoint=");
		sb.append(getAddOrganizationMembersEndpoint());
		sb.append(", removeOrganizationMembersPropagation=");
		sb.append(getRemoveOrganizationMembersPropagation());
		sb.append(", removeOrganizationMembersEndpoint=");
		sb.append(getRemoveOrganizationMembersEndpoint());
		sb.append(", moduleAlias=");
		sb.append(getModuleAlias());
		sb.append(", moduleSharedSecret=");
		sb.append(getModuleSharedSecret());
		sb.append(", contentType=");
		sb.append(getContentType());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(58);

		sb.append("<model><model-name>");
		sb.append("it.eng.model.ExtModule");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>moduleId</column-name><column-value><![CDATA[");
		sb.append(getModuleId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>moduleName</column-name><column-value><![CDATA[");
		sb.append(getModuleName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>moduleURL</column-name><column-value><![CDATA[");
		sb.append(getModuleURL());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>registrationPropagation</column-name><column-value><![CDATA[");
		sb.append(getRegistrationPropagation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>moduleEndpoint</column-name><column-value><![CDATA[");
		sb.append(getModuleEndpoint());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>deletionPropagation</column-name><column-value><![CDATA[");
		sb.append(getDeletionPropagation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userDeletionEndpoint</column-name><column-value><![CDATA[");
		sb.append(getUserDeletionEndpoint());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>upsertOrganizationPropagation</column-name><column-value><![CDATA[");
		sb.append(getUpsertOrganizationPropagation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>upsertOrganizationEndpoint</column-name><column-value><![CDATA[");
		sb.append(getUpsertOrganizationEndpoint());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>deleteOrganizationPropagation</column-name><column-value><![CDATA[");
		sb.append(getDeleteOrganizationPropagation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>deleteOrganizationEndpoint</column-name><column-value><![CDATA[");
		sb.append(getDeleteOrganizationEndpoint());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addOrganizationMembersPropagation</column-name><column-value><![CDATA[");
		sb.append(getAddOrganizationMembersPropagation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addOrganizationMembersEndpoint</column-name><column-value><![CDATA[");
		sb.append(getAddOrganizationMembersEndpoint());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>removeOrganizationMembersPropagation</column-name><column-value><![CDATA[");
		sb.append(getRemoveOrganizationMembersPropagation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>removeOrganizationMembersEndpoint</column-name><column-value><![CDATA[");
		sb.append(getRemoveOrganizationMembersEndpoint());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>moduleAlias</column-name><column-value><![CDATA[");
		sb.append(getModuleAlias());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>moduleSharedSecret</column-name><column-value><![CDATA[");
		sb.append(getModuleSharedSecret());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>contentType</column-name><column-value><![CDATA[");
		sb.append(getContentType());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _moduleId;
	private String _moduleName;
	private String _moduleURL;
	private Boolean _registrationPropagation;
	private String _moduleEndpoint;
	private Boolean _deletionPropagation;
	private String _userDeletionEndpoint;
	private Boolean _upsertOrganizationPropagation;
	private String _upsertOrganizationEndpoint;
	private Boolean _deleteOrganizationPropagation;
	private String _deleteOrganizationEndpoint;
	private Boolean _addOrganizationMembersPropagation;
	private String _addOrganizationMembersEndpoint;
	private Boolean _removeOrganizationMembersPropagation;
	private String _removeOrganizationMembersEndpoint;
	private String _moduleAlias;
	private String _moduleSharedSecret;
	private String _contentType;
	private BaseModel<?> _extModuleRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.service.ClpSerializer.class;
}