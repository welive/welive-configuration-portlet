/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.service.ClpSerializer;
import it.eng.service.PortalConfLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class PortalConfClp extends BaseModelImpl<PortalConf>
	implements PortalConf {
	public PortalConfClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return PortalConf.class;
	}

	@Override
	public String getModelClassName() {
		return PortalConf.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _confName;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setConfName(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _confName;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("confName", getConfName());
		attributes.put("tomcatKsAlias", getTomcatKsAlias());
		attributes.put("tomcatKsPassword", getTomcatKsPassword());
		attributes.put("tomcatKsLocation", getTomcatKsLocation());
		attributes.put("jvmKsLocation", getJvmKsLocation());
		attributes.put("jvmKsPassword", getJvmKsPassword());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String confName = (String)attributes.get("confName");

		if (confName != null) {
			setConfName(confName);
		}

		String tomcatKsAlias = (String)attributes.get("tomcatKsAlias");

		if (tomcatKsAlias != null) {
			setTomcatKsAlias(tomcatKsAlias);
		}

		String tomcatKsPassword = (String)attributes.get("tomcatKsPassword");

		if (tomcatKsPassword != null) {
			setTomcatKsPassword(tomcatKsPassword);
		}

		String tomcatKsLocation = (String)attributes.get("tomcatKsLocation");

		if (tomcatKsLocation != null) {
			setTomcatKsLocation(tomcatKsLocation);
		}

		String jvmKsLocation = (String)attributes.get("jvmKsLocation");

		if (jvmKsLocation != null) {
			setJvmKsLocation(jvmKsLocation);
		}

		String jvmKsPassword = (String)attributes.get("jvmKsPassword");

		if (jvmKsPassword != null) {
			setJvmKsPassword(jvmKsPassword);
		}
	}

	@Override
	public String getConfName() {
		return _confName;
	}

	@Override
	public void setConfName(String confName) {
		_confName = confName;

		if (_portalConfRemoteModel != null) {
			try {
				Class<?> clazz = _portalConfRemoteModel.getClass();

				Method method = clazz.getMethod("setConfName", String.class);

				method.invoke(_portalConfRemoteModel, confName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTomcatKsAlias() {
		return _tomcatKsAlias;
	}

	@Override
	public void setTomcatKsAlias(String tomcatKsAlias) {
		_tomcatKsAlias = tomcatKsAlias;

		if (_portalConfRemoteModel != null) {
			try {
				Class<?> clazz = _portalConfRemoteModel.getClass();

				Method method = clazz.getMethod("setTomcatKsAlias", String.class);

				method.invoke(_portalConfRemoteModel, tomcatKsAlias);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTomcatKsPassword() {
		return _tomcatKsPassword;
	}

	@Override
	public void setTomcatKsPassword(String tomcatKsPassword) {
		_tomcatKsPassword = tomcatKsPassword;

		if (_portalConfRemoteModel != null) {
			try {
				Class<?> clazz = _portalConfRemoteModel.getClass();

				Method method = clazz.getMethod("setTomcatKsPassword",
						String.class);

				method.invoke(_portalConfRemoteModel, tomcatKsPassword);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTomcatKsLocation() {
		return _tomcatKsLocation;
	}

	@Override
	public void setTomcatKsLocation(String tomcatKsLocation) {
		_tomcatKsLocation = tomcatKsLocation;

		if (_portalConfRemoteModel != null) {
			try {
				Class<?> clazz = _portalConfRemoteModel.getClass();

				Method method = clazz.getMethod("setTomcatKsLocation",
						String.class);

				method.invoke(_portalConfRemoteModel, tomcatKsLocation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getJvmKsLocation() {
		return _jvmKsLocation;
	}

	@Override
	public void setJvmKsLocation(String jvmKsLocation) {
		_jvmKsLocation = jvmKsLocation;

		if (_portalConfRemoteModel != null) {
			try {
				Class<?> clazz = _portalConfRemoteModel.getClass();

				Method method = clazz.getMethod("setJvmKsLocation", String.class);

				method.invoke(_portalConfRemoteModel, jvmKsLocation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getJvmKsPassword() {
		return _jvmKsPassword;
	}

	@Override
	public void setJvmKsPassword(String jvmKsPassword) {
		_jvmKsPassword = jvmKsPassword;

		if (_portalConfRemoteModel != null) {
			try {
				Class<?> clazz = _portalConfRemoteModel.getClass();

				Method method = clazz.getMethod("setJvmKsPassword", String.class);

				method.invoke(_portalConfRemoteModel, jvmKsPassword);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getPortalConfRemoteModel() {
		return _portalConfRemoteModel;
	}

	public void setPortalConfRemoteModel(BaseModel<?> portalConfRemoteModel) {
		_portalConfRemoteModel = portalConfRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _portalConfRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_portalConfRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			PortalConfLocalServiceUtil.addPortalConf(this);
		}
		else {
			PortalConfLocalServiceUtil.updatePortalConf(this);
		}
	}

	@Override
	public PortalConf toEscapedModel() {
		return (PortalConf)ProxyUtil.newProxyInstance(PortalConf.class.getClassLoader(),
			new Class[] { PortalConf.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		PortalConfClp clone = new PortalConfClp();

		clone.setConfName(getConfName());
		clone.setTomcatKsAlias(getTomcatKsAlias());
		clone.setTomcatKsPassword(getTomcatKsPassword());
		clone.setTomcatKsLocation(getTomcatKsLocation());
		clone.setJvmKsLocation(getJvmKsLocation());
		clone.setJvmKsPassword(getJvmKsPassword());

		return clone;
	}

	@Override
	public int compareTo(PortalConf portalConf) {
		int value = 0;

		value = getConfName().compareTo(portalConf.getConfName());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PortalConfClp)) {
			return false;
		}

		PortalConfClp portalConf = (PortalConfClp)obj;

		String primaryKey = portalConf.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{confName=");
		sb.append(getConfName());
		sb.append(", tomcatKsAlias=");
		sb.append(getTomcatKsAlias());
		sb.append(", tomcatKsPassword=");
		sb.append(getTomcatKsPassword());
		sb.append(", tomcatKsLocation=");
		sb.append(getTomcatKsLocation());
		sb.append(", jvmKsLocation=");
		sb.append(getJvmKsLocation());
		sb.append(", jvmKsPassword=");
		sb.append(getJvmKsPassword());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("it.eng.model.PortalConf");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>confName</column-name><column-value><![CDATA[");
		sb.append(getConfName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tomcatKsAlias</column-name><column-value><![CDATA[");
		sb.append(getTomcatKsAlias());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tomcatKsPassword</column-name><column-value><![CDATA[");
		sb.append(getTomcatKsPassword());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tomcatKsLocation</column-name><column-value><![CDATA[");
		sb.append(getTomcatKsLocation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>jvmKsLocation</column-name><column-value><![CDATA[");
		sb.append(getJvmKsLocation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>jvmKsPassword</column-name><column-value><![CDATA[");
		sb.append(getJvmKsPassword());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _confName;
	private String _tomcatKsAlias;
	private String _tomcatKsPassword;
	private String _tomcatKsLocation;
	private String _jvmKsLocation;
	private String _jvmKsPassword;
	private BaseModel<?> _portalConfRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.service.ClpSerializer.class;
}