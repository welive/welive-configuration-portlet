/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for ExtModule. This utility wraps
 * {@link it.eng.service.impl.ExtModuleLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see ExtModuleLocalService
 * @see it.eng.service.base.ExtModuleLocalServiceBaseImpl
 * @see it.eng.service.impl.ExtModuleLocalServiceImpl
 * @generated
 */
public class ExtModuleLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.service.impl.ExtModuleLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the ext module to the database. Also notifies the appropriate model listeners.
	*
	* @param extModule the ext module
	* @return the ext module that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule addExtModule(
		it.eng.model.ExtModule extModule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addExtModule(extModule);
	}

	/**
	* Creates a new ext module with the primary key. Does not add the ext module to the database.
	*
	* @param moduleId the primary key for the new ext module
	* @return the new ext module
	*/
	public static it.eng.model.ExtModule createExtModule(long moduleId) {
		return getService().createExtModule(moduleId);
	}

	/**
	* Deletes the ext module with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module that was removed
	* @throws PortalException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule deleteExtModule(long moduleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteExtModule(moduleId);
	}

	/**
	* Deletes the ext module from the database. Also notifies the appropriate model listeners.
	*
	* @param extModule the ext module
	* @return the ext module that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule deleteExtModule(
		it.eng.model.ExtModule extModule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteExtModule(extModule);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.model.ExtModule fetchExtModule(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchExtModule(moduleId);
	}

	/**
	* Returns the ext module with the primary key.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module
	* @throws PortalException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule getExtModule(long moduleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModule(moduleId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the ext modules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> getExtModules(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModules(start, end);
	}

	/**
	* Returns the number of ext modules.
	*
	* @return the number of ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int getExtModulesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModulesCount();
	}

	/**
	* Updates the ext module in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param extModule the ext module
	* @return the ext module that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule updateExtModule(
		it.eng.model.ExtModule extModule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateExtModule(extModule);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static it.eng.model.ExtModule addExtModule(
		java.lang.String moduleName, java.lang.String moduleURL,
		boolean registrationPropagation, java.lang.String moduleEndpoint,
		boolean deletiontionPropagation, java.lang.String userDeletionEndpoint,
		boolean upsertOrganizationPropagation,
		java.lang.String upsertOrganizationEndpoint,
		boolean deleteOrganizationPropagation,
		java.lang.String deleteOrganizationEndpoint,
		boolean addOrganizationMembersPropagation,
		java.lang.String addOrganizationMembersEndpoint,
		boolean removeOrganizationMembersPropagation,
		java.lang.String removeOrganizationMembersEndpoint,
		java.lang.String moduleAlias, java.lang.String moduleSharedSecret,
		java.lang.String moduleContentType)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .addExtModule(moduleName, moduleURL,
			registrationPropagation, moduleEndpoint, deletiontionPropagation,
			userDeletionEndpoint, upsertOrganizationPropagation,
			upsertOrganizationEndpoint, deleteOrganizationPropagation,
			deleteOrganizationEndpoint, addOrganizationMembersPropagation,
			addOrganizationMembersEndpoint,
			removeOrganizationMembersPropagation,
			removeOrganizationMembersEndpoint, moduleAlias, moduleSharedSecret,
			moduleContentType);
	}

	public static it.eng.model.ExtModule updateExtModule(long moduleId,
		java.lang.String moduleName, java.lang.String moduleURL,
		boolean registrationPropagation, java.lang.String moduleEndpoint,
		boolean deletiontionPropagation, java.lang.String userDeletionEndpoint,
		boolean upsertOrganizationPropagation,
		java.lang.String upsertOrganizationEndpoint,
		boolean deleteOrganizationPropagation,
		java.lang.String deleteOrganizationEndpoint,
		boolean addOrganizationMembersPropagation,
		java.lang.String addOrganizationMembersEndpoint,
		boolean removeOrganizationMembersPropagation,
		java.lang.String removeOrganizationMembersEndpoint,
		java.lang.String moduleAlias, java.lang.String moduleSharedSecret,
		java.lang.String moduleContentType)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .updateExtModule(moduleId, moduleName, moduleURL,
			registrationPropagation, moduleEndpoint, deletiontionPropagation,
			userDeletionEndpoint, upsertOrganizationPropagation,
			upsertOrganizationEndpoint, deleteOrganizationPropagation,
			deleteOrganizationEndpoint, addOrganizationMembersPropagation,
			addOrganizationMembersEndpoint,
			removeOrganizationMembersPropagation,
			removeOrganizationMembersEndpoint, moduleAlias, moduleSharedSecret,
			moduleContentType);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByModuleIde(
		long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModuleByModuleIde(moduleId);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByModuleName(
		java.lang.String moduleName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModuleByModuleName(moduleName);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByModuleURL(
		java.lang.String moduleURL)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModuleByModuleURL(moduleURL);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByModuleEndpoint(
		java.lang.String moduleEndpoint)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModuleByModuleEndpoint(moduleEndpoint);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByModuleAlias(
		java.lang.String moduleAlias)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModuleByModuleAlias(moduleAlias);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByModuleSharedSecret(
		java.lang.String moduleSharedSecret)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getExtModuleByModuleSharedSecret(moduleSharedSecret);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByRegistrationPropagation(
		java.lang.Boolean registrationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getExtModuleByRegistrationPropagation(registrationPropagation);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByDeletionPropagation(
		java.lang.Boolean deletionPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getExtModuleByDeletionPropagation(deletionPropagation);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getExtModuleByUpsertOrganizationPropagation(upsertOrganizationPropagation);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getExtModuleByDeleteOrganizationPropagation(deleteOrganizationPropagation);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getExtModuleByAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
	}

	public static java.util.List<it.eng.model.ExtModule> getExtModuleByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getExtModuleByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
	}

	public static void clearService() {
		_service = null;
	}

	public static ExtModuleLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ExtModuleLocalService.class.getName());

			if (invokableLocalService instanceof ExtModuleLocalService) {
				_service = (ExtModuleLocalService)invokableLocalService;
			}
			else {
				_service = new ExtModuleLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ExtModuleLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(ExtModuleLocalService service) {
	}

	private static ExtModuleLocalService _service;
}