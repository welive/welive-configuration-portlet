/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for PortalConf. This utility wraps
 * {@link it.eng.service.impl.PortalConfLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see PortalConfLocalService
 * @see it.eng.service.base.PortalConfLocalServiceBaseImpl
 * @see it.eng.service.impl.PortalConfLocalServiceImpl
 * @generated
 */
public class PortalConfLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.service.impl.PortalConfLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the portal conf to the database. Also notifies the appropriate model listeners.
	*
	* @param portalConf the portal conf
	* @return the portal conf that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.PortalConf addPortalConf(
		it.eng.model.PortalConf portalConf)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addPortalConf(portalConf);
	}

	/**
	* Creates a new portal conf with the primary key. Does not add the portal conf to the database.
	*
	* @param confName the primary key for the new portal conf
	* @return the new portal conf
	*/
	public static it.eng.model.PortalConf createPortalConf(
		java.lang.String confName) {
		return getService().createPortalConf(confName);
	}

	/**
	* Deletes the portal conf with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param confName the primary key of the portal conf
	* @return the portal conf that was removed
	* @throws PortalException if a portal conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.PortalConf deletePortalConf(
		java.lang.String confName)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deletePortalConf(confName);
	}

	/**
	* Deletes the portal conf from the database. Also notifies the appropriate model listeners.
	*
	* @param portalConf the portal conf
	* @return the portal conf that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.PortalConf deletePortalConf(
		it.eng.model.PortalConf portalConf)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deletePortalConf(portalConf);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.model.PortalConf fetchPortalConf(
		java.lang.String confName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchPortalConf(confName);
	}

	/**
	* Returns the portal conf with the primary key.
	*
	* @param confName the primary key of the portal conf
	* @return the portal conf
	* @throws PortalException if a portal conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.PortalConf getPortalConf(
		java.lang.String confName)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPortalConf(confName);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the portal confs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of portal confs
	* @param end the upper bound of the range of portal confs (not inclusive)
	* @return the range of portal confs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.PortalConf> getPortalConfs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getPortalConfs(start, end);
	}

	/**
	* Returns the number of portal confs.
	*
	* @return the number of portal confs
	* @throws SystemException if a system exception occurred
	*/
	public static int getPortalConfsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getPortalConfsCount();
	}

	/**
	* Updates the portal conf in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param portalConf the portal conf
	* @return the portal conf that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.PortalConf updatePortalConf(
		it.eng.model.PortalConf portalConf)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updatePortalConf(portalConf);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static it.eng.model.PortalConf addPortalConf(
		java.lang.String tomcatKsAlias, java.lang.String tomcatKsPassword,
		java.lang.String tomcatKsLocation, java.lang.String jvmKsPassword,
		java.lang.String jvmKsLocation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .addPortalConf(tomcatKsAlias, tomcatKsPassword,
			tomcatKsLocation, jvmKsPassword, jvmKsLocation);
	}

	public static it.eng.model.PortalConf updatePortalConf(
		java.lang.String tomcatKsAlias, java.lang.String tomcatKsPassword,
		java.lang.String tomcatKsLocation, java.lang.String jvmKsPassword,
		java.lang.String jvmKsLocation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .updatePortalConf(tomcatKsAlias, tomcatKsPassword,
			tomcatKsLocation, jvmKsPassword, jvmKsLocation);
	}

	public static it.eng.model.PortalConf deletePortalConf()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deletePortalConf();
	}

	public static it.eng.model.PortalConf getSecurityConfiguration()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getSecurityConfiguration();
	}

	public static void clearService() {
		_service = null;
	}

	public static PortalConfLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					PortalConfLocalService.class.getName());

			if (invokableLocalService instanceof PortalConfLocalService) {
				_service = (PortalConfLocalService)invokableLocalService;
			}
			else {
				_service = new PortalConfLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(PortalConfLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(PortalConfLocalService service) {
	}

	private static PortalConfLocalService _service;
}