/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ExtModuleLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see ExtModuleLocalService
 * @generated
 */
public class ExtModuleLocalServiceWrapper implements ExtModuleLocalService,
	ServiceWrapper<ExtModuleLocalService> {
	public ExtModuleLocalServiceWrapper(
		ExtModuleLocalService extModuleLocalService) {
		_extModuleLocalService = extModuleLocalService;
	}

	/**
	* Adds the ext module to the database. Also notifies the appropriate model listeners.
	*
	* @param extModule the ext module
	* @return the ext module that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.model.ExtModule addExtModule(it.eng.model.ExtModule extModule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.addExtModule(extModule);
	}

	/**
	* Creates a new ext module with the primary key. Does not add the ext module to the database.
	*
	* @param moduleId the primary key for the new ext module
	* @return the new ext module
	*/
	@Override
	public it.eng.model.ExtModule createExtModule(long moduleId) {
		return _extModuleLocalService.createExtModule(moduleId);
	}

	/**
	* Deletes the ext module with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module that was removed
	* @throws PortalException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.model.ExtModule deleteExtModule(long moduleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.deleteExtModule(moduleId);
	}

	/**
	* Deletes the ext module from the database. Also notifies the appropriate model listeners.
	*
	* @param extModule the ext module
	* @return the ext module that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.model.ExtModule deleteExtModule(
		it.eng.model.ExtModule extModule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.deleteExtModule(extModule);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _extModuleLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.eng.model.ExtModule fetchExtModule(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.fetchExtModule(moduleId);
	}

	/**
	* Returns the ext module with the primary key.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module
	* @throws PortalException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.model.ExtModule getExtModule(long moduleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModule(moduleId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the ext modules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of ext modules
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModules(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModules(start, end);
	}

	/**
	* Returns the number of ext modules.
	*
	* @return the number of ext modules
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getExtModulesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModulesCount();
	}

	/**
	* Updates the ext module in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param extModule the ext module
	* @return the ext module that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.model.ExtModule updateExtModule(
		it.eng.model.ExtModule extModule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.updateExtModule(extModule);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _extModuleLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_extModuleLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _extModuleLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public it.eng.model.ExtModule addExtModule(java.lang.String moduleName,
		java.lang.String moduleURL, boolean registrationPropagation,
		java.lang.String moduleEndpoint, boolean deletiontionPropagation,
		java.lang.String userDeletionEndpoint,
		boolean upsertOrganizationPropagation,
		java.lang.String upsertOrganizationEndpoint,
		boolean deleteOrganizationPropagation,
		java.lang.String deleteOrganizationEndpoint,
		boolean addOrganizationMembersPropagation,
		java.lang.String addOrganizationMembersEndpoint,
		boolean removeOrganizationMembersPropagation,
		java.lang.String removeOrganizationMembersEndpoint,
		java.lang.String moduleAlias, java.lang.String moduleSharedSecret,
		java.lang.String moduleContentType)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.addExtModule(moduleName, moduleURL,
			registrationPropagation, moduleEndpoint, deletiontionPropagation,
			userDeletionEndpoint, upsertOrganizationPropagation,
			upsertOrganizationEndpoint, deleteOrganizationPropagation,
			deleteOrganizationEndpoint, addOrganizationMembersPropagation,
			addOrganizationMembersEndpoint,
			removeOrganizationMembersPropagation,
			removeOrganizationMembersEndpoint, moduleAlias, moduleSharedSecret,
			moduleContentType);
	}

	@Override
	public it.eng.model.ExtModule updateExtModule(long moduleId,
		java.lang.String moduleName, java.lang.String moduleURL,
		boolean registrationPropagation, java.lang.String moduleEndpoint,
		boolean deletiontionPropagation, java.lang.String userDeletionEndpoint,
		boolean upsertOrganizationPropagation,
		java.lang.String upsertOrganizationEndpoint,
		boolean deleteOrganizationPropagation,
		java.lang.String deleteOrganizationEndpoint,
		boolean addOrganizationMembersPropagation,
		java.lang.String addOrganizationMembersEndpoint,
		boolean removeOrganizationMembersPropagation,
		java.lang.String removeOrganizationMembersEndpoint,
		java.lang.String moduleAlias, java.lang.String moduleSharedSecret,
		java.lang.String moduleContentType)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.updateExtModule(moduleId, moduleName,
			moduleURL, registrationPropagation, moduleEndpoint,
			deletiontionPropagation, userDeletionEndpoint,
			upsertOrganizationPropagation, upsertOrganizationEndpoint,
			deleteOrganizationPropagation, deleteOrganizationEndpoint,
			addOrganizationMembersPropagation, addOrganizationMembersEndpoint,
			removeOrganizationMembersPropagation,
			removeOrganizationMembersEndpoint, moduleAlias, moduleSharedSecret,
			moduleContentType);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByModuleIde(
		long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByModuleIde(moduleId);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByModuleName(
		java.lang.String moduleName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByModuleName(moduleName);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByModuleURL(
		java.lang.String moduleURL)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByModuleURL(moduleURL);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByModuleEndpoint(
		java.lang.String moduleEndpoint)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByModuleEndpoint(moduleEndpoint);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByModuleAlias(
		java.lang.String moduleAlias)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByModuleAlias(moduleAlias);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByModuleSharedSecret(
		java.lang.String moduleSharedSecret)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByModuleSharedSecret(moduleSharedSecret);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByRegistrationPropagation(
		java.lang.Boolean registrationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByRegistrationPropagation(registrationPropagation);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByDeletionPropagation(
		java.lang.Boolean deletionPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByDeletionPropagation(deletionPropagation);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByUpsertOrganizationPropagation(upsertOrganizationPropagation);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByDeleteOrganizationPropagation(deleteOrganizationPropagation);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
	}

	@Override
	public java.util.List<it.eng.model.ExtModule> getExtModuleByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _extModuleLocalService.getExtModuleByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ExtModuleLocalService getWrappedExtModuleLocalService() {
		return _extModuleLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedExtModuleLocalService(
		ExtModuleLocalService extModuleLocalService) {
		_extModuleLocalService = extModuleLocalService;
	}

	@Override
	public ExtModuleLocalService getWrappedService() {
		return _extModuleLocalService;
	}

	@Override
	public void setWrappedService(ExtModuleLocalService extModuleLocalService) {
		_extModuleLocalService = extModuleLocalService;
	}

	private ExtModuleLocalService _extModuleLocalService;
}