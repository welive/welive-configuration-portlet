/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.model.ExtModule;

import java.util.List;

/**
 * The persistence utility for the ext module service. This utility wraps {@link ExtModulePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see ExtModulePersistence
 * @see ExtModulePersistenceImpl
 * @generated
 */
public class ExtModuleUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ExtModule extModule) {
		getPersistence().clearCache(extModule);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ExtModule> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ExtModule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ExtModule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ExtModule update(ExtModule extModule)
		throws SystemException {
		return getPersistence().update(extModule);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ExtModule update(ExtModule extModule,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(extModule, serviceContext);
	}

	/**
	* Returns all the ext modules where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleId(
		long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleId(moduleId);
	}

	/**
	* Returns a range of all the ext modules where moduleId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleId the module ID
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleId(
		long moduleId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleId(moduleId, start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where moduleId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleId the module ID
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleId(
		long moduleId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByModuleId(moduleId, start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleId_First(long moduleId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence().findByModuleId_First(moduleId, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleId_First(long moduleId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleId_First(moduleId, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleId_Last(long moduleId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence().findByModuleId_Last(moduleId, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleId_Last(long moduleId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByModuleId_Last(moduleId, orderByComparator);
	}

	/**
	* Removes all the ext modules where moduleId = &#63; from the database.
	*
	* @param moduleId the module ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByModuleId(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByModuleId(moduleId);
	}

	/**
	* Returns the number of ext modules where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByModuleId(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByModuleId(moduleId);
	}

	/**
	* Returns all the ext modules where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleName(
		java.lang.String moduleName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleName(moduleName);
	}

	/**
	* Returns a range of all the ext modules where moduleName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleName the module name
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleName(
		java.lang.String moduleName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleName(moduleName, start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where moduleName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleName the module name
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleName(
		java.lang.String moduleName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByModuleName(moduleName, start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleName_First(
		java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleName_First(moduleName, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleName_First(
		java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleName_First(moduleName, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleName_Last(
		java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleName_Last(moduleName, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleName_Last(
		java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleName_Last(moduleName, orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByModuleName_PrevAndNext(
		long moduleId, java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleName_PrevAndNext(moduleId, moduleName,
			orderByComparator);
	}

	/**
	* Removes all the ext modules where moduleName = &#63; from the database.
	*
	* @param moduleName the module name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByModuleName(java.lang.String moduleName)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByModuleName(moduleName);
	}

	/**
	* Returns the number of ext modules where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByModuleName(java.lang.String moduleName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByModuleName(moduleName);
	}

	/**
	* Returns all the ext modules where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleURL(
		java.lang.String moduleURL)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleURL(moduleURL);
	}

	/**
	* Returns a range of all the ext modules where moduleURL = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleURL the module u r l
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleURL(
		java.lang.String moduleURL, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleURL(moduleURL, start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where moduleURL = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleURL the module u r l
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleURL(
		java.lang.String moduleURL, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByModuleURL(moduleURL, start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleURL_First(
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleURL_First(moduleURL, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleURL_First(
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleURL_First(moduleURL, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleURL_Last(
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleURL_Last(moduleURL, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleURL_Last(
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleURL_Last(moduleURL, orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByModuleURL_PrevAndNext(
		long moduleId, java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleURL_PrevAndNext(moduleId, moduleURL,
			orderByComparator);
	}

	/**
	* Removes all the ext modules where moduleURL = &#63; from the database.
	*
	* @param moduleURL the module u r l
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByModuleURL(java.lang.String moduleURL)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByModuleURL(moduleURL);
	}

	/**
	* Returns the number of ext modules where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByModuleURL(java.lang.String moduleURL)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByModuleURL(moduleURL);
	}

	/**
	* Returns all the ext modules where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleEndpoint(
		java.lang.String moduleEndpoint)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleEndpoint(moduleEndpoint);
	}

	/**
	* Returns a range of all the ext modules where moduleEndpoint = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleEndpoint the module endpoint
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleEndpoint(
		java.lang.String moduleEndpoint, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleEndpoint(moduleEndpoint, start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where moduleEndpoint = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleEndpoint the module endpoint
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleEndpoint(
		java.lang.String moduleEndpoint, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByModuleEndpoint(moduleEndpoint, start, end,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleEndpoint_First(
		java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleEndpoint_First(moduleEndpoint, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleEndpoint_First(
		java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleEndpoint_First(moduleEndpoint,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleEndpoint_Last(
		java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleEndpoint_Last(moduleEndpoint, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleEndpoint_Last(
		java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleEndpoint_Last(moduleEndpoint, orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByModuleEndpoint_PrevAndNext(
		long moduleId, java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleEndpoint_PrevAndNext(moduleId, moduleEndpoint,
			orderByComparator);
	}

	/**
	* Removes all the ext modules where moduleEndpoint = &#63; from the database.
	*
	* @param moduleEndpoint the module endpoint
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByModuleEndpoint(java.lang.String moduleEndpoint)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByModuleEndpoint(moduleEndpoint);
	}

	/**
	* Returns the number of ext modules where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByModuleEndpoint(java.lang.String moduleEndpoint)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByModuleEndpoint(moduleEndpoint);
	}

	/**
	* Returns all the ext modules where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleAlias(
		java.lang.String moduleAlias)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleAlias(moduleAlias);
	}

	/**
	* Returns a range of all the ext modules where moduleAlias = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleAlias the module alias
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleAlias(
		java.lang.String moduleAlias, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleAlias(moduleAlias, start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where moduleAlias = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleAlias the module alias
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleAlias(
		java.lang.String moduleAlias, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByModuleAlias(moduleAlias, start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleAlias_First(
		java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleAlias_First(moduleAlias, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleAlias_First(
		java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleAlias_First(moduleAlias, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleAlias_Last(
		java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleAlias_Last(moduleAlias, orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleAlias_Last(
		java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleAlias_Last(moduleAlias, orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByModuleAlias_PrevAndNext(
		long moduleId, java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleAlias_PrevAndNext(moduleId, moduleAlias,
			orderByComparator);
	}

	/**
	* Removes all the ext modules where moduleAlias = &#63; from the database.
	*
	* @param moduleAlias the module alias
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByModuleAlias(java.lang.String moduleAlias)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByModuleAlias(moduleAlias);
	}

	/**
	* Returns the number of ext modules where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByModuleAlias(java.lang.String moduleAlias)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByModuleAlias(moduleAlias);
	}

	/**
	* Returns all the ext modules where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleSharedSecret(
		java.lang.String moduleSharedSecret)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByModuleSharedSecret(moduleSharedSecret);
	}

	/**
	* Returns a range of all the ext modules where moduleSharedSecret = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleSharedSecret the module shared secret
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleSharedSecret(
		java.lang.String moduleSharedSecret, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByModuleSharedSecret(moduleSharedSecret, start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where moduleSharedSecret = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleSharedSecret the module shared secret
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByModuleSharedSecret(
		java.lang.String moduleSharedSecret, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByModuleSharedSecret(moduleSharedSecret, start, end,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleSharedSecret_First(
		java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleSharedSecret_First(moduleSharedSecret,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleSharedSecret_First(
		java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleSharedSecret_First(moduleSharedSecret,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByModuleSharedSecret_Last(
		java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleSharedSecret_Last(moduleSharedSecret,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByModuleSharedSecret_Last(
		java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByModuleSharedSecret_Last(moduleSharedSecret,
			orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByModuleSharedSecret_PrevAndNext(
		long moduleId, java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByModuleSharedSecret_PrevAndNext(moduleId,
			moduleSharedSecret, orderByComparator);
	}

	/**
	* Removes all the ext modules where moduleSharedSecret = &#63; from the database.
	*
	* @param moduleSharedSecret the module shared secret
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByModuleSharedSecret(
		java.lang.String moduleSharedSecret)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByModuleSharedSecret(moduleSharedSecret);
	}

	/**
	* Returns the number of ext modules where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByModuleSharedSecret(
		java.lang.String moduleSharedSecret)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByModuleSharedSecret(moduleSharedSecret);
	}

	/**
	* Returns all the ext modules where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByRegistrationPropagation(
		java.lang.Boolean registrationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByRegistrationPropagation(registrationPropagation);
	}

	/**
	* Returns a range of all the ext modules where registrationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param registrationPropagation the registration propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByRegistrationPropagation(
		java.lang.Boolean registrationPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByRegistrationPropagation(registrationPropagation,
			start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where registrationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param registrationPropagation the registration propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByRegistrationPropagation(
		java.lang.Boolean registrationPropagation, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByRegistrationPropagation(registrationPropagation,
			start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByRegistrationPropagation_First(
		java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByRegistrationPropagation_First(registrationPropagation,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByRegistrationPropagation_First(
		java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByRegistrationPropagation_First(registrationPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByRegistrationPropagation_Last(
		java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByRegistrationPropagation_Last(registrationPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByRegistrationPropagation_Last(
		java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByRegistrationPropagation_Last(registrationPropagation,
			orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByRegistrationPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByRegistrationPropagation_PrevAndNext(moduleId,
			registrationPropagation, orderByComparator);
	}

	/**
	* Removes all the ext modules where registrationPropagation = &#63; from the database.
	*
	* @param registrationPropagation the registration propagation
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByRegistrationPropagation(
		java.lang.Boolean registrationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByRegistrationPropagation(registrationPropagation);
	}

	/**
	* Returns the number of ext modules where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByRegistrationPropagation(
		java.lang.Boolean registrationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByRegistrationPropagation(registrationPropagation);
	}

	/**
	* Returns all the ext modules where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByDeletionPropagation(
		java.lang.Boolean deletionPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDeletionPropagation(deletionPropagation);
	}

	/**
	* Returns a range of all the ext modules where deletionPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param deletionPropagation the deletion propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByDeletionPropagation(
		java.lang.Boolean deletionPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDeletionPropagation(deletionPropagation, start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where deletionPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param deletionPropagation the deletion propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByDeletionPropagation(
		java.lang.Boolean deletionPropagation, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDeletionPropagation(deletionPropagation, start, end,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByDeletionPropagation_First(
		java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByDeletionPropagation_First(deletionPropagation,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByDeletionPropagation_First(
		java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDeletionPropagation_First(deletionPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByDeletionPropagation_Last(
		java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByDeletionPropagation_Last(deletionPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByDeletionPropagation_Last(
		java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDeletionPropagation_Last(deletionPropagation,
			orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByDeletionPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByDeletionPropagation_PrevAndNext(moduleId,
			deletionPropagation, orderByComparator);
	}

	/**
	* Removes all the ext modules where deletionPropagation = &#63; from the database.
	*
	* @param deletionPropagation the deletion propagation
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByDeletionPropagation(
		java.lang.Boolean deletionPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByDeletionPropagation(deletionPropagation);
	}

	/**
	* Returns the number of ext modules where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDeletionPropagation(
		java.lang.Boolean deletionPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDeletionPropagation(deletionPropagation);
	}

	/**
	* Returns all the ext modules where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUpsertOrganizationPropagation(upsertOrganizationPropagation);
	}

	/**
	* Returns a range of all the ext modules where upsertOrganizationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUpsertOrganizationPropagation(upsertOrganizationPropagation,
			start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where upsertOrganizationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUpsertOrganizationPropagation(upsertOrganizationPropagation,
			start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByUpsertOrganizationPropagation_First(
		java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByUpsertOrganizationPropagation_First(upsertOrganizationPropagation,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByUpsertOrganizationPropagation_First(
		java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUpsertOrganizationPropagation_First(upsertOrganizationPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByUpsertOrganizationPropagation_Last(
		java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByUpsertOrganizationPropagation_Last(upsertOrganizationPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByUpsertOrganizationPropagation_Last(
		java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUpsertOrganizationPropagation_Last(upsertOrganizationPropagation,
			orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByUpsertOrganizationPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByUpsertOrganizationPropagation_PrevAndNext(moduleId,
			upsertOrganizationPropagation, orderByComparator);
	}

	/**
	* Removes all the ext modules where upsertOrganizationPropagation = &#63; from the database.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByUpsertOrganizationPropagation(upsertOrganizationPropagation);
	}

	/**
	* Returns the number of ext modules where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByUpsertOrganizationPropagation(upsertOrganizationPropagation);
	}

	/**
	* Returns all the ext modules where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDeleteOrganizationPropagation(deleteOrganizationPropagation);
	}

	/**
	* Returns a range of all the ext modules where deleteOrganizationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDeleteOrganizationPropagation(deleteOrganizationPropagation,
			start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where deleteOrganizationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDeleteOrganizationPropagation(deleteOrganizationPropagation,
			start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByDeleteOrganizationPropagation_First(
		java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByDeleteOrganizationPropagation_First(deleteOrganizationPropagation,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByDeleteOrganizationPropagation_First(
		java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDeleteOrganizationPropagation_First(deleteOrganizationPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByDeleteOrganizationPropagation_Last(
		java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByDeleteOrganizationPropagation_Last(deleteOrganizationPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByDeleteOrganizationPropagation_Last(
		java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDeleteOrganizationPropagation_Last(deleteOrganizationPropagation,
			orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByDeleteOrganizationPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByDeleteOrganizationPropagation_PrevAndNext(moduleId,
			deleteOrganizationPropagation, orderByComparator);
	}

	/**
	* Removes all the ext modules where deleteOrganizationPropagation = &#63; from the database.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByDeleteOrganizationPropagation(deleteOrganizationPropagation);
	}

	/**
	* Returns the number of ext modules where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByDeleteOrganizationPropagation(deleteOrganizationPropagation);
	}

	/**
	* Returns all the ext modules where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
	}

	/**
	* Returns a range of all the ext modules where addOrganizationMembersPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAddOrganizationMembersPropagation(addOrganizationMembersPropagation,
			start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where addOrganizationMembersPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAddOrganizationMembersPropagation(addOrganizationMembersPropagation,
			start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByAddOrganizationMembersPropagation_First(
		java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByAddOrganizationMembersPropagation_First(addOrganizationMembersPropagation,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByAddOrganizationMembersPropagation_First(
		java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAddOrganizationMembersPropagation_First(addOrganizationMembersPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByAddOrganizationMembersPropagation_Last(
		java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByAddOrganizationMembersPropagation_Last(addOrganizationMembersPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByAddOrganizationMembersPropagation_Last(
		java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAddOrganizationMembersPropagation_Last(addOrganizationMembersPropagation,
			orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByAddOrganizationMembersPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByAddOrganizationMembersPropagation_PrevAndNext(moduleId,
			addOrganizationMembersPropagation, orderByComparator);
	}

	/**
	* Removes all the ext modules where addOrganizationMembersPropagation = &#63; from the database.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
	}

	/**
	* Returns the number of ext modules where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
	}

	/**
	* Returns all the ext modules where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
	}

	/**
	* Returns a range of all the ext modules where removeOrganizationMembersPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation,
			start, end);
	}

	/**
	* Returns an ordered range of all the ext modules where removeOrganizationMembersPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation,
			start, end, orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByRemoveOrganizationMembersPropagation_First(
		java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByRemoveOrganizationMembersPropagation_First(removeOrganizationMembersPropagation,
			orderByComparator);
	}

	/**
	* Returns the first ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByRemoveOrganizationMembersPropagation_First(
		java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByRemoveOrganizationMembersPropagation_First(removeOrganizationMembersPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByRemoveOrganizationMembersPropagation_Last(
		java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByRemoveOrganizationMembersPropagation_Last(removeOrganizationMembersPropagation,
			orderByComparator);
	}

	/**
	* Returns the last ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByRemoveOrganizationMembersPropagation_Last(
		java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByRemoveOrganizationMembersPropagation_Last(removeOrganizationMembersPropagation,
			orderByComparator);
	}

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule[] findByRemoveOrganizationMembersPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence()
				   .findByRemoveOrganizationMembersPropagation_PrevAndNext(moduleId,
			removeOrganizationMembersPropagation, orderByComparator);
	}

	/**
	* Removes all the ext modules where removeOrganizationMembersPropagation = &#63; from the database.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
	}

	/**
	* Returns the number of ext modules where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
	}

	/**
	* Caches the ext module in the entity cache if it is enabled.
	*
	* @param extModule the ext module
	*/
	public static void cacheResult(it.eng.model.ExtModule extModule) {
		getPersistence().cacheResult(extModule);
	}

	/**
	* Caches the ext modules in the entity cache if it is enabled.
	*
	* @param extModules the ext modules
	*/
	public static void cacheResult(
		java.util.List<it.eng.model.ExtModule> extModules) {
		getPersistence().cacheResult(extModules);
	}

	/**
	* Creates a new ext module with the primary key. Does not add the ext module to the database.
	*
	* @param moduleId the primary key for the new ext module
	* @return the new ext module
	*/
	public static it.eng.model.ExtModule create(long moduleId) {
		return getPersistence().create(moduleId);
	}

	/**
	* Removes the ext module with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module that was removed
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule remove(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence().remove(moduleId);
	}

	public static it.eng.model.ExtModule updateImpl(
		it.eng.model.ExtModule extModule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(extModule);
	}

	/**
	* Returns the ext module with the primary key or throws a {@link it.eng.NoSuchExtModuleException} if it could not be found.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule findByPrimaryKey(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException {
		return getPersistence().findByPrimaryKey(moduleId);
	}

	/**
	* Returns the ext module with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module, or <code>null</code> if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.model.ExtModule fetchByPrimaryKey(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(moduleId);
	}

	/**
	* Returns all the ext modules.
	*
	* @return the ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the ext modules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the ext modules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.model.ExtModule> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the ext modules from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of ext modules.
	*
	* @return the number of ext modules
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ExtModulePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ExtModulePersistence)PortletBeanLocatorUtil.locate(it.eng.service.ClpSerializer.getServletContextName(),
					ExtModulePersistence.class.getName());

			ReferenceRegistry.registerReference(ExtModuleUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ExtModulePersistence persistence) {
	}

	private static ExtModulePersistence _persistence;
}