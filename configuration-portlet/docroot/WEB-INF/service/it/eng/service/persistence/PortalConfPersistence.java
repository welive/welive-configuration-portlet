/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.model.PortalConf;

/**
 * The persistence interface for the portal conf service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see PortalConfPersistenceImpl
 * @see PortalConfUtil
 * @generated
 */
public interface PortalConfPersistence extends BasePersistence<PortalConf> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PortalConfUtil} to access the portal conf persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the portal confs where confName = &#63;.
	*
	* @param confName the conf name
	* @return the matching portal confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.PortalConf> findByConfName(
		java.lang.String confName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the portal confs where confName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param confName the conf name
	* @param start the lower bound of the range of portal confs
	* @param end the upper bound of the range of portal confs (not inclusive)
	* @return the range of matching portal confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.PortalConf> findByConfName(
		java.lang.String confName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the portal confs where confName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param confName the conf name
	* @param start the lower bound of the range of portal confs
	* @param end the upper bound of the range of portal confs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching portal confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.PortalConf> findByConfName(
		java.lang.String confName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first portal conf in the ordered set where confName = &#63;.
	*
	* @param confName the conf name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching portal conf
	* @throws it.eng.NoSuchPortalConfException if a matching portal conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.PortalConf findByConfName_First(
		java.lang.String confName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchPortalConfException;

	/**
	* Returns the first portal conf in the ordered set where confName = &#63;.
	*
	* @param confName the conf name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching portal conf, or <code>null</code> if a matching portal conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.PortalConf fetchByConfName_First(
		java.lang.String confName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last portal conf in the ordered set where confName = &#63;.
	*
	* @param confName the conf name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching portal conf
	* @throws it.eng.NoSuchPortalConfException if a matching portal conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.PortalConf findByConfName_Last(
		java.lang.String confName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchPortalConfException;

	/**
	* Returns the last portal conf in the ordered set where confName = &#63;.
	*
	* @param confName the conf name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching portal conf, or <code>null</code> if a matching portal conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.PortalConf fetchByConfName_Last(
		java.lang.String confName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the portal confs where confName = &#63; from the database.
	*
	* @param confName the conf name
	* @throws SystemException if a system exception occurred
	*/
	public void removeByConfName(java.lang.String confName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of portal confs where confName = &#63;.
	*
	* @param confName the conf name
	* @return the number of matching portal confs
	* @throws SystemException if a system exception occurred
	*/
	public int countByConfName(java.lang.String confName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the portal conf in the entity cache if it is enabled.
	*
	* @param portalConf the portal conf
	*/
	public void cacheResult(it.eng.model.PortalConf portalConf);

	/**
	* Caches the portal confs in the entity cache if it is enabled.
	*
	* @param portalConfs the portal confs
	*/
	public void cacheResult(java.util.List<it.eng.model.PortalConf> portalConfs);

	/**
	* Creates a new portal conf with the primary key. Does not add the portal conf to the database.
	*
	* @param confName the primary key for the new portal conf
	* @return the new portal conf
	*/
	public it.eng.model.PortalConf create(java.lang.String confName);

	/**
	* Removes the portal conf with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param confName the primary key of the portal conf
	* @return the portal conf that was removed
	* @throws it.eng.NoSuchPortalConfException if a portal conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.PortalConf remove(java.lang.String confName)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchPortalConfException;

	public it.eng.model.PortalConf updateImpl(
		it.eng.model.PortalConf portalConf)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the portal conf with the primary key or throws a {@link it.eng.NoSuchPortalConfException} if it could not be found.
	*
	* @param confName the primary key of the portal conf
	* @return the portal conf
	* @throws it.eng.NoSuchPortalConfException if a portal conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.PortalConf findByPrimaryKey(java.lang.String confName)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchPortalConfException;

	/**
	* Returns the portal conf with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param confName the primary key of the portal conf
	* @return the portal conf, or <code>null</code> if a portal conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.PortalConf fetchByPrimaryKey(java.lang.String confName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the portal confs.
	*
	* @return the portal confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.PortalConf> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the portal confs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of portal confs
	* @param end the upper bound of the range of portal confs (not inclusive)
	* @return the range of portal confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.PortalConf> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the portal confs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of portal confs
	* @param end the upper bound of the range of portal confs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of portal confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.PortalConf> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the portal confs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of portal confs.
	*
	* @return the number of portal confs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}