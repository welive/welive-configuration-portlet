/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.model.ExtModule;

/**
 * The persistence interface for the ext module service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see ExtModulePersistenceImpl
 * @see ExtModuleUtil
 * @generated
 */
public interface ExtModulePersistence extends BasePersistence<ExtModule> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ExtModuleUtil} to access the ext module persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the ext modules where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleId(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where moduleId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleId the module ID
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleId(
		long moduleId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where moduleId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleId the module ID
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleId(
		long moduleId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleId_First(long moduleId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleId_First(long moduleId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleId_Last(long moduleId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleId_Last(long moduleId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the ext modules where moduleId = &#63; from the database.
	*
	* @param moduleId the module ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByModuleId(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where moduleId = &#63;.
	*
	* @param moduleId the module ID
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByModuleId(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleName(
		java.lang.String moduleName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where moduleName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleName the module name
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleName(
		java.lang.String moduleName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where moduleName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleName the module name
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleName(
		java.lang.String moduleName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleName_First(
		java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleName_First(
		java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleName_Last(
		java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleName_Last(
		java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleName = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleName the module name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByModuleName_PrevAndNext(
		long moduleId, java.lang.String moduleName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where moduleName = &#63; from the database.
	*
	* @param moduleName the module name
	* @throws SystemException if a system exception occurred
	*/
	public void removeByModuleName(java.lang.String moduleName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where moduleName = &#63;.
	*
	* @param moduleName the module name
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByModuleName(java.lang.String moduleName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleURL(
		java.lang.String moduleURL)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where moduleURL = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleURL the module u r l
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleURL(
		java.lang.String moduleURL, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where moduleURL = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleURL the module u r l
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleURL(
		java.lang.String moduleURL, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleURL_First(
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleURL_First(
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleURL_Last(
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleURL_Last(
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleURL = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleURL the module u r l
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByModuleURL_PrevAndNext(long moduleId,
		java.lang.String moduleURL,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where moduleURL = &#63; from the database.
	*
	* @param moduleURL the module u r l
	* @throws SystemException if a system exception occurred
	*/
	public void removeByModuleURL(java.lang.String moduleURL)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where moduleURL = &#63;.
	*
	* @param moduleURL the module u r l
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByModuleURL(java.lang.String moduleURL)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleEndpoint(
		java.lang.String moduleEndpoint)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where moduleEndpoint = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleEndpoint the module endpoint
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleEndpoint(
		java.lang.String moduleEndpoint, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where moduleEndpoint = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleEndpoint the module endpoint
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleEndpoint(
		java.lang.String moduleEndpoint, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleEndpoint_First(
		java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleEndpoint_First(
		java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleEndpoint_Last(
		java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleEndpoint_Last(
		java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleEndpoint = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleEndpoint the module endpoint
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByModuleEndpoint_PrevAndNext(
		long moduleId, java.lang.String moduleEndpoint,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where moduleEndpoint = &#63; from the database.
	*
	* @param moduleEndpoint the module endpoint
	* @throws SystemException if a system exception occurred
	*/
	public void removeByModuleEndpoint(java.lang.String moduleEndpoint)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where moduleEndpoint = &#63;.
	*
	* @param moduleEndpoint the module endpoint
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByModuleEndpoint(java.lang.String moduleEndpoint)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleAlias(
		java.lang.String moduleAlias)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where moduleAlias = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleAlias the module alias
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleAlias(
		java.lang.String moduleAlias, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where moduleAlias = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleAlias the module alias
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleAlias(
		java.lang.String moduleAlias, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleAlias_First(
		java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleAlias_First(
		java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleAlias_Last(
		java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleAlias_Last(
		java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleAlias = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleAlias the module alias
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByModuleAlias_PrevAndNext(
		long moduleId, java.lang.String moduleAlias,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where moduleAlias = &#63; from the database.
	*
	* @param moduleAlias the module alias
	* @throws SystemException if a system exception occurred
	*/
	public void removeByModuleAlias(java.lang.String moduleAlias)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where moduleAlias = &#63;.
	*
	* @param moduleAlias the module alias
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByModuleAlias(java.lang.String moduleAlias)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleSharedSecret(
		java.lang.String moduleSharedSecret)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where moduleSharedSecret = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleSharedSecret the module shared secret
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleSharedSecret(
		java.lang.String moduleSharedSecret, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where moduleSharedSecret = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param moduleSharedSecret the module shared secret
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByModuleSharedSecret(
		java.lang.String moduleSharedSecret, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleSharedSecret_First(
		java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleSharedSecret_First(
		java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByModuleSharedSecret_Last(
		java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByModuleSharedSecret_Last(
		java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where moduleSharedSecret = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param moduleSharedSecret the module shared secret
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByModuleSharedSecret_PrevAndNext(
		long moduleId, java.lang.String moduleSharedSecret,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where moduleSharedSecret = &#63; from the database.
	*
	* @param moduleSharedSecret the module shared secret
	* @throws SystemException if a system exception occurred
	*/
	public void removeByModuleSharedSecret(java.lang.String moduleSharedSecret)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where moduleSharedSecret = &#63;.
	*
	* @param moduleSharedSecret the module shared secret
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByModuleSharedSecret(java.lang.String moduleSharedSecret)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByRegistrationPropagation(
		java.lang.Boolean registrationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where registrationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param registrationPropagation the registration propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByRegistrationPropagation(
		java.lang.Boolean registrationPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where registrationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param registrationPropagation the registration propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByRegistrationPropagation(
		java.lang.Boolean registrationPropagation, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByRegistrationPropagation_First(
		java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByRegistrationPropagation_First(
		java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByRegistrationPropagation_Last(
		java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByRegistrationPropagation_Last(
		java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where registrationPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param registrationPropagation the registration propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByRegistrationPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean registrationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where registrationPropagation = &#63; from the database.
	*
	* @param registrationPropagation the registration propagation
	* @throws SystemException if a system exception occurred
	*/
	public void removeByRegistrationPropagation(
		java.lang.Boolean registrationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where registrationPropagation = &#63;.
	*
	* @param registrationPropagation the registration propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByRegistrationPropagation(
		java.lang.Boolean registrationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByDeletionPropagation(
		java.lang.Boolean deletionPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where deletionPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param deletionPropagation the deletion propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByDeletionPropagation(
		java.lang.Boolean deletionPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where deletionPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param deletionPropagation the deletion propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByDeletionPropagation(
		java.lang.Boolean deletionPropagation, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByDeletionPropagation_First(
		java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByDeletionPropagation_First(
		java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByDeletionPropagation_Last(
		java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByDeletionPropagation_Last(
		java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where deletionPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param deletionPropagation the deletion propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByDeletionPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean deletionPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where deletionPropagation = &#63; from the database.
	*
	* @param deletionPropagation the deletion propagation
	* @throws SystemException if a system exception occurred
	*/
	public void removeByDeletionPropagation(
		java.lang.Boolean deletionPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where deletionPropagation = &#63;.
	*
	* @param deletionPropagation the deletion propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByDeletionPropagation(java.lang.Boolean deletionPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where upsertOrganizationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where upsertOrganizationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByUpsertOrganizationPropagation_First(
		java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByUpsertOrganizationPropagation_First(
		java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByUpsertOrganizationPropagation_Last(
		java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByUpsertOrganizationPropagation_Last(
		java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByUpsertOrganizationPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean upsertOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where upsertOrganizationPropagation = &#63; from the database.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where upsertOrganizationPropagation = &#63;.
	*
	* @param upsertOrganizationPropagation the upsert organization propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByUpsertOrganizationPropagation(
		java.lang.Boolean upsertOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where deleteOrganizationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where deleteOrganizationPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByDeleteOrganizationPropagation_First(
		java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByDeleteOrganizationPropagation_First(
		java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByDeleteOrganizationPropagation_Last(
		java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByDeleteOrganizationPropagation_Last(
		java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param deleteOrganizationPropagation the delete organization propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByDeleteOrganizationPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean deleteOrganizationPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where deleteOrganizationPropagation = &#63; from the database.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @throws SystemException if a system exception occurred
	*/
	public void removeByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where deleteOrganizationPropagation = &#63;.
	*
	* @param deleteOrganizationPropagation the delete organization propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByDeleteOrganizationPropagation(
		java.lang.Boolean deleteOrganizationPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where addOrganizationMembersPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where addOrganizationMembersPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByAddOrganizationMembersPropagation_First(
		java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByAddOrganizationMembersPropagation_First(
		java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByAddOrganizationMembersPropagation_Last(
		java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByAddOrganizationMembersPropagation_Last(
		java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByAddOrganizationMembersPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean addOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where addOrganizationMembersPropagation = &#63; from the database.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @throws SystemException if a system exception occurred
	*/
	public void removeByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where addOrganizationMembersPropagation = &#63;.
	*
	* @param addOrganizationMembersPropagation the add organization members propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByAddOrganizationMembersPropagation(
		java.lang.Boolean addOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @return the matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules where removeOrganizationMembersPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules where removeOrganizationMembersPropagation = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByRemoveOrganizationMembersPropagation_First(
		java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the first ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByRemoveOrganizationMembersPropagation_First(
		java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module
	* @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByRemoveOrganizationMembersPropagation_Last(
		java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the last ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByRemoveOrganizationMembersPropagation_Last(
		java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext modules before and after the current ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	*
	* @param moduleId the primary key of the current ext module
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule[] findByRemoveOrganizationMembersPropagation_PrevAndNext(
		long moduleId, java.lang.Boolean removeOrganizationMembersPropagation,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Removes all the ext modules where removeOrganizationMembersPropagation = &#63; from the database.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @throws SystemException if a system exception occurred
	*/
	public void removeByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules where removeOrganizationMembersPropagation = &#63;.
	*
	* @param removeOrganizationMembersPropagation the remove organization members propagation
	* @return the number of matching ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countByRemoveOrganizationMembersPropagation(
		java.lang.Boolean removeOrganizationMembersPropagation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the ext module in the entity cache if it is enabled.
	*
	* @param extModule the ext module
	*/
	public void cacheResult(it.eng.model.ExtModule extModule);

	/**
	* Caches the ext modules in the entity cache if it is enabled.
	*
	* @param extModules the ext modules
	*/
	public void cacheResult(java.util.List<it.eng.model.ExtModule> extModules);

	/**
	* Creates a new ext module with the primary key. Does not add the ext module to the database.
	*
	* @param moduleId the primary key for the new ext module
	* @return the new ext module
	*/
	public it.eng.model.ExtModule create(long moduleId);

	/**
	* Removes the ext module with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module that was removed
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule remove(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	public it.eng.model.ExtModule updateImpl(it.eng.model.ExtModule extModule)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ext module with the primary key or throws a {@link it.eng.NoSuchExtModuleException} if it could not be found.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module
	* @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule findByPrimaryKey(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.NoSuchExtModuleException;

	/**
	* Returns the ext module with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param moduleId the primary key of the ext module
	* @return the ext module, or <code>null</code> if a ext module with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.model.ExtModule fetchByPrimaryKey(long moduleId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ext modules.
	*
	* @return the ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ext modules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @return the range of ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ext modules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ext modules
	* @param end the upper bound of the range of ext modules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ext modules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.model.ExtModule> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the ext modules from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ext modules.
	*
	* @return the number of ext modules
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}