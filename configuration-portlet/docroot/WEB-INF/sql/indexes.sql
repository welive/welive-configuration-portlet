create index IX_178B27F6 on ExtModule_ExtModule (addOrganizationMembersPropagation);
create index IX_492F429D on ExtModule_ExtModule (deleteOrganizationPropagation);
create index IX_58EE278D on ExtModule_ExtModule (deletionPropagation);
create index IX_EE244133 on ExtModule_ExtModule (moduleAlias);
create index IX_D590EE36 on ExtModule_ExtModule (moduleEndpoint);
create index IX_751F037C on ExtModule_ExtModule (moduleId);
create index IX_B1E9952C on ExtModule_ExtModule (moduleName);
create index IX_67E43096 on ExtModule_ExtModule (moduleSharedSecret);
create index IX_2F62D8D2 on ExtModule_ExtModule (moduleURL);
create index IX_FF261202 on ExtModule_ExtModule (registrationPropagation);
create index IX_C9A3C2BF on ExtModule_ExtModule (removeOrganizationMembersPropagation);
create index IX_7E58EB99 on ExtModule_ExtModule (upsertOrganizationPropagation);

create index IX_49B295CB on ExtModule_PortalConf (confName);