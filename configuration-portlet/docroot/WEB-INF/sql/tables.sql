create table ExtModule_ExtModule (
	moduleId LONG not null primary key,
	moduleName TEXT null,
	moduleURL TEXT null,
	registrationPropagation BOOLEAN,
	moduleEndpoint TEXT null,
	deletionPropagation BOOLEAN,
	userDeletionEndpoint TEXT null,
	upsertOrganizationPropagation BOOLEAN,
	upsertOrganizationEndpoint TEXT null,
	deleteOrganizationPropagation BOOLEAN,
	deleteOrganizationEndpoint TEXT null,
	addOrganizationMembersPropagation BOOLEAN,
	addOrganizationMembersEndpoint TEXT null,
	removeOrganizationMembersPropagation BOOLEAN,
	removeOrganizationMembersEndpoint TEXT null,
	moduleAlias TEXT null,
	moduleSharedSecret TEXT null,
	contentType TEXT null
);

create table ExtModule_PortalConf (
	confName VARCHAR(75) not null primary key,
	tomcatKsAlias VARCHAR(75) null,
	tomcatKsPassword VARCHAR(75) null,
	tomcatKsLocation VARCHAR(75) null,
	jvmKsLocation VARCHAR(75) null,
	jvmKsPassword VARCHAR(75) null
);