package it.eng.configuration;

import it.eng.service.ExtModuleLocalServiceUtil;
import it.eng.service.PortalConfLocalServiceUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ConfigurationActions extends MVCPortlet
{
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void deleteExtModule(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception
	{
		long moduleId = Long.parseLong(actionRequest.getParameter("moduleId"));
		
		try{
			ExtModuleLocalServiceUtil.deleteExtModule(moduleId);
		}catch(Exception e)	{
			SessionErrors.add(actionRequest, "delete_error");
		}
		
		actionResponse.sendRedirect(actionRequest.getParameter("redirect"));
	}
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void addExtModule(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception
	{
		
		String moduleName = actionRequest.getParameter("moduleName");
		String moduleURL = actionRequest.getParameter("moduleURL");
		
		boolean registrationPropagation = Boolean.valueOf(actionRequest.getParameter("registrationPropagation"));
		String moduleEndpoint = actionRequest.getParameter("moduleEndpoint");
		
		boolean deletionPropagation = Boolean.valueOf(actionRequest.getParameter("deletionPropagation"));
		String userDeletionEndpoint = actionRequest.getParameter("userDeletionEndpoint");
		
		boolean upsertOrganizationPropagation = Boolean.valueOf(actionRequest.getParameter("upsertOrganizationPropagation"));
		String upsertOrganizationEndpoint = actionRequest.getParameter("upsertOrganizationEndpoint");
		
		boolean deleteOrganizationPropagation = Boolean.valueOf(actionRequest.getParameter("deleteOrganizationPropagation"));
		String deleteOrganizationEndpoint = actionRequest.getParameter("deleteOrganizationEndpoint");
		
		boolean addOrganizationMembersPropagation = Boolean.valueOf(actionRequest.getParameter("addOrganizationMembersPropagation"));
		String addOrganizationMembersEndpoint = actionRequest.getParameter("addOrganizationMembersEndpoint");
		
		boolean removeOrganizationMembersPropagation = Boolean.valueOf(actionRequest.getParameter("removeOrganizationMembersPropagation"));
		String removeOrganizationMembersEndpoint = actionRequest.getParameter("removeOrganizationMembersEndpoint");
		
		String moduleAlias = actionRequest.getParameter("moduleAlias");
		String moduleSharedSecret = actionRequest.getParameter("moduleSharedSecret");
		String moduleContentType=actionRequest.getParameter("moduleContentType");
		
		try	{
				ExtModuleLocalServiceUtil.addExtModule(moduleName, moduleURL, registrationPropagation, moduleEndpoint, deletionPropagation,	userDeletionEndpoint, 
						upsertOrganizationPropagation, upsertOrganizationEndpoint, deleteOrganizationPropagation, deleteOrganizationEndpoint,
						addOrganizationMembersPropagation, addOrganizationMembersEndpoint, removeOrganizationMembersPropagation, removeOrganizationMembersEndpoint, 						
						moduleAlias,moduleSharedSecret, moduleContentType);
		}catch(Exception e){
				SessionErrors.add(actionRequest, "add_error");
		}

		actionResponse.sendRedirect(actionRequest.getParameter("redirect"));
	}
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void updateExtModule(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
		long moduleId = Long.parseLong(actionRequest.getParameter("moduleId"));
		String moduleName = actionRequest.getParameter("moduleName");
		String moduleURL = actionRequest.getParameter("moduleURL");
		
		boolean registrationPropagation = Boolean.valueOf(actionRequest.getParameter("registrationPropagation"));
		String moduleEndpoint = actionRequest.getParameter("moduleEndpoint");
		
		boolean deletionPropagation = Boolean.valueOf(actionRequest.getParameter("deletionPropagation"));
		String userDeletionEndpoint = actionRequest.getParameter("userDeletionEndpoint");
		
		boolean upsertOrganizationPropagation = Boolean.valueOf(actionRequest.getParameter("upsertOrganizationPropagation"));
		String upsertOrganizationEndpoint = actionRequest.getParameter("upsertOrganizationEndpoint");
		
		boolean deleteOrganizationPropagation = Boolean.valueOf(actionRequest.getParameter("deleteOrganizationPropagation"));
		String deleteOrganizationEndpoint = actionRequest.getParameter("deleteOrganizationEndpoint");
		
		boolean addOrganizationMembersPropagation = Boolean.valueOf(actionRequest.getParameter("addOrganizationMembersPropagation"));
		String addOrganizationMembersEndpoint = actionRequest.getParameter("addOrganizationMembersEndpoint");
		
		boolean removeOrganizationMembersPropagation = Boolean.valueOf(actionRequest.getParameter("removeOrganizationMembersPropagation"));
		String removeOrganizationMembersEndpoint = actionRequest.getParameter("removeOrganizationMembersEndpoint");
		
		String moduleAlias = actionRequest.getParameter("moduleAlias");
		String moduleSharedSecret = actionRequest.getParameter("moduleSharedSecret");
		String moduleContentType=actionRequest.getParameter("moduleContentType");
		
		try	{
			ExtModuleLocalServiceUtil.updateExtModule(moduleId, moduleName, moduleURL, registrationPropagation, moduleEndpoint, deletionPropagation, userDeletionEndpoint, 
					upsertOrganizationPropagation, upsertOrganizationEndpoint, deleteOrganizationPropagation, deleteOrganizationEndpoint,
					addOrganizationMembersPropagation, addOrganizationMembersEndpoint, removeOrganizationMembersPropagation, removeOrganizationMembersEndpoint, 
					moduleAlias, moduleSharedSecret, moduleContentType);
		}catch(Exception e)	{
			SessionErrors.add(actionRequest, "update_error");
		}
		
		actionResponse.sendRedirect(actionRequest.getParameter("redirect"));
	}
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void addPortalConf(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
		String tomcatKsLocation = actionRequest.getParameter("tomcatKsLocation");
		String tomcatKsPassword = actionRequest.getParameter("tomcatKsPassword");
		String tomcatKsAlias = actionRequest.getParameter("tomcatKsAlias");
		String jvmKsLocation = actionRequest.getParameter("jvmKsLocation");
		String jvmKsPassword = actionRequest.getParameter("jvmKsPassword");
		
		try	{
			PortalConfLocalServiceUtil.addPortalConf(tomcatKsAlias, tomcatKsPassword, tomcatKsLocation, jvmKsPassword, jvmKsLocation);
		}catch(Exception e){
			SessionErrors.add(actionRequest, "conf_error");
		}
		
		actionResponse.sendRedirect(actionRequest.getParameter("redirect"));
	}
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void updatePortalConf(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
		String tomcatKsLocation = actionRequest.getParameter("tomcatKsLocation");
		String tomcatKsPassword = actionRequest.getParameter("tomcatKsPassword");
		String tomcatKsAlias = actionRequest.getParameter("tomcatKsAlias");
		String jvmKsLocation = actionRequest.getParameter("jvmKsLocation");
		String jvmKsPassword = actionRequest.getParameter("jvmKsPassword");
		
		try{
			PortalConfLocalServiceUtil.updatePortalConf(tomcatKsAlias, tomcatKsPassword, tomcatKsLocation, jvmKsPassword, jvmKsLocation);
		}catch(Exception e)	{
			SessionErrors.add(actionRequest, "conf_error");
		}
		
		actionResponse.sendRedirect(actionRequest.getParameter("redirect"));
	}
}
