/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.model.ExtModule;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ExtModule in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see ExtModule
 * @generated
 */
public class ExtModuleCacheModel implements CacheModel<ExtModule>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(37);

		sb.append("{moduleId=");
		sb.append(moduleId);
		sb.append(", moduleName=");
		sb.append(moduleName);
		sb.append(", moduleURL=");
		sb.append(moduleURL);
		sb.append(", registrationPropagation=");
		sb.append(registrationPropagation);
		sb.append(", moduleEndpoint=");
		sb.append(moduleEndpoint);
		sb.append(", deletionPropagation=");
		sb.append(deletionPropagation);
		sb.append(", userDeletionEndpoint=");
		sb.append(userDeletionEndpoint);
		sb.append(", upsertOrganizationPropagation=");
		sb.append(upsertOrganizationPropagation);
		sb.append(", upsertOrganizationEndpoint=");
		sb.append(upsertOrganizationEndpoint);
		sb.append(", deleteOrganizationPropagation=");
		sb.append(deleteOrganizationPropagation);
		sb.append(", deleteOrganizationEndpoint=");
		sb.append(deleteOrganizationEndpoint);
		sb.append(", addOrganizationMembersPropagation=");
		sb.append(addOrganizationMembersPropagation);
		sb.append(", addOrganizationMembersEndpoint=");
		sb.append(addOrganizationMembersEndpoint);
		sb.append(", removeOrganizationMembersPropagation=");
		sb.append(removeOrganizationMembersPropagation);
		sb.append(", removeOrganizationMembersEndpoint=");
		sb.append(removeOrganizationMembersEndpoint);
		sb.append(", moduleAlias=");
		sb.append(moduleAlias);
		sb.append(", moduleSharedSecret=");
		sb.append(moduleSharedSecret);
		sb.append(", contentType=");
		sb.append(contentType);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ExtModule toEntityModel() {
		ExtModuleImpl extModuleImpl = new ExtModuleImpl();

		extModuleImpl.setModuleId(moduleId);

		if (moduleName == null) {
			extModuleImpl.setModuleName(StringPool.BLANK);
		}
		else {
			extModuleImpl.setModuleName(moduleName);
		}

		if (moduleURL == null) {
			extModuleImpl.setModuleURL(StringPool.BLANK);
		}
		else {
			extModuleImpl.setModuleURL(moduleURL);
		}

		extModuleImpl.setRegistrationPropagation(registrationPropagation);

		if (moduleEndpoint == null) {
			extModuleImpl.setModuleEndpoint(StringPool.BLANK);
		}
		else {
			extModuleImpl.setModuleEndpoint(moduleEndpoint);
		}

		extModuleImpl.setDeletionPropagation(deletionPropagation);

		if (userDeletionEndpoint == null) {
			extModuleImpl.setUserDeletionEndpoint(StringPool.BLANK);
		}
		else {
			extModuleImpl.setUserDeletionEndpoint(userDeletionEndpoint);
		}

		extModuleImpl.setUpsertOrganizationPropagation(upsertOrganizationPropagation);

		if (upsertOrganizationEndpoint == null) {
			extModuleImpl.setUpsertOrganizationEndpoint(StringPool.BLANK);
		}
		else {
			extModuleImpl.setUpsertOrganizationEndpoint(upsertOrganizationEndpoint);
		}

		extModuleImpl.setDeleteOrganizationPropagation(deleteOrganizationPropagation);

		if (deleteOrganizationEndpoint == null) {
			extModuleImpl.setDeleteOrganizationEndpoint(StringPool.BLANK);
		}
		else {
			extModuleImpl.setDeleteOrganizationEndpoint(deleteOrganizationEndpoint);
		}

		extModuleImpl.setAddOrganizationMembersPropagation(addOrganizationMembersPropagation);

		if (addOrganizationMembersEndpoint == null) {
			extModuleImpl.setAddOrganizationMembersEndpoint(StringPool.BLANK);
		}
		else {
			extModuleImpl.setAddOrganizationMembersEndpoint(addOrganizationMembersEndpoint);
		}

		extModuleImpl.setRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);

		if (removeOrganizationMembersEndpoint == null) {
			extModuleImpl.setRemoveOrganizationMembersEndpoint(StringPool.BLANK);
		}
		else {
			extModuleImpl.setRemoveOrganizationMembersEndpoint(removeOrganizationMembersEndpoint);
		}

		if (moduleAlias == null) {
			extModuleImpl.setModuleAlias(StringPool.BLANK);
		}
		else {
			extModuleImpl.setModuleAlias(moduleAlias);
		}

		if (moduleSharedSecret == null) {
			extModuleImpl.setModuleSharedSecret(StringPool.BLANK);
		}
		else {
			extModuleImpl.setModuleSharedSecret(moduleSharedSecret);
		}

		if (contentType == null) {
			extModuleImpl.setContentType(StringPool.BLANK);
		}
		else {
			extModuleImpl.setContentType(contentType);
		}

		extModuleImpl.resetOriginalValues();

		return extModuleImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		moduleId = objectInput.readLong();
		moduleName = objectInput.readUTF();
		moduleURL = objectInput.readUTF();
		registrationPropagation = objectInput.readBoolean();
		moduleEndpoint = objectInput.readUTF();
		deletionPropagation = objectInput.readBoolean();
		userDeletionEndpoint = objectInput.readUTF();
		upsertOrganizationPropagation = objectInput.readBoolean();
		upsertOrganizationEndpoint = objectInput.readUTF();
		deleteOrganizationPropagation = objectInput.readBoolean();
		deleteOrganizationEndpoint = objectInput.readUTF();
		addOrganizationMembersPropagation = objectInput.readBoolean();
		addOrganizationMembersEndpoint = objectInput.readUTF();
		removeOrganizationMembersPropagation = objectInput.readBoolean();
		removeOrganizationMembersEndpoint = objectInput.readUTF();
		moduleAlias = objectInput.readUTF();
		moduleSharedSecret = objectInput.readUTF();
		contentType = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(moduleId);

		if (moduleName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(moduleName);
		}

		if (moduleURL == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(moduleURL);
		}

		objectOutput.writeBoolean(registrationPropagation);

		if (moduleEndpoint == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(moduleEndpoint);
		}

		objectOutput.writeBoolean(deletionPropagation);

		if (userDeletionEndpoint == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userDeletionEndpoint);
		}

		objectOutput.writeBoolean(upsertOrganizationPropagation);

		if (upsertOrganizationEndpoint == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(upsertOrganizationEndpoint);
		}

		objectOutput.writeBoolean(deleteOrganizationPropagation);

		if (deleteOrganizationEndpoint == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(deleteOrganizationEndpoint);
		}

		objectOutput.writeBoolean(addOrganizationMembersPropagation);

		if (addOrganizationMembersEndpoint == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(addOrganizationMembersEndpoint);
		}

		objectOutput.writeBoolean(removeOrganizationMembersPropagation);

		if (removeOrganizationMembersEndpoint == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(removeOrganizationMembersEndpoint);
		}

		if (moduleAlias == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(moduleAlias);
		}

		if (moduleSharedSecret == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(moduleSharedSecret);
		}

		if (contentType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(contentType);
		}
	}

	public long moduleId;
	public String moduleName;
	public String moduleURL;
	public Boolean registrationPropagation;
	public String moduleEndpoint;
	public Boolean deletionPropagation;
	public String userDeletionEndpoint;
	public Boolean upsertOrganizationPropagation;
	public String upsertOrganizationEndpoint;
	public Boolean deleteOrganizationPropagation;
	public String deleteOrganizationEndpoint;
	public Boolean addOrganizationMembersPropagation;
	public String addOrganizationMembersEndpoint;
	public Boolean removeOrganizationMembersPropagation;
	public String removeOrganizationMembersEndpoint;
	public String moduleAlias;
	public String moduleSharedSecret;
	public String contentType;
}