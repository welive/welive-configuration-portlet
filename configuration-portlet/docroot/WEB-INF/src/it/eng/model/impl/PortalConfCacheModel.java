/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.model.PortalConf;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PortalConf in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see PortalConf
 * @generated
 */
public class PortalConfCacheModel implements CacheModel<PortalConf>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{confName=");
		sb.append(confName);
		sb.append(", tomcatKsAlias=");
		sb.append(tomcatKsAlias);
		sb.append(", tomcatKsPassword=");
		sb.append(tomcatKsPassword);
		sb.append(", tomcatKsLocation=");
		sb.append(tomcatKsLocation);
		sb.append(", jvmKsLocation=");
		sb.append(jvmKsLocation);
		sb.append(", jvmKsPassword=");
		sb.append(jvmKsPassword);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PortalConf toEntityModel() {
		PortalConfImpl portalConfImpl = new PortalConfImpl();

		if (confName == null) {
			portalConfImpl.setConfName(StringPool.BLANK);
		}
		else {
			portalConfImpl.setConfName(confName);
		}

		if (tomcatKsAlias == null) {
			portalConfImpl.setTomcatKsAlias(StringPool.BLANK);
		}
		else {
			portalConfImpl.setTomcatKsAlias(tomcatKsAlias);
		}

		if (tomcatKsPassword == null) {
			portalConfImpl.setTomcatKsPassword(StringPool.BLANK);
		}
		else {
			portalConfImpl.setTomcatKsPassword(tomcatKsPassword);
		}

		if (tomcatKsLocation == null) {
			portalConfImpl.setTomcatKsLocation(StringPool.BLANK);
		}
		else {
			portalConfImpl.setTomcatKsLocation(tomcatKsLocation);
		}

		if (jvmKsLocation == null) {
			portalConfImpl.setJvmKsLocation(StringPool.BLANK);
		}
		else {
			portalConfImpl.setJvmKsLocation(jvmKsLocation);
		}

		if (jvmKsPassword == null) {
			portalConfImpl.setJvmKsPassword(StringPool.BLANK);
		}
		else {
			portalConfImpl.setJvmKsPassword(jvmKsPassword);
		}

		portalConfImpl.resetOriginalValues();

		return portalConfImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		confName = objectInput.readUTF();
		tomcatKsAlias = objectInput.readUTF();
		tomcatKsPassword = objectInput.readUTF();
		tomcatKsLocation = objectInput.readUTF();
		jvmKsLocation = objectInput.readUTF();
		jvmKsPassword = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (confName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(confName);
		}

		if (tomcatKsAlias == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tomcatKsAlias);
		}

		if (tomcatKsPassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tomcatKsPassword);
		}

		if (tomcatKsLocation == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tomcatKsLocation);
		}

		if (jvmKsLocation == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(jvmKsLocation);
		}

		if (jvmKsPassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(jvmKsPassword);
		}
	}

	public String confName;
	public String tomcatKsAlias;
	public String tomcatKsPassword;
	public String tomcatKsLocation;
	public String jvmKsLocation;
	public String jvmKsPassword;
}