/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.NoSuchPortalConfException;

import it.eng.model.PortalConf;
import it.eng.model.impl.PortalConfImpl;
import it.eng.model.impl.PortalConfModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the portal conf service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see PortalConfPersistence
 * @see PortalConfUtil
 * @generated
 */
public class PortalConfPersistenceImpl extends BasePersistenceImpl<PortalConf>
	implements PortalConfPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PortalConfUtil} to access the portal conf persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PortalConfImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfModelImpl.FINDER_CACHE_ENABLED, PortalConfImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfModelImpl.FINDER_CACHE_ENABLED, PortalConfImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CONFNAME = new FinderPath(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfModelImpl.FINDER_CACHE_ENABLED, PortalConfImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByConfName",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONFNAME =
		new FinderPath(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfModelImpl.FINDER_CACHE_ENABLED, PortalConfImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByConfName",
			new String[] { String.class.getName() },
			PortalConfModelImpl.CONFNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CONFNAME = new FinderPath(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByConfName",
			new String[] { String.class.getName() });

	/**
	 * Returns all the portal confs where confName = &#63;.
	 *
	 * @param confName the conf name
	 * @return the matching portal confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PortalConf> findByConfName(String confName)
		throws SystemException {
		return findByConfName(confName, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the portal confs where confName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param confName the conf name
	 * @param start the lower bound of the range of portal confs
	 * @param end the upper bound of the range of portal confs (not inclusive)
	 * @return the range of matching portal confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PortalConf> findByConfName(String confName, int start, int end)
		throws SystemException {
		return findByConfName(confName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the portal confs where confName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param confName the conf name
	 * @param start the lower bound of the range of portal confs
	 * @param end the upper bound of the range of portal confs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching portal confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PortalConf> findByConfName(String confName, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONFNAME;
			finderArgs = new Object[] { confName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CONFNAME;
			finderArgs = new Object[] { confName, start, end, orderByComparator };
		}

		List<PortalConf> list = (List<PortalConf>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PortalConf portalConf : list) {
				if (!Validator.equals(confName, portalConf.getConfName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PORTALCONF_WHERE);

			boolean bindConfName = false;

			if (confName == null) {
				query.append(_FINDER_COLUMN_CONFNAME_CONFNAME_1);
			}
			else if (confName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CONFNAME_CONFNAME_3);
			}
			else {
				bindConfName = true;

				query.append(_FINDER_COLUMN_CONFNAME_CONFNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PortalConfModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindConfName) {
					qPos.add(confName);
				}

				if (!pagination) {
					list = (List<PortalConf>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PortalConf>(list);
				}
				else {
					list = (List<PortalConf>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first portal conf in the ordered set where confName = &#63;.
	 *
	 * @param confName the conf name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching portal conf
	 * @throws it.eng.NoSuchPortalConfException if a matching portal conf could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf findByConfName_First(String confName,
		OrderByComparator orderByComparator)
		throws NoSuchPortalConfException, SystemException {
		PortalConf portalConf = fetchByConfName_First(confName,
				orderByComparator);

		if (portalConf != null) {
			return portalConf;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("confName=");
		msg.append(confName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPortalConfException(msg.toString());
	}

	/**
	 * Returns the first portal conf in the ordered set where confName = &#63;.
	 *
	 * @param confName the conf name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching portal conf, or <code>null</code> if a matching portal conf could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf fetchByConfName_First(String confName,
		OrderByComparator orderByComparator) throws SystemException {
		List<PortalConf> list = findByConfName(confName, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last portal conf in the ordered set where confName = &#63;.
	 *
	 * @param confName the conf name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching portal conf
	 * @throws it.eng.NoSuchPortalConfException if a matching portal conf could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf findByConfName_Last(String confName,
		OrderByComparator orderByComparator)
		throws NoSuchPortalConfException, SystemException {
		PortalConf portalConf = fetchByConfName_Last(confName, orderByComparator);

		if (portalConf != null) {
			return portalConf;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("confName=");
		msg.append(confName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPortalConfException(msg.toString());
	}

	/**
	 * Returns the last portal conf in the ordered set where confName = &#63;.
	 *
	 * @param confName the conf name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching portal conf, or <code>null</code> if a matching portal conf could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf fetchByConfName_Last(String confName,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByConfName(confName);

		if (count == 0) {
			return null;
		}

		List<PortalConf> list = findByConfName(confName, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the portal confs where confName = &#63; from the database.
	 *
	 * @param confName the conf name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByConfName(String confName) throws SystemException {
		for (PortalConf portalConf : findByConfName(confName,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(portalConf);
		}
	}

	/**
	 * Returns the number of portal confs where confName = &#63;.
	 *
	 * @param confName the conf name
	 * @return the number of matching portal confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByConfName(String confName) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CONFNAME;

		Object[] finderArgs = new Object[] { confName };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PORTALCONF_WHERE);

			boolean bindConfName = false;

			if (confName == null) {
				query.append(_FINDER_COLUMN_CONFNAME_CONFNAME_1);
			}
			else if (confName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CONFNAME_CONFNAME_3);
			}
			else {
				bindConfName = true;

				query.append(_FINDER_COLUMN_CONFNAME_CONFNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindConfName) {
					qPos.add(confName);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CONFNAME_CONFNAME_1 = "portalConf.confName IS NULL";
	private static final String _FINDER_COLUMN_CONFNAME_CONFNAME_2 = "portalConf.confName = ?";
	private static final String _FINDER_COLUMN_CONFNAME_CONFNAME_3 = "(portalConf.confName IS NULL OR portalConf.confName = '')";

	public PortalConfPersistenceImpl() {
		setModelClass(PortalConf.class);
	}

	/**
	 * Caches the portal conf in the entity cache if it is enabled.
	 *
	 * @param portalConf the portal conf
	 */
	@Override
	public void cacheResult(PortalConf portalConf) {
		EntityCacheUtil.putResult(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfImpl.class, portalConf.getPrimaryKey(), portalConf);

		portalConf.resetOriginalValues();
	}

	/**
	 * Caches the portal confs in the entity cache if it is enabled.
	 *
	 * @param portalConfs the portal confs
	 */
	@Override
	public void cacheResult(List<PortalConf> portalConfs) {
		for (PortalConf portalConf : portalConfs) {
			if (EntityCacheUtil.getResult(
						PortalConfModelImpl.ENTITY_CACHE_ENABLED,
						PortalConfImpl.class, portalConf.getPrimaryKey()) == null) {
				cacheResult(portalConf);
			}
			else {
				portalConf.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all portal confs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PortalConfImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PortalConfImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the portal conf.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PortalConf portalConf) {
		EntityCacheUtil.removeResult(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfImpl.class, portalConf.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<PortalConf> portalConfs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PortalConf portalConf : portalConfs) {
			EntityCacheUtil.removeResult(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
				PortalConfImpl.class, portalConf.getPrimaryKey());
		}
	}

	/**
	 * Creates a new portal conf with the primary key. Does not add the portal conf to the database.
	 *
	 * @param confName the primary key for the new portal conf
	 * @return the new portal conf
	 */
	@Override
	public PortalConf create(String confName) {
		PortalConf portalConf = new PortalConfImpl();

		portalConf.setNew(true);
		portalConf.setPrimaryKey(confName);

		return portalConf;
	}

	/**
	 * Removes the portal conf with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param confName the primary key of the portal conf
	 * @return the portal conf that was removed
	 * @throws it.eng.NoSuchPortalConfException if a portal conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf remove(String confName)
		throws NoSuchPortalConfException, SystemException {
		return remove((Serializable)confName);
	}

	/**
	 * Removes the portal conf with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the portal conf
	 * @return the portal conf that was removed
	 * @throws it.eng.NoSuchPortalConfException if a portal conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf remove(Serializable primaryKey)
		throws NoSuchPortalConfException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PortalConf portalConf = (PortalConf)session.get(PortalConfImpl.class,
					primaryKey);

			if (portalConf == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPortalConfException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(portalConf);
		}
		catch (NoSuchPortalConfException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PortalConf removeImpl(PortalConf portalConf)
		throws SystemException {
		portalConf = toUnwrappedModel(portalConf);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(portalConf)) {
				portalConf = (PortalConf)session.get(PortalConfImpl.class,
						portalConf.getPrimaryKeyObj());
			}

			if (portalConf != null) {
				session.delete(portalConf);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (portalConf != null) {
			clearCache(portalConf);
		}

		return portalConf;
	}

	@Override
	public PortalConf updateImpl(it.eng.model.PortalConf portalConf)
		throws SystemException {
		portalConf = toUnwrappedModel(portalConf);

		boolean isNew = portalConf.isNew();

		PortalConfModelImpl portalConfModelImpl = (PortalConfModelImpl)portalConf;

		Session session = null;

		try {
			session = openSession();

			if (portalConf.isNew()) {
				session.save(portalConf);

				portalConf.setNew(false);
			}
			else {
				session.merge(portalConf);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PortalConfModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((portalConfModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONFNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						portalConfModelImpl.getOriginalConfName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CONFNAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONFNAME,
					args);

				args = new Object[] { portalConfModelImpl.getConfName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CONFNAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONFNAME,
					args);
			}
		}

		EntityCacheUtil.putResult(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
			PortalConfImpl.class, portalConf.getPrimaryKey(), portalConf);

		return portalConf;
	}

	protected PortalConf toUnwrappedModel(PortalConf portalConf) {
		if (portalConf instanceof PortalConfImpl) {
			return portalConf;
		}

		PortalConfImpl portalConfImpl = new PortalConfImpl();

		portalConfImpl.setNew(portalConf.isNew());
		portalConfImpl.setPrimaryKey(portalConf.getPrimaryKey());

		portalConfImpl.setConfName(portalConf.getConfName());
		portalConfImpl.setTomcatKsAlias(portalConf.getTomcatKsAlias());
		portalConfImpl.setTomcatKsPassword(portalConf.getTomcatKsPassword());
		portalConfImpl.setTomcatKsLocation(portalConf.getTomcatKsLocation());
		portalConfImpl.setJvmKsLocation(portalConf.getJvmKsLocation());
		portalConfImpl.setJvmKsPassword(portalConf.getJvmKsPassword());

		return portalConfImpl;
	}

	/**
	 * Returns the portal conf with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the portal conf
	 * @return the portal conf
	 * @throws it.eng.NoSuchPortalConfException if a portal conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPortalConfException, SystemException {
		PortalConf portalConf = fetchByPrimaryKey(primaryKey);

		if (portalConf == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPortalConfException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return portalConf;
	}

	/**
	 * Returns the portal conf with the primary key or throws a {@link it.eng.NoSuchPortalConfException} if it could not be found.
	 *
	 * @param confName the primary key of the portal conf
	 * @return the portal conf
	 * @throws it.eng.NoSuchPortalConfException if a portal conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf findByPrimaryKey(String confName)
		throws NoSuchPortalConfException, SystemException {
		return findByPrimaryKey((Serializable)confName);
	}

	/**
	 * Returns the portal conf with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the portal conf
	 * @return the portal conf, or <code>null</code> if a portal conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PortalConf portalConf = (PortalConf)EntityCacheUtil.getResult(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
				PortalConfImpl.class, primaryKey);

		if (portalConf == _nullPortalConf) {
			return null;
		}

		if (portalConf == null) {
			Session session = null;

			try {
				session = openSession();

				portalConf = (PortalConf)session.get(PortalConfImpl.class,
						primaryKey);

				if (portalConf != null) {
					cacheResult(portalConf);
				}
				else {
					EntityCacheUtil.putResult(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
						PortalConfImpl.class, primaryKey, _nullPortalConf);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PortalConfModelImpl.ENTITY_CACHE_ENABLED,
					PortalConfImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return portalConf;
	}

	/**
	 * Returns the portal conf with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param confName the primary key of the portal conf
	 * @return the portal conf, or <code>null</code> if a portal conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PortalConf fetchByPrimaryKey(String confName)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)confName);
	}

	/**
	 * Returns all the portal confs.
	 *
	 * @return the portal confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PortalConf> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the portal confs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of portal confs
	 * @param end the upper bound of the range of portal confs (not inclusive)
	 * @return the range of portal confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PortalConf> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the portal confs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.PortalConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of portal confs
	 * @param end the upper bound of the range of portal confs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of portal confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PortalConf> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PortalConf> list = (List<PortalConf>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PORTALCONF);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PORTALCONF;

				if (pagination) {
					sql = sql.concat(PortalConfModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PortalConf>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PortalConf>(list);
				}
				else {
					list = (List<PortalConf>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the portal confs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PortalConf portalConf : findAll()) {
			remove(portalConf);
		}
	}

	/**
	 * Returns the number of portal confs.
	 *
	 * @return the number of portal confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PORTALCONF);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the portal conf persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.model.PortalConf")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PortalConf>> listenersList = new ArrayList<ModelListener<PortalConf>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PortalConf>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PortalConfImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PORTALCONF = "SELECT portalConf FROM PortalConf portalConf";
	private static final String _SQL_SELECT_PORTALCONF_WHERE = "SELECT portalConf FROM PortalConf portalConf WHERE ";
	private static final String _SQL_COUNT_PORTALCONF = "SELECT COUNT(portalConf) FROM PortalConf portalConf";
	private static final String _SQL_COUNT_PORTALCONF_WHERE = "SELECT COUNT(portalConf) FROM PortalConf portalConf WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "portalConf.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PortalConf exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PortalConf exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PortalConfPersistenceImpl.class);
	private static PortalConf _nullPortalConf = new PortalConfImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PortalConf> toCacheModel() {
				return _nullPortalConfCacheModel;
			}
		};

	private static CacheModel<PortalConf> _nullPortalConfCacheModel = new CacheModel<PortalConf>() {
			@Override
			public PortalConf toEntityModel() {
				return _nullPortalConf;
			}
		};
}