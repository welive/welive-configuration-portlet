/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.NoSuchExtModuleException;

import it.eng.model.ExtModule;
import it.eng.model.impl.ExtModuleImpl;
import it.eng.model.impl.ExtModuleModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the ext module service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see ExtModulePersistence
 * @see ExtModuleUtil
 * @generated
 */
public class ExtModulePersistenceImpl extends BasePersistenceImpl<ExtModule>
	implements ExtModulePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ExtModuleUtil} to access the ext module persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ExtModuleImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULEID = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModuleId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEID =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByModuleId",
			new String[] { Long.class.getName() },
			ExtModuleModelImpl.MODULEID_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MODULEID = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByModuleId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the ext modules where moduleId = &#63;.
	 *
	 * @param moduleId the module ID
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleId(long moduleId)
		throws SystemException {
		return findByModuleId(moduleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the ext modules where moduleId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleId the module ID
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleId(long moduleId, int start, int end)
		throws SystemException {
		return findByModuleId(moduleId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where moduleId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleId the module ID
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleId(long moduleId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEID;
			finderArgs = new Object[] { moduleId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULEID;
			finderArgs = new Object[] { moduleId, start, end, orderByComparator };
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if ((moduleId != extModule.getModuleId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_MODULEID_MODULEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(moduleId);

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where moduleId = &#63;.
	 *
	 * @param moduleId the module ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleId_First(long moduleId,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleId_First(moduleId, orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleId=");
		msg.append(moduleId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where moduleId = &#63;.
	 *
	 * @param moduleId the module ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleId_First(long moduleId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByModuleId(moduleId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where moduleId = &#63;.
	 *
	 * @param moduleId the module ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleId_Last(long moduleId,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleId_Last(moduleId, orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleId=");
		msg.append(moduleId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where moduleId = &#63;.
	 *
	 * @param moduleId the module ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleId_Last(long moduleId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByModuleId(moduleId);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByModuleId(moduleId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the ext modules where moduleId = &#63; from the database.
	 *
	 * @param moduleId the module ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByModuleId(long moduleId) throws SystemException {
		for (ExtModule extModule : findByModuleId(moduleId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where moduleId = &#63;.
	 *
	 * @param moduleId the module ID
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByModuleId(long moduleId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MODULEID;

		Object[] finderArgs = new Object[] { moduleId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_MODULEID_MODULEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(moduleId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MODULEID_MODULEID_2 = "extModule.moduleId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULENAME =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModuleName",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULENAME =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByModuleName",
			new String[] { String.class.getName() },
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MODULENAME = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByModuleName",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ext modules where moduleName = &#63;.
	 *
	 * @param moduleName the module name
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleName(String moduleName)
		throws SystemException {
		return findByModuleName(moduleName, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where moduleName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleName the module name
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleName(String moduleName, int start,
		int end) throws SystemException {
		return findByModuleName(moduleName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where moduleName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleName the module name
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleName(String moduleName, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULENAME;
			finderArgs = new Object[] { moduleName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULENAME;
			finderArgs = new Object[] { moduleName, start, end, orderByComparator };
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(moduleName, extModule.getModuleName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			boolean bindModuleName = false;

			if (moduleName == null) {
				query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_1);
			}
			else if (moduleName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_3);
			}
			else {
				bindModuleName = true;

				query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleName) {
					qPos.add(moduleName);
				}

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where moduleName = &#63;.
	 *
	 * @param moduleName the module name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleName_First(String moduleName,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleName_First(moduleName,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleName=");
		msg.append(moduleName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where moduleName = &#63;.
	 *
	 * @param moduleName the module name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleName_First(String moduleName,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByModuleName(moduleName, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where moduleName = &#63;.
	 *
	 * @param moduleName the module name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleName_Last(String moduleName,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleName_Last(moduleName,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleName=");
		msg.append(moduleName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where moduleName = &#63;.
	 *
	 * @param moduleName the module name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleName_Last(String moduleName,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByModuleName(moduleName);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByModuleName(moduleName, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where moduleName = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param moduleName the module name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByModuleName_PrevAndNext(long moduleId,
		String moduleName, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByModuleName_PrevAndNext(session, extModule,
					moduleName, orderByComparator, true);

			array[1] = extModule;

			array[2] = getByModuleName_PrevAndNext(session, extModule,
					moduleName, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByModuleName_PrevAndNext(Session session,
		ExtModule extModule, String moduleName,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		boolean bindModuleName = false;

		if (moduleName == null) {
			query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_1);
		}
		else if (moduleName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_3);
		}
		else {
			bindModuleName = true;

			query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindModuleName) {
			qPos.add(moduleName);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where moduleName = &#63; from the database.
	 *
	 * @param moduleName the module name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByModuleName(String moduleName) throws SystemException {
		for (ExtModule extModule : findByModuleName(moduleName,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where moduleName = &#63;.
	 *
	 * @param moduleName the module name
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByModuleName(String moduleName) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MODULENAME;

		Object[] finderArgs = new Object[] { moduleName };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			boolean bindModuleName = false;

			if (moduleName == null) {
				query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_1);
			}
			else if (moduleName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_3);
			}
			else {
				bindModuleName = true;

				query.append(_FINDER_COLUMN_MODULENAME_MODULENAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleName) {
					qPos.add(moduleName);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MODULENAME_MODULENAME_1 = "extModule.moduleName IS NULL";
	private static final String _FINDER_COLUMN_MODULENAME_MODULENAME_2 = "extModule.moduleName = ?";
	private static final String _FINDER_COLUMN_MODULENAME_MODULENAME_3 = "(extModule.moduleName IS NULL OR extModule.moduleName = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULEURL =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModuleURL",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEURL =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByModuleURL",
			new String[] { String.class.getName() },
			ExtModuleModelImpl.MODULEURL_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MODULEURL = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByModuleURL",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ext modules where moduleURL = &#63;.
	 *
	 * @param moduleURL the module u r l
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleURL(String moduleURL)
		throws SystemException {
		return findByModuleURL(moduleURL, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the ext modules where moduleURL = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleURL the module u r l
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleURL(String moduleURL, int start, int end)
		throws SystemException {
		return findByModuleURL(moduleURL, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where moduleURL = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleURL the module u r l
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleURL(String moduleURL, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEURL;
			finderArgs = new Object[] { moduleURL };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULEURL;
			finderArgs = new Object[] { moduleURL, start, end, orderByComparator };
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(moduleURL, extModule.getModuleURL())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			boolean bindModuleURL = false;

			if (moduleURL == null) {
				query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_1);
			}
			else if (moduleURL.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_3);
			}
			else {
				bindModuleURL = true;

				query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleURL) {
					qPos.add(moduleURL);
				}

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where moduleURL = &#63;.
	 *
	 * @param moduleURL the module u r l
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleURL_First(String moduleURL,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleURL_First(moduleURL,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleURL=");
		msg.append(moduleURL);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where moduleURL = &#63;.
	 *
	 * @param moduleURL the module u r l
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleURL_First(String moduleURL,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByModuleURL(moduleURL, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where moduleURL = &#63;.
	 *
	 * @param moduleURL the module u r l
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleURL_Last(String moduleURL,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleURL_Last(moduleURL, orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleURL=");
		msg.append(moduleURL);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where moduleURL = &#63;.
	 *
	 * @param moduleURL the module u r l
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleURL_Last(String moduleURL,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByModuleURL(moduleURL);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByModuleURL(moduleURL, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where moduleURL = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param moduleURL the module u r l
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByModuleURL_PrevAndNext(long moduleId,
		String moduleURL, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByModuleURL_PrevAndNext(session, extModule,
					moduleURL, orderByComparator, true);

			array[1] = extModule;

			array[2] = getByModuleURL_PrevAndNext(session, extModule,
					moduleURL, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByModuleURL_PrevAndNext(Session session,
		ExtModule extModule, String moduleURL,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		boolean bindModuleURL = false;

		if (moduleURL == null) {
			query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_1);
		}
		else if (moduleURL.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_3);
		}
		else {
			bindModuleURL = true;

			query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindModuleURL) {
			qPos.add(moduleURL);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where moduleURL = &#63; from the database.
	 *
	 * @param moduleURL the module u r l
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByModuleURL(String moduleURL) throws SystemException {
		for (ExtModule extModule : findByModuleURL(moduleURL,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where moduleURL = &#63;.
	 *
	 * @param moduleURL the module u r l
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByModuleURL(String moduleURL) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MODULEURL;

		Object[] finderArgs = new Object[] { moduleURL };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			boolean bindModuleURL = false;

			if (moduleURL == null) {
				query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_1);
			}
			else if (moduleURL.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_3);
			}
			else {
				bindModuleURL = true;

				query.append(_FINDER_COLUMN_MODULEURL_MODULEURL_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleURL) {
					qPos.add(moduleURL);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MODULEURL_MODULEURL_1 = "extModule.moduleURL IS NULL";
	private static final String _FINDER_COLUMN_MODULEURL_MODULEURL_2 = "extModule.moduleURL = ?";
	private static final String _FINDER_COLUMN_MODULEURL_MODULEURL_3 = "(extModule.moduleURL IS NULL OR extModule.moduleURL = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULEENDPOINT =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModuleEndpoint",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEENDPOINT =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByModuleEndpoint",
			new String[] { String.class.getName() },
			ExtModuleModelImpl.MODULEENDPOINT_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MODULEENDPOINT = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByModuleEndpoint",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ext modules where moduleEndpoint = &#63;.
	 *
	 * @param moduleEndpoint the module endpoint
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleEndpoint(String moduleEndpoint)
		throws SystemException {
		return findByModuleEndpoint(moduleEndpoint, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where moduleEndpoint = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleEndpoint the module endpoint
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleEndpoint(String moduleEndpoint,
		int start, int end) throws SystemException {
		return findByModuleEndpoint(moduleEndpoint, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where moduleEndpoint = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleEndpoint the module endpoint
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleEndpoint(String moduleEndpoint,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEENDPOINT;
			finderArgs = new Object[] { moduleEndpoint };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULEENDPOINT;
			finderArgs = new Object[] {
					moduleEndpoint,
					
					start, end, orderByComparator
				};
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(moduleEndpoint,
							extModule.getModuleEndpoint())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			boolean bindModuleEndpoint = false;

			if (moduleEndpoint == null) {
				query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_1);
			}
			else if (moduleEndpoint.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_3);
			}
			else {
				bindModuleEndpoint = true;

				query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleEndpoint) {
					qPos.add(moduleEndpoint);
				}

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where moduleEndpoint = &#63;.
	 *
	 * @param moduleEndpoint the module endpoint
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleEndpoint_First(String moduleEndpoint,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleEndpoint_First(moduleEndpoint,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleEndpoint=");
		msg.append(moduleEndpoint);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where moduleEndpoint = &#63;.
	 *
	 * @param moduleEndpoint the module endpoint
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleEndpoint_First(String moduleEndpoint,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByModuleEndpoint(moduleEndpoint, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where moduleEndpoint = &#63;.
	 *
	 * @param moduleEndpoint the module endpoint
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleEndpoint_Last(String moduleEndpoint,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleEndpoint_Last(moduleEndpoint,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleEndpoint=");
		msg.append(moduleEndpoint);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where moduleEndpoint = &#63;.
	 *
	 * @param moduleEndpoint the module endpoint
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleEndpoint_Last(String moduleEndpoint,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByModuleEndpoint(moduleEndpoint);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByModuleEndpoint(moduleEndpoint, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where moduleEndpoint = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param moduleEndpoint the module endpoint
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByModuleEndpoint_PrevAndNext(long moduleId,
		String moduleEndpoint, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByModuleEndpoint_PrevAndNext(session, extModule,
					moduleEndpoint, orderByComparator, true);

			array[1] = extModule;

			array[2] = getByModuleEndpoint_PrevAndNext(session, extModule,
					moduleEndpoint, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByModuleEndpoint_PrevAndNext(Session session,
		ExtModule extModule, String moduleEndpoint,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		boolean bindModuleEndpoint = false;

		if (moduleEndpoint == null) {
			query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_1);
		}
		else if (moduleEndpoint.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_3);
		}
		else {
			bindModuleEndpoint = true;

			query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindModuleEndpoint) {
			qPos.add(moduleEndpoint);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where moduleEndpoint = &#63; from the database.
	 *
	 * @param moduleEndpoint the module endpoint
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByModuleEndpoint(String moduleEndpoint)
		throws SystemException {
		for (ExtModule extModule : findByModuleEndpoint(moduleEndpoint,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where moduleEndpoint = &#63;.
	 *
	 * @param moduleEndpoint the module endpoint
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByModuleEndpoint(String moduleEndpoint)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MODULEENDPOINT;

		Object[] finderArgs = new Object[] { moduleEndpoint };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			boolean bindModuleEndpoint = false;

			if (moduleEndpoint == null) {
				query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_1);
			}
			else if (moduleEndpoint.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_3);
			}
			else {
				bindModuleEndpoint = true;

				query.append(_FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleEndpoint) {
					qPos.add(moduleEndpoint);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_1 = "extModule.moduleEndpoint IS NULL";
	private static final String _FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_2 = "extModule.moduleEndpoint = ?";
	private static final String _FINDER_COLUMN_MODULEENDPOINT_MODULEENDPOINT_3 = "(extModule.moduleEndpoint IS NULL OR extModule.moduleEndpoint = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULEALIAS =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModuleAlias",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEALIAS =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByModuleAlias",
			new String[] { String.class.getName() },
			ExtModuleModelImpl.MODULEALIAS_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MODULEALIAS = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByModuleAlias",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ext modules where moduleAlias = &#63;.
	 *
	 * @param moduleAlias the module alias
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleAlias(String moduleAlias)
		throws SystemException {
		return findByModuleAlias(moduleAlias, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where moduleAlias = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleAlias the module alias
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleAlias(String moduleAlias, int start,
		int end) throws SystemException {
		return findByModuleAlias(moduleAlias, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where moduleAlias = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleAlias the module alias
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleAlias(String moduleAlias, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEALIAS;
			finderArgs = new Object[] { moduleAlias };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULEALIAS;
			finderArgs = new Object[] { moduleAlias, start, end, orderByComparator };
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(moduleAlias, extModule.getModuleAlias())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			boolean bindModuleAlias = false;

			if (moduleAlias == null) {
				query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_1);
			}
			else if (moduleAlias.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_3);
			}
			else {
				bindModuleAlias = true;

				query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleAlias) {
					qPos.add(moduleAlias);
				}

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where moduleAlias = &#63;.
	 *
	 * @param moduleAlias the module alias
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleAlias_First(String moduleAlias,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleAlias_First(moduleAlias,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleAlias=");
		msg.append(moduleAlias);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where moduleAlias = &#63;.
	 *
	 * @param moduleAlias the module alias
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleAlias_First(String moduleAlias,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByModuleAlias(moduleAlias, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where moduleAlias = &#63;.
	 *
	 * @param moduleAlias the module alias
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleAlias_Last(String moduleAlias,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleAlias_Last(moduleAlias,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleAlias=");
		msg.append(moduleAlias);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where moduleAlias = &#63;.
	 *
	 * @param moduleAlias the module alias
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleAlias_Last(String moduleAlias,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByModuleAlias(moduleAlias);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByModuleAlias(moduleAlias, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where moduleAlias = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param moduleAlias the module alias
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByModuleAlias_PrevAndNext(long moduleId,
		String moduleAlias, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByModuleAlias_PrevAndNext(session, extModule,
					moduleAlias, orderByComparator, true);

			array[1] = extModule;

			array[2] = getByModuleAlias_PrevAndNext(session, extModule,
					moduleAlias, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByModuleAlias_PrevAndNext(Session session,
		ExtModule extModule, String moduleAlias,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		boolean bindModuleAlias = false;

		if (moduleAlias == null) {
			query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_1);
		}
		else if (moduleAlias.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_3);
		}
		else {
			bindModuleAlias = true;

			query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindModuleAlias) {
			qPos.add(moduleAlias);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where moduleAlias = &#63; from the database.
	 *
	 * @param moduleAlias the module alias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByModuleAlias(String moduleAlias)
		throws SystemException {
		for (ExtModule extModule : findByModuleAlias(moduleAlias,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where moduleAlias = &#63;.
	 *
	 * @param moduleAlias the module alias
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByModuleAlias(String moduleAlias) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MODULEALIAS;

		Object[] finderArgs = new Object[] { moduleAlias };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			boolean bindModuleAlias = false;

			if (moduleAlias == null) {
				query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_1);
			}
			else if (moduleAlias.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_3);
			}
			else {
				bindModuleAlias = true;

				query.append(_FINDER_COLUMN_MODULEALIAS_MODULEALIAS_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleAlias) {
					qPos.add(moduleAlias);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MODULEALIAS_MODULEALIAS_1 = "extModule.moduleAlias IS NULL";
	private static final String _FINDER_COLUMN_MODULEALIAS_MODULEALIAS_2 = "extModule.moduleAlias = ?";
	private static final String _FINDER_COLUMN_MODULEALIAS_MODULEALIAS_3 = "(extModule.moduleAlias IS NULL OR extModule.moduleAlias = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULESHAREDSECRET =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModuleSharedSecret",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULESHAREDSECRET =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByModuleSharedSecret",
			new String[] { String.class.getName() },
			ExtModuleModelImpl.MODULESHAREDSECRET_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MODULESHAREDSECRET = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByModuleSharedSecret", new String[] { String.class.getName() });

	/**
	 * Returns all the ext modules where moduleSharedSecret = &#63;.
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleSharedSecret(String moduleSharedSecret)
		throws SystemException {
		return findByModuleSharedSecret(moduleSharedSecret, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where moduleSharedSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleSharedSecret(String moduleSharedSecret,
		int start, int end) throws SystemException {
		return findByModuleSharedSecret(moduleSharedSecret, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where moduleSharedSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByModuleSharedSecret(String moduleSharedSecret,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULESHAREDSECRET;
			finderArgs = new Object[] { moduleSharedSecret };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODULESHAREDSECRET;
			finderArgs = new Object[] {
					moduleSharedSecret,
					
					start, end, orderByComparator
				};
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(moduleSharedSecret,
							extModule.getModuleSharedSecret())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			boolean bindModuleSharedSecret = false;

			if (moduleSharedSecret == null) {
				query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_1);
			}
			else if (moduleSharedSecret.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_3);
			}
			else {
				bindModuleSharedSecret = true;

				query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleSharedSecret) {
					qPos.add(moduleSharedSecret);
				}

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where moduleSharedSecret = &#63;.
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleSharedSecret_First(String moduleSharedSecret,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleSharedSecret_First(moduleSharedSecret,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleSharedSecret=");
		msg.append(moduleSharedSecret);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where moduleSharedSecret = &#63;.
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleSharedSecret_First(
		String moduleSharedSecret, OrderByComparator orderByComparator)
		throws SystemException {
		List<ExtModule> list = findByModuleSharedSecret(moduleSharedSecret, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where moduleSharedSecret = &#63;.
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByModuleSharedSecret_Last(String moduleSharedSecret,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByModuleSharedSecret_Last(moduleSharedSecret,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("moduleSharedSecret=");
		msg.append(moduleSharedSecret);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where moduleSharedSecret = &#63;.
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByModuleSharedSecret_Last(String moduleSharedSecret,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByModuleSharedSecret(moduleSharedSecret);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByModuleSharedSecret(moduleSharedSecret,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where moduleSharedSecret = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param moduleSharedSecret the module shared secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByModuleSharedSecret_PrevAndNext(long moduleId,
		String moduleSharedSecret, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByModuleSharedSecret_PrevAndNext(session, extModule,
					moduleSharedSecret, orderByComparator, true);

			array[1] = extModule;

			array[2] = getByModuleSharedSecret_PrevAndNext(session, extModule,
					moduleSharedSecret, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByModuleSharedSecret_PrevAndNext(Session session,
		ExtModule extModule, String moduleSharedSecret,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		boolean bindModuleSharedSecret = false;

		if (moduleSharedSecret == null) {
			query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_1);
		}
		else if (moduleSharedSecret.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_3);
		}
		else {
			bindModuleSharedSecret = true;

			query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindModuleSharedSecret) {
			qPos.add(moduleSharedSecret);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where moduleSharedSecret = &#63; from the database.
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByModuleSharedSecret(String moduleSharedSecret)
		throws SystemException {
		for (ExtModule extModule : findByModuleSharedSecret(
				moduleSharedSecret, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where moduleSharedSecret = &#63;.
	 *
	 * @param moduleSharedSecret the module shared secret
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByModuleSharedSecret(String moduleSharedSecret)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MODULESHAREDSECRET;

		Object[] finderArgs = new Object[] { moduleSharedSecret };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			boolean bindModuleSharedSecret = false;

			if (moduleSharedSecret == null) {
				query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_1);
			}
			else if (moduleSharedSecret.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_3);
			}
			else {
				bindModuleSharedSecret = true;

				query.append(_FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindModuleSharedSecret) {
					qPos.add(moduleSharedSecret);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_1 =
		"extModule.moduleSharedSecret IS NULL";
	private static final String _FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_2 =
		"extModule.moduleSharedSecret = ?";
	private static final String _FINDER_COLUMN_MODULESHAREDSECRET_MODULESHAREDSECRET_3 =
		"(extModule.moduleSharedSecret IS NULL OR extModule.moduleSharedSecret = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REGISTRATIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByRegistrationPropagation",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REGISTRATIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByRegistrationPropagation",
			new String[] { Boolean.class.getName() },
			ExtModuleModelImpl.REGISTRATIONPROPAGATION_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REGISTRATIONPROPAGATION = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByRegistrationPropagation",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the ext modules where registrationPropagation = &#63;.
	 *
	 * @param registrationPropagation the registration propagation
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByRegistrationPropagation(
		Boolean registrationPropagation) throws SystemException {
		return findByRegistrationPropagation(registrationPropagation,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where registrationPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param registrationPropagation the registration propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByRegistrationPropagation(
		Boolean registrationPropagation, int start, int end)
		throws SystemException {
		return findByRegistrationPropagation(registrationPropagation, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where registrationPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param registrationPropagation the registration propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByRegistrationPropagation(
		Boolean registrationPropagation, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REGISTRATIONPROPAGATION;
			finderArgs = new Object[] { registrationPropagation };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REGISTRATIONPROPAGATION;
			finderArgs = new Object[] {
					registrationPropagation,
					
					start, end, orderByComparator
				};
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(registrationPropagation,
							extModule.getRegistrationPropagation())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_REGISTRATIONPROPAGATION_REGISTRATIONPROPAGATION_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(registrationPropagation.booleanValue());

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where registrationPropagation = &#63;.
	 *
	 * @param registrationPropagation the registration propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByRegistrationPropagation_First(
		Boolean registrationPropagation, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByRegistrationPropagation_First(registrationPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("registrationPropagation=");
		msg.append(registrationPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where registrationPropagation = &#63;.
	 *
	 * @param registrationPropagation the registration propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByRegistrationPropagation_First(
		Boolean registrationPropagation, OrderByComparator orderByComparator)
		throws SystemException {
		List<ExtModule> list = findByRegistrationPropagation(registrationPropagation,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where registrationPropagation = &#63;.
	 *
	 * @param registrationPropagation the registration propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByRegistrationPropagation_Last(
		Boolean registrationPropagation, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByRegistrationPropagation_Last(registrationPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("registrationPropagation=");
		msg.append(registrationPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where registrationPropagation = &#63;.
	 *
	 * @param registrationPropagation the registration propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByRegistrationPropagation_Last(
		Boolean registrationPropagation, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByRegistrationPropagation(registrationPropagation);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByRegistrationPropagation(registrationPropagation,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where registrationPropagation = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param registrationPropagation the registration propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByRegistrationPropagation_PrevAndNext(
		long moduleId, Boolean registrationPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByRegistrationPropagation_PrevAndNext(session,
					extModule, registrationPropagation, orderByComparator, true);

			array[1] = extModule;

			array[2] = getByRegistrationPropagation_PrevAndNext(session,
					extModule, registrationPropagation, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByRegistrationPropagation_PrevAndNext(
		Session session, ExtModule extModule, Boolean registrationPropagation,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		query.append(_FINDER_COLUMN_REGISTRATIONPROPAGATION_REGISTRATIONPROPAGATION_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(registrationPropagation.booleanValue());

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where registrationPropagation = &#63; from the database.
	 *
	 * @param registrationPropagation the registration propagation
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByRegistrationPropagation(Boolean registrationPropagation)
		throws SystemException {
		for (ExtModule extModule : findByRegistrationPropagation(
				registrationPropagation, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where registrationPropagation = &#63;.
	 *
	 * @param registrationPropagation the registration propagation
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByRegistrationPropagation(Boolean registrationPropagation)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REGISTRATIONPROPAGATION;

		Object[] finderArgs = new Object[] { registrationPropagation };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_REGISTRATIONPROPAGATION_REGISTRATIONPROPAGATION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(registrationPropagation.booleanValue());

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REGISTRATIONPROPAGATION_REGISTRATIONPROPAGATION_2 =
		"extModule.registrationPropagation = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DELETIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByDeletionPropagation",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByDeletionPropagation",
			new String[] { Boolean.class.getName() },
			ExtModuleModelImpl.DELETIONPROPAGATION_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DELETIONPROPAGATION = new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByDeletionPropagation",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the ext modules where deletionPropagation = &#63;.
	 *
	 * @param deletionPropagation the deletion propagation
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByDeletionPropagation(
		Boolean deletionPropagation) throws SystemException {
		return findByDeletionPropagation(deletionPropagation,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where deletionPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param deletionPropagation the deletion propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByDeletionPropagation(
		Boolean deletionPropagation, int start, int end)
		throws SystemException {
		return findByDeletionPropagation(deletionPropagation, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where deletionPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param deletionPropagation the deletion propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByDeletionPropagation(
		Boolean deletionPropagation, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETIONPROPAGATION;
			finderArgs = new Object[] { deletionPropagation };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DELETIONPROPAGATION;
			finderArgs = new Object[] {
					deletionPropagation,
					
					start, end, orderByComparator
				};
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(deletionPropagation,
							extModule.getDeletionPropagation())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_DELETIONPROPAGATION_DELETIONPROPAGATION_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(deletionPropagation.booleanValue());

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where deletionPropagation = &#63;.
	 *
	 * @param deletionPropagation the deletion propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByDeletionPropagation_First(
		Boolean deletionPropagation, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByDeletionPropagation_First(deletionPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("deletionPropagation=");
		msg.append(deletionPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where deletionPropagation = &#63;.
	 *
	 * @param deletionPropagation the deletion propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByDeletionPropagation_First(
		Boolean deletionPropagation, OrderByComparator orderByComparator)
		throws SystemException {
		List<ExtModule> list = findByDeletionPropagation(deletionPropagation,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where deletionPropagation = &#63;.
	 *
	 * @param deletionPropagation the deletion propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByDeletionPropagation_Last(
		Boolean deletionPropagation, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByDeletionPropagation_Last(deletionPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("deletionPropagation=");
		msg.append(deletionPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where deletionPropagation = &#63;.
	 *
	 * @param deletionPropagation the deletion propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByDeletionPropagation_Last(
		Boolean deletionPropagation, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByDeletionPropagation(deletionPropagation);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByDeletionPropagation(deletionPropagation,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where deletionPropagation = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param deletionPropagation the deletion propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByDeletionPropagation_PrevAndNext(long moduleId,
		Boolean deletionPropagation, OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByDeletionPropagation_PrevAndNext(session, extModule,
					deletionPropagation, orderByComparator, true);

			array[1] = extModule;

			array[2] = getByDeletionPropagation_PrevAndNext(session, extModule,
					deletionPropagation, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByDeletionPropagation_PrevAndNext(Session session,
		ExtModule extModule, Boolean deletionPropagation,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		query.append(_FINDER_COLUMN_DELETIONPROPAGATION_DELETIONPROPAGATION_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(deletionPropagation.booleanValue());

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where deletionPropagation = &#63; from the database.
	 *
	 * @param deletionPropagation the deletion propagation
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByDeletionPropagation(Boolean deletionPropagation)
		throws SystemException {
		for (ExtModule extModule : findByDeletionPropagation(
				deletionPropagation, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where deletionPropagation = &#63;.
	 *
	 * @param deletionPropagation the deletion propagation
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDeletionPropagation(Boolean deletionPropagation)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DELETIONPROPAGATION;

		Object[] finderArgs = new Object[] { deletionPropagation };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_DELETIONPROPAGATION_DELETIONPROPAGATION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(deletionPropagation.booleanValue());

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DELETIONPROPAGATION_DELETIONPROPAGATION_2 =
		"extModule.deletionPropagation = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UPSERTORGANIZATIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUpsertOrganizationPropagation",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UPSERTORGANIZATIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUpsertOrganizationPropagation",
			new String[] { Boolean.class.getName() },
			ExtModuleModelImpl.UPSERTORGANIZATIONPROPAGATION_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UPSERTORGANIZATIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUpsertOrganizationPropagation",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the ext modules where upsertOrganizationPropagation = &#63;.
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByUpsertOrganizationPropagation(
		Boolean upsertOrganizationPropagation) throws SystemException {
		return findByUpsertOrganizationPropagation(upsertOrganizationPropagation,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where upsertOrganizationPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByUpsertOrganizationPropagation(
		Boolean upsertOrganizationPropagation, int start, int end)
		throws SystemException {
		return findByUpsertOrganizationPropagation(upsertOrganizationPropagation,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where upsertOrganizationPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByUpsertOrganizationPropagation(
		Boolean upsertOrganizationPropagation, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UPSERTORGANIZATIONPROPAGATION;
			finderArgs = new Object[] { upsertOrganizationPropagation };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UPSERTORGANIZATIONPROPAGATION;
			finderArgs = new Object[] {
					upsertOrganizationPropagation,
					
					start, end, orderByComparator
				};
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(upsertOrganizationPropagation,
							extModule.getUpsertOrganizationPropagation())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_UPSERTORGANIZATIONPROPAGATION_UPSERTORGANIZATIONPROPAGATION_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(upsertOrganizationPropagation.booleanValue());

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByUpsertOrganizationPropagation_First(
		Boolean upsertOrganizationPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByUpsertOrganizationPropagation_First(upsertOrganizationPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("upsertOrganizationPropagation=");
		msg.append(upsertOrganizationPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByUpsertOrganizationPropagation_First(
		Boolean upsertOrganizationPropagation,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByUpsertOrganizationPropagation(upsertOrganizationPropagation,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByUpsertOrganizationPropagation_Last(
		Boolean upsertOrganizationPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByUpsertOrganizationPropagation_Last(upsertOrganizationPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("upsertOrganizationPropagation=");
		msg.append(upsertOrganizationPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByUpsertOrganizationPropagation_Last(
		Boolean upsertOrganizationPropagation,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUpsertOrganizationPropagation(upsertOrganizationPropagation);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByUpsertOrganizationPropagation(upsertOrganizationPropagation,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where upsertOrganizationPropagation = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByUpsertOrganizationPropagation_PrevAndNext(
		long moduleId, Boolean upsertOrganizationPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByUpsertOrganizationPropagation_PrevAndNext(session,
					extModule, upsertOrganizationPropagation,
					orderByComparator, true);

			array[1] = extModule;

			array[2] = getByUpsertOrganizationPropagation_PrevAndNext(session,
					extModule, upsertOrganizationPropagation,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByUpsertOrganizationPropagation_PrevAndNext(
		Session session, ExtModule extModule,
		Boolean upsertOrganizationPropagation,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		query.append(_FINDER_COLUMN_UPSERTORGANIZATIONPROPAGATION_UPSERTORGANIZATIONPROPAGATION_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(upsertOrganizationPropagation.booleanValue());

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where upsertOrganizationPropagation = &#63; from the database.
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUpsertOrganizationPropagation(
		Boolean upsertOrganizationPropagation) throws SystemException {
		for (ExtModule extModule : findByUpsertOrganizationPropagation(
				upsertOrganizationPropagation, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where upsertOrganizationPropagation = &#63;.
	 *
	 * @param upsertOrganizationPropagation the upsert organization propagation
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUpsertOrganizationPropagation(
		Boolean upsertOrganizationPropagation) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UPSERTORGANIZATIONPROPAGATION;

		Object[] finderArgs = new Object[] { upsertOrganizationPropagation };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_UPSERTORGANIZATIONPROPAGATION_UPSERTORGANIZATIONPROPAGATION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(upsertOrganizationPropagation.booleanValue());

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UPSERTORGANIZATIONPROPAGATION_UPSERTORGANIZATIONPROPAGATION_2 =
		"extModule.upsertOrganizationPropagation = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DELETEORGANIZATIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByDeleteOrganizationPropagation",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETEORGANIZATIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByDeleteOrganizationPropagation",
			new String[] { Boolean.class.getName() },
			ExtModuleModelImpl.DELETEORGANIZATIONPROPAGATION_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DELETEORGANIZATIONPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByDeleteOrganizationPropagation",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the ext modules where deleteOrganizationPropagation = &#63;.
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByDeleteOrganizationPropagation(
		Boolean deleteOrganizationPropagation) throws SystemException {
		return findByDeleteOrganizationPropagation(deleteOrganizationPropagation,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where deleteOrganizationPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByDeleteOrganizationPropagation(
		Boolean deleteOrganizationPropagation, int start, int end)
		throws SystemException {
		return findByDeleteOrganizationPropagation(deleteOrganizationPropagation,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where deleteOrganizationPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByDeleteOrganizationPropagation(
		Boolean deleteOrganizationPropagation, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETEORGANIZATIONPROPAGATION;
			finderArgs = new Object[] { deleteOrganizationPropagation };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DELETEORGANIZATIONPROPAGATION;
			finderArgs = new Object[] {
					deleteOrganizationPropagation,
					
					start, end, orderByComparator
				};
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(deleteOrganizationPropagation,
							extModule.getDeleteOrganizationPropagation())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_DELETEORGANIZATIONPROPAGATION_DELETEORGANIZATIONPROPAGATION_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(deleteOrganizationPropagation.booleanValue());

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByDeleteOrganizationPropagation_First(
		Boolean deleteOrganizationPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByDeleteOrganizationPropagation_First(deleteOrganizationPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("deleteOrganizationPropagation=");
		msg.append(deleteOrganizationPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByDeleteOrganizationPropagation_First(
		Boolean deleteOrganizationPropagation,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByDeleteOrganizationPropagation(deleteOrganizationPropagation,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByDeleteOrganizationPropagation_Last(
		Boolean deleteOrganizationPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByDeleteOrganizationPropagation_Last(deleteOrganizationPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("deleteOrganizationPropagation=");
		msg.append(deleteOrganizationPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByDeleteOrganizationPropagation_Last(
		Boolean deleteOrganizationPropagation,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByDeleteOrganizationPropagation(deleteOrganizationPropagation);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByDeleteOrganizationPropagation(deleteOrganizationPropagation,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where deleteOrganizationPropagation = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByDeleteOrganizationPropagation_PrevAndNext(
		long moduleId, Boolean deleteOrganizationPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByDeleteOrganizationPropagation_PrevAndNext(session,
					extModule, deleteOrganizationPropagation,
					orderByComparator, true);

			array[1] = extModule;

			array[2] = getByDeleteOrganizationPropagation_PrevAndNext(session,
					extModule, deleteOrganizationPropagation,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByDeleteOrganizationPropagation_PrevAndNext(
		Session session, ExtModule extModule,
		Boolean deleteOrganizationPropagation,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		query.append(_FINDER_COLUMN_DELETEORGANIZATIONPROPAGATION_DELETEORGANIZATIONPROPAGATION_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(deleteOrganizationPropagation.booleanValue());

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where deleteOrganizationPropagation = &#63; from the database.
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByDeleteOrganizationPropagation(
		Boolean deleteOrganizationPropagation) throws SystemException {
		for (ExtModule extModule : findByDeleteOrganizationPropagation(
				deleteOrganizationPropagation, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where deleteOrganizationPropagation = &#63;.
	 *
	 * @param deleteOrganizationPropagation the delete organization propagation
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDeleteOrganizationPropagation(
		Boolean deleteOrganizationPropagation) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DELETEORGANIZATIONPROPAGATION;

		Object[] finderArgs = new Object[] { deleteOrganizationPropagation };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_DELETEORGANIZATIONPROPAGATION_DELETEORGANIZATIONPROPAGATION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(deleteOrganizationPropagation.booleanValue());

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DELETEORGANIZATIONPROPAGATION_DELETEORGANIZATIONPROPAGATION_2 =
		"extModule.deleteOrganizationPropagation = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ADDORGANIZATIONMEMBERSPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByAddOrganizationMembersPropagation",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ADDORGANIZATIONMEMBERSPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByAddOrganizationMembersPropagation",
			new String[] { Boolean.class.getName() },
			ExtModuleModelImpl.ADDORGANIZATIONMEMBERSPROPAGATION_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ADDORGANIZATIONMEMBERSPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByAddOrganizationMembersPropagation",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the ext modules where addOrganizationMembersPropagation = &#63;.
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByAddOrganizationMembersPropagation(
		Boolean addOrganizationMembersPropagation) throws SystemException {
		return findByAddOrganizationMembersPropagation(addOrganizationMembersPropagation,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where addOrganizationMembersPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByAddOrganizationMembersPropagation(
		Boolean addOrganizationMembersPropagation, int start, int end)
		throws SystemException {
		return findByAddOrganizationMembersPropagation(addOrganizationMembersPropagation,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where addOrganizationMembersPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByAddOrganizationMembersPropagation(
		Boolean addOrganizationMembersPropagation, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ADDORGANIZATIONMEMBERSPROPAGATION;
			finderArgs = new Object[] { addOrganizationMembersPropagation };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ADDORGANIZATIONMEMBERSPROPAGATION;
			finderArgs = new Object[] {
					addOrganizationMembersPropagation,
					
					start, end, orderByComparator
				};
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(addOrganizationMembersPropagation,
							extModule.getAddOrganizationMembersPropagation())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_ADDORGANIZATIONMEMBERSPROPAGATION_ADDORGANIZATIONMEMBERSPROPAGATION_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(addOrganizationMembersPropagation.booleanValue());

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByAddOrganizationMembersPropagation_First(
		Boolean addOrganizationMembersPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByAddOrganizationMembersPropagation_First(addOrganizationMembersPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("addOrganizationMembersPropagation=");
		msg.append(addOrganizationMembersPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByAddOrganizationMembersPropagation_First(
		Boolean addOrganizationMembersPropagation,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByAddOrganizationMembersPropagation(addOrganizationMembersPropagation,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByAddOrganizationMembersPropagation_Last(
		Boolean addOrganizationMembersPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByAddOrganizationMembersPropagation_Last(addOrganizationMembersPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("addOrganizationMembersPropagation=");
		msg.append(addOrganizationMembersPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByAddOrganizationMembersPropagation_Last(
		Boolean addOrganizationMembersPropagation,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByAddOrganizationMembersPropagation(addOrganizationMembersPropagation);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByAddOrganizationMembersPropagation(addOrganizationMembersPropagation,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where addOrganizationMembersPropagation = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByAddOrganizationMembersPropagation_PrevAndNext(
		long moduleId, Boolean addOrganizationMembersPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByAddOrganizationMembersPropagation_PrevAndNext(session,
					extModule, addOrganizationMembersPropagation,
					orderByComparator, true);

			array[1] = extModule;

			array[2] = getByAddOrganizationMembersPropagation_PrevAndNext(session,
					extModule, addOrganizationMembersPropagation,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByAddOrganizationMembersPropagation_PrevAndNext(
		Session session, ExtModule extModule,
		Boolean addOrganizationMembersPropagation,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		query.append(_FINDER_COLUMN_ADDORGANIZATIONMEMBERSPROPAGATION_ADDORGANIZATIONMEMBERSPROPAGATION_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(addOrganizationMembersPropagation.booleanValue());

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where addOrganizationMembersPropagation = &#63; from the database.
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByAddOrganizationMembersPropagation(
		Boolean addOrganizationMembersPropagation) throws SystemException {
		for (ExtModule extModule : findByAddOrganizationMembersPropagation(
				addOrganizationMembersPropagation, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where addOrganizationMembersPropagation = &#63;.
	 *
	 * @param addOrganizationMembersPropagation the add organization members propagation
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByAddOrganizationMembersPropagation(
		Boolean addOrganizationMembersPropagation) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ADDORGANIZATIONMEMBERSPROPAGATION;

		Object[] finderArgs = new Object[] { addOrganizationMembersPropagation };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_ADDORGANIZATIONMEMBERSPROPAGATION_ADDORGANIZATIONMEMBERSPROPAGATION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(addOrganizationMembersPropagation.booleanValue());

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ADDORGANIZATIONMEMBERSPROPAGATION_ADDORGANIZATIONMEMBERSPROPAGATION_2 =
		"extModule.addOrganizationMembersPropagation = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByRemoveOrganizationMembersPropagation",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, ExtModuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByRemoveOrganizationMembersPropagation",
			new String[] { Boolean.class.getName() },
			ExtModuleModelImpl.REMOVEORGANIZATIONMEMBERSPROPAGATION_COLUMN_BITMASK |
			ExtModuleModelImpl.MODULENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION =
		new FinderPath(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByRemoveOrganizationMembersPropagation",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the ext modules where removeOrganizationMembersPropagation = &#63;.
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @return the matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByRemoveOrganizationMembersPropagation(
		Boolean removeOrganizationMembersPropagation) throws SystemException {
		return findByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules where removeOrganizationMembersPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByRemoveOrganizationMembersPropagation(
		Boolean removeOrganizationMembersPropagation, int start, int end)
		throws SystemException {
		return findByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules where removeOrganizationMembersPropagation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findByRemoveOrganizationMembersPropagation(
		Boolean removeOrganizationMembersPropagation, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION;
			finderArgs = new Object[] { removeOrganizationMembersPropagation };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION;
			finderArgs = new Object[] {
					removeOrganizationMembersPropagation,
					
					start, end, orderByComparator
				};
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ExtModule extModule : list) {
				if (!Validator.equals(removeOrganizationMembersPropagation,
							extModule.getRemoveOrganizationMembersPropagation())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_REMOVEORGANIZATIONMEMBERSPROPAGATION_REMOVEORGANIZATIONMEMBERSPROPAGATION_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(removeOrganizationMembersPropagation.booleanValue());

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByRemoveOrganizationMembersPropagation_First(
		Boolean removeOrganizationMembersPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByRemoveOrganizationMembersPropagation_First(removeOrganizationMembersPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("removeOrganizationMembersPropagation=");
		msg.append(removeOrganizationMembersPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the first ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByRemoveOrganizationMembersPropagation_First(
		Boolean removeOrganizationMembersPropagation,
		OrderByComparator orderByComparator) throws SystemException {
		List<ExtModule> list = findByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module
	 * @throws it.eng.NoSuchExtModuleException if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByRemoveOrganizationMembersPropagation_Last(
		Boolean removeOrganizationMembersPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByRemoveOrganizationMembersPropagation_Last(removeOrganizationMembersPropagation,
				orderByComparator);

		if (extModule != null) {
			return extModule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("removeOrganizationMembersPropagation=");
		msg.append(removeOrganizationMembersPropagation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExtModuleException(msg.toString());
	}

	/**
	 * Returns the last ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ext module, or <code>null</code> if a matching ext module could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByRemoveOrganizationMembersPropagation_Last(
		Boolean removeOrganizationMembersPropagation,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);

		if (count == 0) {
			return null;
		}

		List<ExtModule> list = findByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ext modules before and after the current ext module in the ordered set where removeOrganizationMembersPropagation = &#63;.
	 *
	 * @param moduleId the primary key of the current ext module
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule[] findByRemoveOrganizationMembersPropagation_PrevAndNext(
		long moduleId, Boolean removeOrganizationMembersPropagation,
		OrderByComparator orderByComparator)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = findByPrimaryKey(moduleId);

		Session session = null;

		try {
			session = openSession();

			ExtModule[] array = new ExtModuleImpl[3];

			array[0] = getByRemoveOrganizationMembersPropagation_PrevAndNext(session,
					extModule, removeOrganizationMembersPropagation,
					orderByComparator, true);

			array[1] = extModule;

			array[2] = getByRemoveOrganizationMembersPropagation_PrevAndNext(session,
					extModule, removeOrganizationMembersPropagation,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExtModule getByRemoveOrganizationMembersPropagation_PrevAndNext(
		Session session, ExtModule extModule,
		Boolean removeOrganizationMembersPropagation,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXTMODULE_WHERE);

		query.append(_FINDER_COLUMN_REMOVEORGANIZATIONMEMBERSPROPAGATION_REMOVEORGANIZATIONMEMBERSPROPAGATION_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ExtModuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(removeOrganizationMembersPropagation.booleanValue());

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(extModule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ExtModule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ext modules where removeOrganizationMembersPropagation = &#63; from the database.
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByRemoveOrganizationMembersPropagation(
		Boolean removeOrganizationMembersPropagation) throws SystemException {
		for (ExtModule extModule : findByRemoveOrganizationMembersPropagation(
				removeOrganizationMembersPropagation, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules where removeOrganizationMembersPropagation = &#63;.
	 *
	 * @param removeOrganizationMembersPropagation the remove organization members propagation
	 * @return the number of matching ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByRemoveOrganizationMembersPropagation(
		Boolean removeOrganizationMembersPropagation) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION;

		Object[] finderArgs = new Object[] { removeOrganizationMembersPropagation };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXTMODULE_WHERE);

			query.append(_FINDER_COLUMN_REMOVEORGANIZATIONMEMBERSPROPAGATION_REMOVEORGANIZATIONMEMBERSPROPAGATION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(removeOrganizationMembersPropagation.booleanValue());

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REMOVEORGANIZATIONMEMBERSPROPAGATION_REMOVEORGANIZATIONMEMBERSPROPAGATION_2 =
		"extModule.removeOrganizationMembersPropagation = ?";

	public ExtModulePersistenceImpl() {
		setModelClass(ExtModule.class);
	}

	/**
	 * Caches the ext module in the entity cache if it is enabled.
	 *
	 * @param extModule the ext module
	 */
	@Override
	public void cacheResult(ExtModule extModule) {
		EntityCacheUtil.putResult(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleImpl.class, extModule.getPrimaryKey(), extModule);

		extModule.resetOriginalValues();
	}

	/**
	 * Caches the ext modules in the entity cache if it is enabled.
	 *
	 * @param extModules the ext modules
	 */
	@Override
	public void cacheResult(List<ExtModule> extModules) {
		for (ExtModule extModule : extModules) {
			if (EntityCacheUtil.getResult(
						ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
						ExtModuleImpl.class, extModule.getPrimaryKey()) == null) {
				cacheResult(extModule);
			}
			else {
				extModule.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ext modules.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ExtModuleImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ExtModuleImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ext module.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ExtModule extModule) {
		EntityCacheUtil.removeResult(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleImpl.class, extModule.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ExtModule> extModules) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ExtModule extModule : extModules) {
			EntityCacheUtil.removeResult(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
				ExtModuleImpl.class, extModule.getPrimaryKey());
		}
	}

	/**
	 * Creates a new ext module with the primary key. Does not add the ext module to the database.
	 *
	 * @param moduleId the primary key for the new ext module
	 * @return the new ext module
	 */
	@Override
	public ExtModule create(long moduleId) {
		ExtModule extModule = new ExtModuleImpl();

		extModule.setNew(true);
		extModule.setPrimaryKey(moduleId);

		return extModule;
	}

	/**
	 * Removes the ext module with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param moduleId the primary key of the ext module
	 * @return the ext module that was removed
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule remove(long moduleId)
		throws NoSuchExtModuleException, SystemException {
		return remove((Serializable)moduleId);
	}

	/**
	 * Removes the ext module with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ext module
	 * @return the ext module that was removed
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule remove(Serializable primaryKey)
		throws NoSuchExtModuleException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ExtModule extModule = (ExtModule)session.get(ExtModuleImpl.class,
					primaryKey);

			if (extModule == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchExtModuleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(extModule);
		}
		catch (NoSuchExtModuleException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ExtModule removeImpl(ExtModule extModule)
		throws SystemException {
		extModule = toUnwrappedModel(extModule);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(extModule)) {
				extModule = (ExtModule)session.get(ExtModuleImpl.class,
						extModule.getPrimaryKeyObj());
			}

			if (extModule != null) {
				session.delete(extModule);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (extModule != null) {
			clearCache(extModule);
		}

		return extModule;
	}

	@Override
	public ExtModule updateImpl(it.eng.model.ExtModule extModule)
		throws SystemException {
		extModule = toUnwrappedModel(extModule);

		boolean isNew = extModule.isNew();

		ExtModuleModelImpl extModuleModelImpl = (ExtModuleModelImpl)extModule;

		Session session = null;

		try {
			session = openSession();

			if (extModule.isNew()) {
				session.save(extModule);

				extModule.setNew(false);
			}
			else {
				session.merge(extModule);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ExtModuleModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalModuleId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEID,
					args);

				args = new Object[] { extModuleModelImpl.getModuleId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEID,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULENAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalModuleName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULENAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULENAME,
					args);

				args = new Object[] { extModuleModelImpl.getModuleName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULENAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULENAME,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEURL.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalModuleURL()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULEURL,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEURL,
					args);

				args = new Object[] { extModuleModelImpl.getModuleURL() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULEURL,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEURL,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEENDPOINT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalModuleEndpoint()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULEENDPOINT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEENDPOINT,
					args);

				args = new Object[] { extModuleModelImpl.getModuleEndpoint() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULEENDPOINT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEENDPOINT,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEALIAS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalModuleAlias()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULEALIAS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEALIAS,
					args);

				args = new Object[] { extModuleModelImpl.getModuleAlias() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULEALIAS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULEALIAS,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULESHAREDSECRET.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalModuleSharedSecret()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULESHAREDSECRET,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULESHAREDSECRET,
					args);

				args = new Object[] { extModuleModelImpl.getModuleSharedSecret() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODULESHAREDSECRET,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODULESHAREDSECRET,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REGISTRATIONPROPAGATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalRegistrationPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REGISTRATIONPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REGISTRATIONPROPAGATION,
					args);

				args = new Object[] {
						extModuleModelImpl.getRegistrationPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REGISTRATIONPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REGISTRATIONPROPAGATION,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETIONPROPAGATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalDeletionPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DELETIONPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETIONPROPAGATION,
					args);

				args = new Object[] { extModuleModelImpl.getDeletionPropagation() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DELETIONPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETIONPROPAGATION,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UPSERTORGANIZATIONPROPAGATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalUpsertOrganizationPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UPSERTORGANIZATIONPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UPSERTORGANIZATIONPROPAGATION,
					args);

				args = new Object[] {
						extModuleModelImpl.getUpsertOrganizationPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UPSERTORGANIZATIONPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UPSERTORGANIZATIONPROPAGATION,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETEORGANIZATIONPROPAGATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalDeleteOrganizationPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DELETEORGANIZATIONPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETEORGANIZATIONPROPAGATION,
					args);

				args = new Object[] {
						extModuleModelImpl.getDeleteOrganizationPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DELETEORGANIZATIONPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DELETEORGANIZATIONPROPAGATION,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ADDORGANIZATIONMEMBERSPROPAGATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalAddOrganizationMembersPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ADDORGANIZATIONMEMBERSPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ADDORGANIZATIONMEMBERSPROPAGATION,
					args);

				args = new Object[] {
						extModuleModelImpl.getAddOrganizationMembersPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ADDORGANIZATIONMEMBERSPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ADDORGANIZATIONMEMBERSPROPAGATION,
					args);
			}

			if ((extModuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						extModuleModelImpl.getOriginalRemoveOrganizationMembersPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION,
					args);

				args = new Object[] {
						extModuleModelImpl.getRemoveOrganizationMembersPropagation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMOVEORGANIZATIONMEMBERSPROPAGATION,
					args);
			}
		}

		EntityCacheUtil.putResult(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
			ExtModuleImpl.class, extModule.getPrimaryKey(), extModule);

		return extModule;
	}

	protected ExtModule toUnwrappedModel(ExtModule extModule) {
		if (extModule instanceof ExtModuleImpl) {
			return extModule;
		}

		ExtModuleImpl extModuleImpl = new ExtModuleImpl();

		extModuleImpl.setNew(extModule.isNew());
		extModuleImpl.setPrimaryKey(extModule.getPrimaryKey());

		extModuleImpl.setModuleId(extModule.getModuleId());
		extModuleImpl.setModuleName(extModule.getModuleName());
		extModuleImpl.setModuleURL(extModule.getModuleURL());
		extModuleImpl.setRegistrationPropagation(extModule.getRegistrationPropagation());
		extModuleImpl.setModuleEndpoint(extModule.getModuleEndpoint());
		extModuleImpl.setDeletionPropagation(extModule.getDeletionPropagation());
		extModuleImpl.setUserDeletionEndpoint(extModule.getUserDeletionEndpoint());
		extModuleImpl.setUpsertOrganizationPropagation(extModule.getUpsertOrganizationPropagation());
		extModuleImpl.setUpsertOrganizationEndpoint(extModule.getUpsertOrganizationEndpoint());
		extModuleImpl.setDeleteOrganizationPropagation(extModule.getDeleteOrganizationPropagation());
		extModuleImpl.setDeleteOrganizationEndpoint(extModule.getDeleteOrganizationEndpoint());
		extModuleImpl.setAddOrganizationMembersPropagation(extModule.getAddOrganizationMembersPropagation());
		extModuleImpl.setAddOrganizationMembersEndpoint(extModule.getAddOrganizationMembersEndpoint());
		extModuleImpl.setRemoveOrganizationMembersPropagation(extModule.getRemoveOrganizationMembersPropagation());
		extModuleImpl.setRemoveOrganizationMembersEndpoint(extModule.getRemoveOrganizationMembersEndpoint());
		extModuleImpl.setModuleAlias(extModule.getModuleAlias());
		extModuleImpl.setModuleSharedSecret(extModule.getModuleSharedSecret());
		extModuleImpl.setContentType(extModule.getContentType());

		return extModuleImpl;
	}

	/**
	 * Returns the ext module with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ext module
	 * @return the ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByPrimaryKey(Serializable primaryKey)
		throws NoSuchExtModuleException, SystemException {
		ExtModule extModule = fetchByPrimaryKey(primaryKey);

		if (extModule == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchExtModuleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return extModule;
	}

	/**
	 * Returns the ext module with the primary key or throws a {@link it.eng.NoSuchExtModuleException} if it could not be found.
	 *
	 * @param moduleId the primary key of the ext module
	 * @return the ext module
	 * @throws it.eng.NoSuchExtModuleException if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule findByPrimaryKey(long moduleId)
		throws NoSuchExtModuleException, SystemException {
		return findByPrimaryKey((Serializable)moduleId);
	}

	/**
	 * Returns the ext module with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ext module
	 * @return the ext module, or <code>null</code> if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ExtModule extModule = (ExtModule)EntityCacheUtil.getResult(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
				ExtModuleImpl.class, primaryKey);

		if (extModule == _nullExtModule) {
			return null;
		}

		if (extModule == null) {
			Session session = null;

			try {
				session = openSession();

				extModule = (ExtModule)session.get(ExtModuleImpl.class,
						primaryKey);

				if (extModule != null) {
					cacheResult(extModule);
				}
				else {
					EntityCacheUtil.putResult(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
						ExtModuleImpl.class, primaryKey, _nullExtModule);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ExtModuleModelImpl.ENTITY_CACHE_ENABLED,
					ExtModuleImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return extModule;
	}

	/**
	 * Returns the ext module with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param moduleId the primary key of the ext module
	 * @return the ext module, or <code>null</code> if a ext module with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ExtModule fetchByPrimaryKey(long moduleId) throws SystemException {
		return fetchByPrimaryKey((Serializable)moduleId);
	}

	/**
	 * Returns all the ext modules.
	 *
	 * @return the ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ext modules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @return the range of ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ext modules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.model.impl.ExtModuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ext modules
	 * @param end the upper bound of the range of ext modules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ExtModule> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ExtModule> list = (List<ExtModule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EXTMODULE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EXTMODULE;

				if (pagination) {
					sql = sql.concat(ExtModuleModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ExtModule>(list);
				}
				else {
					list = (List<ExtModule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ext modules from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ExtModule extModule : findAll()) {
			remove(extModule);
		}
	}

	/**
	 * Returns the number of ext modules.
	 *
	 * @return the number of ext modules
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EXTMODULE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the ext module persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.model.ExtModule")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ExtModule>> listenersList = new ArrayList<ModelListener<ExtModule>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ExtModule>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ExtModuleImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_EXTMODULE = "SELECT extModule FROM ExtModule extModule";
	private static final String _SQL_SELECT_EXTMODULE_WHERE = "SELECT extModule FROM ExtModule extModule WHERE ";
	private static final String _SQL_COUNT_EXTMODULE = "SELECT COUNT(extModule) FROM ExtModule extModule";
	private static final String _SQL_COUNT_EXTMODULE_WHERE = "SELECT COUNT(extModule) FROM ExtModule extModule WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "extModule.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ExtModule exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ExtModule exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ExtModulePersistenceImpl.class);
	private static ExtModule _nullExtModule = new ExtModuleImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ExtModule> toCacheModel() {
				return _nullExtModuleCacheModel;
			}
		};

	private static CacheModel<ExtModule> _nullExtModuleCacheModel = new CacheModel<ExtModule>() {
			@Override
			public ExtModule toEntityModel() {
				return _nullExtModule;
			}
		};
}