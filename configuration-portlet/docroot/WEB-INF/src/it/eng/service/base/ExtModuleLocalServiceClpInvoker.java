/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service.base;

import it.eng.service.ExtModuleLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class ExtModuleLocalServiceClpInvoker {
	public ExtModuleLocalServiceClpInvoker() {
		_methodName0 = "addExtModule";

		_methodParameterTypes0 = new String[] { "it.eng.model.ExtModule" };

		_methodName1 = "createExtModule";

		_methodParameterTypes1 = new String[] { "long" };

		_methodName2 = "deleteExtModule";

		_methodParameterTypes2 = new String[] { "long" };

		_methodName3 = "deleteExtModule";

		_methodParameterTypes3 = new String[] { "it.eng.model.ExtModule" };

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchExtModule";

		_methodParameterTypes10 = new String[] { "long" };

		_methodName11 = "getExtModule";

		_methodParameterTypes11 = new String[] { "long" };

		_methodName12 = "getPersistedModel";

		_methodParameterTypes12 = new String[] { "java.io.Serializable" };

		_methodName13 = "getExtModules";

		_methodParameterTypes13 = new String[] { "int", "int" };

		_methodName14 = "getExtModulesCount";

		_methodParameterTypes14 = new String[] {  };

		_methodName15 = "updateExtModule";

		_methodParameterTypes15 = new String[] { "it.eng.model.ExtModule" };

		_methodName36 = "getBeanIdentifier";

		_methodParameterTypes36 = new String[] {  };

		_methodName37 = "setBeanIdentifier";

		_methodParameterTypes37 = new String[] { "java.lang.String" };

		_methodName42 = "addExtModule";

		_methodParameterTypes42 = new String[] {
				"java.lang.String", "java.lang.String", "boolean",
				"java.lang.String", "boolean", "java.lang.String", "boolean",
				"java.lang.String", "boolean", "java.lang.String", "boolean",
				"java.lang.String", "boolean", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String"
			};

		_methodName43 = "deleteExtModule";

		_methodParameterTypes43 = new String[] { "it.eng.model.ExtModule" };

		_methodName44 = "deleteExtModule";

		_methodParameterTypes44 = new String[] { "long" };

		_methodName45 = "updateExtModule";

		_methodParameterTypes45 = new String[] {
				"long", "java.lang.String", "java.lang.String", "boolean",
				"java.lang.String", "boolean", "java.lang.String", "boolean",
				"java.lang.String", "boolean", "java.lang.String", "boolean",
				"java.lang.String", "boolean", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String"
			};

		_methodName46 = "getExtModuleByModuleIde";

		_methodParameterTypes46 = new String[] { "long" };

		_methodName47 = "getExtModuleByModuleName";

		_methodParameterTypes47 = new String[] { "java.lang.String" };

		_methodName48 = "getExtModuleByModuleURL";

		_methodParameterTypes48 = new String[] { "java.lang.String" };

		_methodName49 = "getExtModuleByModuleEndpoint";

		_methodParameterTypes49 = new String[] { "java.lang.String" };

		_methodName50 = "getExtModuleByModuleAlias";

		_methodParameterTypes50 = new String[] { "java.lang.String" };

		_methodName51 = "getExtModuleByModuleSharedSecret";

		_methodParameterTypes51 = new String[] { "java.lang.String" };

		_methodName52 = "getExtModuleByRegistrationPropagation";

		_methodParameterTypes52 = new String[] { "java.lang.Boolean" };

		_methodName53 = "getExtModuleByDeletionPropagation";

		_methodParameterTypes53 = new String[] { "java.lang.Boolean" };

		_methodName54 = "getExtModuleByUpsertOrganizationPropagation";

		_methodParameterTypes54 = new String[] { "java.lang.Boolean" };

		_methodName55 = "getExtModuleByDeleteOrganizationPropagation";

		_methodParameterTypes55 = new String[] { "java.lang.Boolean" };

		_methodName56 = "getExtModuleByAddOrganizationMembersPropagation";

		_methodParameterTypes56 = new String[] { "java.lang.Boolean" };

		_methodName57 = "getExtModuleByRemoveOrganizationMembersPropagation";

		_methodParameterTypes57 = new String[] { "java.lang.Boolean" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return ExtModuleLocalServiceUtil.addExtModule((it.eng.model.ExtModule)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return ExtModuleLocalServiceUtil.createExtModule(((Long)arguments[0]).longValue());
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return ExtModuleLocalServiceUtil.deleteExtModule(((Long)arguments[0]).longValue());
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return ExtModuleLocalServiceUtil.deleteExtModule((it.eng.model.ExtModule)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return ExtModuleLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return ExtModuleLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return ExtModuleLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return ExtModuleLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return ExtModuleLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return ExtModuleLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return ExtModuleLocalServiceUtil.fetchExtModule(((Long)arguments[0]).longValue());
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModule(((Long)arguments[0]).longValue());
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModules(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModulesCount();
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return ExtModuleLocalServiceUtil.updateExtModule((it.eng.model.ExtModule)arguments[0]);
		}

		if (_methodName36.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes36, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName37.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes37, parameterTypes)) {
			ExtModuleLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName42.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes42, parameterTypes)) {
			return ExtModuleLocalServiceUtil.addExtModule((java.lang.String)arguments[0],
				(java.lang.String)arguments[1],
				((Boolean)arguments[2]).booleanValue(),
				(java.lang.String)arguments[3],
				((Boolean)arguments[4]).booleanValue(),
				(java.lang.String)arguments[5],
				((Boolean)arguments[6]).booleanValue(),
				(java.lang.String)arguments[7],
				((Boolean)arguments[8]).booleanValue(),
				(java.lang.String)arguments[9],
				((Boolean)arguments[10]).booleanValue(),
				(java.lang.String)arguments[11],
				((Boolean)arguments[12]).booleanValue(),
				(java.lang.String)arguments[13],
				(java.lang.String)arguments[14],
				(java.lang.String)arguments[15], (java.lang.String)arguments[16]);
		}

		if (_methodName43.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes43, parameterTypes)) {
			return ExtModuleLocalServiceUtil.deleteExtModule((it.eng.model.ExtModule)arguments[0]);
		}

		if (_methodName44.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes44, parameterTypes)) {
			return ExtModuleLocalServiceUtil.deleteExtModule(((Long)arguments[0]).longValue());
		}

		if (_methodName45.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes45, parameterTypes)) {
			return ExtModuleLocalServiceUtil.updateExtModule(((Long)arguments[0]).longValue(),
				(java.lang.String)arguments[1], (java.lang.String)arguments[2],
				((Boolean)arguments[3]).booleanValue(),
				(java.lang.String)arguments[4],
				((Boolean)arguments[5]).booleanValue(),
				(java.lang.String)arguments[6],
				((Boolean)arguments[7]).booleanValue(),
				(java.lang.String)arguments[8],
				((Boolean)arguments[9]).booleanValue(),
				(java.lang.String)arguments[10],
				((Boolean)arguments[11]).booleanValue(),
				(java.lang.String)arguments[12],
				((Boolean)arguments[13]).booleanValue(),
				(java.lang.String)arguments[14],
				(java.lang.String)arguments[15],
				(java.lang.String)arguments[16], (java.lang.String)arguments[17]);
		}

		if (_methodName46.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes46, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByModuleIde(((Long)arguments[0]).longValue());
		}

		if (_methodName47.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes47, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByModuleName((java.lang.String)arguments[0]);
		}

		if (_methodName48.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes48, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByModuleURL((java.lang.String)arguments[0]);
		}

		if (_methodName49.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes49, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByModuleEndpoint((java.lang.String)arguments[0]);
		}

		if (_methodName50.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes50, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByModuleAlias((java.lang.String)arguments[0]);
		}

		if (_methodName51.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes51, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByModuleSharedSecret((java.lang.String)arguments[0]);
		}

		if (_methodName52.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes52, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByRegistrationPropagation((java.lang.Boolean)arguments[0]);
		}

		if (_methodName53.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes53, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByDeletionPropagation((java.lang.Boolean)arguments[0]);
		}

		if (_methodName54.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes54, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByUpsertOrganizationPropagation((java.lang.Boolean)arguments[0]);
		}

		if (_methodName55.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes55, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByDeleteOrganizationPropagation((java.lang.Boolean)arguments[0]);
		}

		if (_methodName56.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes56, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByAddOrganizationMembersPropagation((java.lang.Boolean)arguments[0]);
		}

		if (_methodName57.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes57, parameterTypes)) {
			return ExtModuleLocalServiceUtil.getExtModuleByRemoveOrganizationMembersPropagation((java.lang.Boolean)arguments[0]);
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName36;
	private String[] _methodParameterTypes36;
	private String _methodName37;
	private String[] _methodParameterTypes37;
	private String _methodName42;
	private String[] _methodParameterTypes42;
	private String _methodName43;
	private String[] _methodParameterTypes43;
	private String _methodName44;
	private String[] _methodParameterTypes44;
	private String _methodName45;
	private String[] _methodParameterTypes45;
	private String _methodName46;
	private String[] _methodParameterTypes46;
	private String _methodName47;
	private String[] _methodParameterTypes47;
	private String _methodName48;
	private String[] _methodParameterTypes48;
	private String _methodName49;
	private String[] _methodParameterTypes49;
	private String _methodName50;
	private String[] _methodParameterTypes50;
	private String _methodName51;
	private String[] _methodParameterTypes51;
	private String _methodName52;
	private String[] _methodParameterTypes52;
	private String _methodName53;
	private String[] _methodParameterTypes53;
	private String _methodName54;
	private String[] _methodParameterTypes54;
	private String _methodName55;
	private String[] _methodParameterTypes55;
	private String _methodName56;
	private String[] _methodParameterTypes56;
	private String _methodName57;
	private String[] _methodParameterTypes57;
}