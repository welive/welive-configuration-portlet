/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import it.eng.model.ExtModule;
import it.eng.service.ExtModuleLocalServiceUtil;
import it.eng.service.base.ExtModuleLocalServiceBaseImpl;

/**
 * The implementation of the ext module local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.service.ExtModuleLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author eng
 * @see it.eng.service.base.ExtModuleLocalServiceBaseImpl
 * @see it.eng.service.ExtModuleLocalServiceUtil
 */
public class ExtModuleLocalServiceImpl extends ExtModuleLocalServiceBaseImpl 
{
	public ExtModule addExtModule(String moduleName, String moduleURL, boolean registrationPropagation, String moduleEndpoint, 
			boolean deletiontionPropagation,String userDeletionEndpoint,  boolean upsertOrganizationPropagation, String upsertOrganizationEndpoint, 
			boolean deleteOrganizationPropagation, String deleteOrganizationEndpoint,	boolean addOrganizationMembersPropagation, String addOrganizationMembersEndpoint, 
			boolean removeOrganizationMembersPropagation, String removeOrganizationMembersEndpoint,			
			String moduleAlias, String moduleSharedSecret, String moduleContentType)throws PortalException, SystemException {
		long moduleId = counterLocalService.increment();
		
		ExtModule extModule = extModulePersistence.create(moduleId);
		extModule.setModuleName(moduleName);
		extModule.setModuleURL(moduleURL);
		extModule.setRegistrationPropagation(registrationPropagation);
		extModule.setModuleEndpoint(moduleEndpoint);
		extModule.setDeletionPropagation(deletiontionPropagation);
		extModule.setUserDeletionEndpoint(userDeletionEndpoint);
		extModule.setUpsertOrganizationPropagation(upsertOrganizationPropagation);
		extModule.setUpsertOrganizationEndpoint(upsertOrganizationEndpoint);
		extModule.setDeleteOrganizationPropagation(deleteOrganizationPropagation);
		extModule.setDeleteOrganizationEndpoint(deleteOrganizationEndpoint);
		extModule.setAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
		extModule.setAddOrganizationMembersEndpoint(addOrganizationMembersEndpoint);
		extModule.setRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
		extModule.setRemoveOrganizationMembersEndpoint(removeOrganizationMembersEndpoint);
		extModule.setModuleAlias(moduleAlias);
		extModule.setModuleSharedSecret(moduleSharedSecret);
		extModule.setContentType(moduleContentType);
		
		super.addExtModule(extModule);
		
		return extModule;
	}
	
	public ExtModule deleteExtModule(ExtModule module) throws SystemException
	{
		return extModulePersistence.remove(module);
	}
	
	public ExtModule deleteExtModule(long moduleId) throws SystemException
	{
		ExtModule module = extModulePersistence.fetchByPrimaryKey(moduleId);
		
		return deleteExtModule(module);
	}
	
	public ExtModule updateExtModule(long moduleId, String moduleName, String moduleURL, boolean registrationPropagation, String moduleEndpoint, 
			boolean deletiontionPropagation,String userDeletionEndpoint,  boolean upsertOrganizationPropagation, String upsertOrganizationEndpoint, 
			boolean deleteOrganizationPropagation, String deleteOrganizationEndpoint,	boolean addOrganizationMembersPropagation, String addOrganizationMembersEndpoint, 
			boolean removeOrganizationMembersPropagation, String removeOrganizationMembersEndpoint,	 String moduleAlias, String moduleSharedSecret, String moduleContentType)
	throws PortalException, SystemException {
		ExtModule extModule = extModulePersistence.fetchByPrimaryKey(moduleId);
		
		extModule.setModuleName(moduleName);
		extModule.setModuleURL(moduleURL);
		extModule.setRegistrationPropagation(registrationPropagation);
		extModule.setModuleEndpoint(moduleEndpoint);
		extModule.setDeletionPropagation(deletiontionPropagation);
		extModule.setUserDeletionEndpoint(userDeletionEndpoint);
		extModule.setUpsertOrganizationPropagation(upsertOrganizationPropagation);
		extModule.setUpsertOrganizationEndpoint(upsertOrganizationEndpoint);
		extModule.setDeleteOrganizationPropagation(deleteOrganizationPropagation);
		extModule.setDeleteOrganizationEndpoint(deleteOrganizationEndpoint);
		extModule.setAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
		extModule.setAddOrganizationMembersEndpoint(addOrganizationMembersEndpoint);
		extModule.setRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
		extModule.setRemoveOrganizationMembersEndpoint(removeOrganizationMembersEndpoint);
		extModule.setModuleAlias(moduleAlias);
		extModule.setModuleSharedSecret(moduleSharedSecret);
		extModule.setContentType(moduleContentType);
		
		super.updateExtModule(extModule);
		
		return extModule;
	}
	
	public List<ExtModule> getExtModuleByModuleIde(long moduleId) throws SystemException
	{
		return extModulePersistence.findByModuleId(moduleId);
	}
	
	public List<ExtModule> getExtModuleByModuleName(String moduleName) throws SystemException
	{
		return extModulePersistence.findByModuleName(moduleName);
	}
	
	public List<ExtModule> getExtModuleByModuleURL(String moduleURL) throws SystemException
	{
		return extModulePersistence.findByModuleURL(moduleURL);
	}
	
	public List<ExtModule> getExtModuleByModuleEndpoint(String moduleEndpoint) throws SystemException
	{
		return extModulePersistence.findByModuleEndpoint(moduleEndpoint);
	}
	
	public List<ExtModule> getExtModuleByModuleAlias(String moduleAlias) throws SystemException
	{
		return extModulePersistence.findByModuleAlias(moduleAlias);
	}
	
	public List<ExtModule> getExtModuleByModuleSharedSecret(String moduleSharedSecret) throws SystemException
	{
		return extModulePersistence.findByModuleSharedSecret(moduleSharedSecret);
	}
	
	public List<ExtModule> getExtModuleByRegistrationPropagation(Boolean registrationPropagation) throws SystemException{
		return extModulePersistence.findByRegistrationPropagation(registrationPropagation);
	}
	
	public List<ExtModule> getExtModuleByDeletionPropagation(Boolean deletionPropagation) throws SystemException{
		return extModulePersistence.findByDeletionPropagation(deletionPropagation);
	}
	
	public List<ExtModule> getExtModuleByUpsertOrganizationPropagation(Boolean upsertOrganizationPropagation) throws SystemException{
		return extModulePersistence.findByUpsertOrganizationPropagation(upsertOrganizationPropagation);
	}
	
	public List<ExtModule> getExtModuleByDeleteOrganizationPropagation(Boolean deleteOrganizationPropagation) throws SystemException{
		return extModulePersistence.findByDeleteOrganizationPropagation(deleteOrganizationPropagation);
	}
	
	public List<ExtModule> getExtModuleByAddOrganizationMembersPropagation(Boolean addOrganizationMembersPropagation) throws SystemException{
		return extModulePersistence.findByAddOrganizationMembersPropagation(addOrganizationMembersPropagation);
	}
	
	public List<ExtModule> getExtModuleByRemoveOrganizationMembersPropagation(Boolean removeOrganizationMembersPropagation) throws SystemException{
		return extModulePersistence.findByRemoveOrganizationMembersPropagation(removeOrganizationMembersPropagation);
	}
	
	
	
	
}