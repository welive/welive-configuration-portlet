/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import it.eng.model.PortalConf;
import it.eng.service.base.PortalConfLocalServiceBaseImpl;

/**
 * The implementation of the portal conf local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.service.PortalConfLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author eng
 * @see it.eng.service.base.PortalConfLocalServiceBaseImpl
 * @see it.eng.service.PortalConfLocalServiceUtil
 */
public class PortalConfLocalServiceImpl extends PortalConfLocalServiceBaseImpl 
{
	public PortalConf addPortalConf(
			String tomcatKsAlias,
			String tomcatKsPassword,
			String tomcatKsLocation,
			String jvmKsPassword,
			String jvmKsLocation) throws SystemException
	{
		PortalConf portalConf = portalConfPersistence.create("securityConfiguration");
		
		portalConf.setTomcatKsAlias(tomcatKsAlias);
		portalConf.setTomcatKsPassword(tomcatKsPassword);
		portalConf.setTomcatKsLocation(tomcatKsLocation);
		portalConf.setJvmKsLocation(jvmKsLocation);
		portalConf.setJvmKsPassword(jvmKsPassword);
		
		super.addPortalConf(portalConf);
		
		return portalConf;
	}
	
	public PortalConf updatePortalConf(
			String tomcatKsAlias,
			String tomcatKsPassword,
			String tomcatKsLocation,
			String jvmKsPassword,
			String jvmKsLocation) throws SystemException
	{
		PortalConf portalConf = portalConfPersistence.fetchByPrimaryKey("securityConfiguration");
		
		portalConf.setTomcatKsAlias(tomcatKsAlias);
		portalConf.setTomcatKsPassword(tomcatKsPassword);
		portalConf.setTomcatKsLocation(tomcatKsLocation);
		portalConf.setJvmKsLocation(jvmKsLocation);
		portalConf.setJvmKsPassword(jvmKsPassword);
		
		super.addPortalConf(portalConf);
		
		return portalConf;
	}
	
	public PortalConf deletePortalConf() throws SystemException
	{
		PortalConf portalConf = portalConfPersistence.fetchByPrimaryKey("securityConfiguration");
		
		return portalConfPersistence.remove(portalConf);
	}
	
	public PortalConf getSecurityConfiguration() throws SystemException
	{
		List<PortalConf> portalConf = portalConfPersistence.findByConfName("securityConfiguration");
		
		if(portalConf.size() > 0)
			return portalConf.get(0);
		
		return null;
	}
}