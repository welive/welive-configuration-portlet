<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@page import="javax.portlet.WindowState" %>
<%@page import="javax.portlet.ActionRequest" %>
<%@page import="javax.portlet.PortletURL" %>
<%@page import="javax.portlet.PortletMode" %>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil" %>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants" %>

<%@ page import="it.eng.model.ExtModule" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
    ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    ExtModule module = (ExtModule) row.getObject();

    String redirect = PortalUtil.getCurrentURL(renderRequest);
    
%>

<liferay-ui:icon-menu message="actions">
    <portlet:renderURL var="editURL">
        <portlet:param name="mvcPath" value="/edit.jsp" />
        <portlet:param name="moduleId" value="<%= String.valueOf(module.getModuleId()) %>" />
        <portlet:param name="redirect" value="<%= redirect %>" />
    </portlet:renderURL>

    <liferay-ui:icon message="actions-edit" image="edit" url="<%= editURL.toString() %>" />

    <portlet:actionURL name="deleteExtModule" var="deleteURL">
        <portlet:param name="moduleId" value="<%= String.valueOf(module.getModuleId()) %>" />
        <portlet:param name="redirect" value="<%= redirect %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete message="actions-delete" url="<%= deleteURL.toString() %>" />
    
</liferay-ui:icon-menu>